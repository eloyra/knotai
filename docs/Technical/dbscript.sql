--=====================================================================================================================
--                          KNOTAI's DATABASE CREATION AND CONFIGURATION SCRIPT                                       --
--=====================================================================================================================


--==========
-- API DATA
--==========
/*
postgraphile \
  --connection postgres://knotai_postgraphile:90042f76-2fb1-4891-892a-63f1d72192c9@localhost \
  --schema knotai \
  --default-role knotai_admin \
  --secret 030d6dbe-0993-4559-9950-24b52eac3456 \
  --token knotai.jwt_token

postgraphile --connection postgres://knotai_postgraphile:90042f76-2fb1-4891-892a-63f1d72192c9@localhost/knotai --schema knotai --default-role knotai_admin --secret 030d6dbe-0993-4559-9950-24b52eac3456 --token jwt_token

postgraphile --append-plugins @graphile-contrib/pg-simplify-inflector --connection postgres://knotai_postgraphile:90042f76-2fb1-4891-892a-63f1d72192c9@localhost/knotai --schema knotai --default-role knotai_admin --secret 030d6dbe-0993-4559-9950-24b52eac3456 --token knotai.jwt_token

postgraphile --append-plugins @graphile-contrib/pg-simplify-inflector  --connection postgres://eloyra:admin@localhost/knotai   --schema knotai   --default-role eloyra   --secret 030d6dbe-0993-4559-9950-24b52eac3456   --token knotai.jwt_token

postgraphile --append-plugins @graphile-contrib/pg-simplify-inflector  --connection postgres://eloyra:admin@localhost/knotai   --schema knotai   --default-role eloyra   --secret 030d6dbe-0993-4559-9950-24b52eac3456   --token knotai.jwt_token  --export-schema-json /home/eloyra/TFG/Knotai/docs/Technical/graphql-schema.gql --cors --subscriptions --watch --dynamic-json --no-setof-function-contains-nulls --no-ignore-rbac --no-ignore-indexes --show-error-stack=json --extended-errors hint,detail,errcode --enhance-graphiql --allow-explain --enable-query-batching --legacy-relations omit

postgraphile --append-plugins @graphile-contrib/pg-simplify-inflector  --connection postgres://eloyra:admin@localhost/knotai   --schema knotai   --default-role eloyra   --secret 030d6dbe-0993-4559-9950-24b52eac3456   --token knotai.jwt_token  --export-schema-json /home/eloyra/TFG/Knotai/docs/Technical/graphql-schema.gql --cors --subscriptions --watch --dynamic-json --no-setof-function-contains-nulls --no-ignore-rbac --show-error-stack=json --extended-errors hint,detail,errcode --enhance-graphiql --allow-explain --enable-query-batching --legacy-relations omit

*/


-- FIRST WE MAKE SURE THE REQUIRED SCHEMAS EXIST AND ARE CLEAN
DROP SCHEMA IF EXISTS knotai;
CREATE SCHEMA IF NOT EXISTS knotai;
DROP SCHEMA IF EXISTS knotai_private;
CREATE SCHEMA IF NOT EXISTS knotai_private;


-- WE REVOKE PUBLIC ACCESS FROM ALL OUR FUNCTIONS
alter default privileges revoke execute on functions from public;




-- WE START USING OUR PRIVATE SCHEMA
SET search_path TO knotai_private;


-- AND INSTALL THE REQUIRED EXTENSIONS
DROP EXTENSION IF EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
DROP EXTENSION IF EXISTS "pgcrypto";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";


--//////////////////////////////
-- DATA TYPES
--//////////////////////////////
CREATE TYPE states AS ENUM (
  'new',
  'pending',
  'ongoing',
  'blocked',
  'revision',
  'finished'
);

CREATE TYPE priorities AS ENUM (
  'high',
  'medium',
  'low'
);

CREATE TYPE task_type AS ENUM (
  'analysis',
  'design',
  'front',
  'back',
  'test',
  'systems'
);




-- WE START USING OUR PUBLIC SCHEMA AND DEFINE OUR DB STRUCTURE
SET search_path TO knotai;


--//////////////////////////////
-- DB STRUCTURE
--//////////////////////////////


-- REPOSITORY
create table repository
(
	id uuid default knotai_private.uuid_generate_v4()
		constraint repository_pk
			primary key,
	created_at timestamp default now() not null,
	updated_at timestamp default now() not null,
	url text not null
);

comment on table knotai.repository is 'The version management repository of one or more projects.';

comment on column knotai.repository.id is 'Repository unique identifier.';

comment on column knotai.repository.created_at is 'Indicates the time of creation.';

comment on column knotai.repository.url is 'Repository URL.';


-- PROJECT
create table project
(
	id uuid default knotai_private.uuid_generate_v4()
		constraint project_pk
			primary key,
	created_at timestamp default now() not null,
	updated_at timestamp default now() not null,
	name varchar(20) not null,
	description varchar(120),
	start timestamp,
	finish_estimate timestamp,
	finish_real timestamp,
	wiki text,
	config json,
	state knotai_private.states default 'new' not null,
	priority knotai_private.priorities default 'medium',
	repository uuid
        references knotai.repository
            on update cascade on delete set null
);

comment on table knotai.project is 'A project to manage.';

comment on column knotai.project.id is 'Project unique identifier.';

comment on column knotai.project.created_at is 'Indicates the time of creation.';

comment on column knotai.project.name is 'Project human readable name.';

comment on column knotai.project.description is 'Short description.';

comment on column knotai.project.start is 'Scheduled start time.';

comment on column knotai.project.finish_estimate is 'Scheduled end of project.';

comment on column knotai.project.finish_real is 'Actual project finish date.';

comment on column knotai.project.wiki is 'Project wiki page.';

comment on column knotai.project.config is 'Particular configuration object.';

comment on column knotai.project.state is 'Lifecycle status.';

comment on column knotai.project.priority is 'Project priority.';

create unique index project_name_uindex
	on knotai.project (name);


-- ROLE
create table role
(
	id uuid default knotai_private.uuid_generate_v4()
        constraint role_pk
            primary key,
    created_at timestamp default now() not null,
    updated_at timestamp default now() not null,
	name varchar(30) not null
		unique
);

comment on table role is 'Application roles.';

comment on column role.id is 'Role unique identifier.';

comment on column role.created_at is 'Indicates the time of creation.';

comment on column role.name is 'Unique name.';


-- USER PROFILE
create table user_profile
(
	id uuid default knotai_private.uuid_generate_v4()
		constraint user_pk
			primary key,
	created_at timestamp default now() not null,
	updated_at timestamp default now() not null,
	first_name VARCHAR(80),
	last_name VARCHAR(120) not null,
	picture text,
	role uuid
		references knotai.role
			on update cascade on delete set null
);

comment on table knotai.user_profile is 'User of the application.';

comment on column knotai.user_profile.id is 'User unique identifier.';

comment on column knotai.user_profile.created_at is 'Time of creation.';

comment on column knotai.user_profile.first_name is 'First name.';

comment on column knotai.user_profile.last_name is 'Surname.';

comment on column knotai.user_profile.role is 'User assigned role.';


-- MEMBERS - A MANY-TO-MANY RELATION OF USERS AND PROJECTS
create table members
(
    id uuid default knotai_private.uuid_generate_v4()
        constraint members_pk
            primary key,
	project uuid not null
		constraint members_project_id_fk
			references knotai.project (id)
				on update cascade on delete cascade,
	user_profile uuid not null
		constraint "members_user_profile_id_fk"
			references knotai.user_profile (id)
				on update cascade on delete cascade
);

comment on table members is E'@name UserByProject\nProject members.';

comment on column members.project is 'Project reference.';

comment on column members.user_profile is 'User reference.';


-- SPRINT
create table sprint
(
	id uuid default knotai_private.uuid_generate_v4()
		constraint sprint_pk
			primary key,
	project uuid
		references knotai.project
			on update cascade on delete cascade,
	created_at timestamp default now() not null,
	updated_at timestamp default now() not null,
	name varchar(30) not null,
	description varchar(120),
	start timestamp,
	finish_estimate timestamp,
	finish_real timestamp
);

comment on table sprint is 'Sprint information.';

comment on column sprint.id is 'Sprint unique identifier.';

comment on column sprint.project is 'Project the sprint is associated to.';

comment on column sprint.created_at is 'Indicates the time of creation.';

comment on column sprint.name is 'Name.';

comment on column sprint.description is 'Description.';

comment on column sprint.start is 'Start date.';

comment on column sprint.finish_estimate is 'Deadline.';

comment on column sprint.finish_real is 'Real finish date.';


-- PROFICIENCY
create table proficiency (
    id uuid default knotai_private.uuid_generate_v4()
            constraint proficiency_pk
                primary key,
    created_at timestamp default now() not null,
	updated_at timestamp default now() not null,
    media decimal,
    task_type knotai_private.task_type not null
);

comment on table proficiency is 'Skill level at some task.';

comment on column proficiency.id is 'Proficiency unique identifier.';

comment on column proficiency.media is 'Skill value.';

comment on column proficiency.task_type is 'Task type being measured.';


-- SKILLBOOK - A MANY-TO-MANY RELATION OF USERS AND PROFICIENCIES
create table skillbook (
    id uuid default knotai_private.uuid_generate_v4()
        constraint skillbook_pk
            primary key,
    user_profile uuid not null
            references knotai.user_profile
                on update cascade on delete cascade,
    proficiency uuid not null
            references knotai.proficiency
                on update cascade on delete cascade
);

comment on table skillbook is 'Relation of users and proficiencies.';

comment on column skillbook.user_profile is 'User reference.';

comment on column skillbook.proficiency is 'Proficiency reference.';


-- STORY
create table story (
    id uuid default knotai_private.uuid_generate_v4()
                constraint story_pk
                    primary key,
    sprint uuid not null
            references knotai.sprint
                   on update cascade on delete cascade,
    created_at timestamp not null default now(),
    updated_at timestamp default now() not null,
    name varchar(120) not null,
    description text,
    start timestamp,
    finish_estimate timestamp,
    finish_real timestamp,
    points int not null,
    state knotai_private.states not null default 'new'
);


-- TASK
create table task(
    id uuid default knotai_private.uuid_generate_v4()
                constraint task_pk
                    primary key,
    story uuid
            references knotai.story
                 on update cascade on delete set null,
    created_at timestamp not null default now(),
    updated_at timestamp default now() not null,
    name varchar(120) not null,
    description text,
    type knotai_private.task_type
);


-- TAG
create table tag(
    id uuid default knotai_private.uuid_generate_v4()
        constraint tag_pk
                primary key,
    created_at timestamp not null default now(),
    updated_at timestamp default now() not null,
    label varchar(30) unique not null,
    description text
);


-- TAGBOOK - A MANY-TO-MANY RELATION OF TAGS AND STORIES
create table tagbook(
    id uuid default knotai.knotai_private.uuid_generate_v4()
        constraint tagbook_pk
            primary key,
    tag uuid not null
            references knotai.tag
                 on update cascade on delete cascade,
    story uuid not null
                references knotai.story
                    on update cascade on delete cascade
);

-- COMMENT
create table comment (
    id uuid default knotai_private.uuid_generate_v4()
        constraint comment_pk
                     primary key,
    author uuid
                references knotai.user_profile
                     on update cascade on delete set null,
    story uuid
                references knotai.story
                     on update cascade on delete cascade,
    task uuid
                references knotai.task
                     on update cascade on delete cascade,
    created_at timestamp not null default now(),
    updated_at timestamp default now() not null,
    content text not null
);


-- WORK UNIT
create table work_unit(
    id uuid default knotai_private.uuid_generate_v4()
        constraint work_unit_pk
                primary key,
    author uuid not null
            references knotai.user_profile
                on update cascade on delete set null,
    task uuid not null
            references knotai.task
                    on update cascade on delete cascade,
    created_at timestamp not null default now(),
    updated_at timestamp default now() not null,
    hours decimal not null,
    description varchar(80) not null
);


-- SYSTEM
create table system(
    id uuid default knotai_private.uuid_generate_v4()
        constraint system_pk
                   primary key,
    created_at timestamp not null default now(),
    updated_at timestamp default now() not null,
    config json not null
);




-- AGAIN WE USE OUR PRIVATE SCHEMA
SET search_path TO knotai_private;


-- FOR CREATING A USER PRIVATE TABLE
create table user_account(
    user_id uuid primary key references knotai.user_profile(id) on update cascade on delete cascade,
    email varchar(60) not null unique check (email ~* '^.+@.+\..+$'),
    password_hash varchar(20) not null
);




--//////////////////////////////
-- FUNCTIONS AND TRIGGERS
--//////////////////////////////


-- WE DEFINE A FUNCTION FOR REFRESHING THE UPDATED_AT FIELD OF OUR TABLES
create function set_updated_at() returns trigger as $$
    begin
        new.updated_at := current_timestamp;
        return new;
    end;
$$ language plpgsql;

comment on function knotai_private.set_updated_at() is 'Function that returns current time as timestamp.';


-- WE RETURN TO THE PUBLIC SCHEMA
SET search_path TO knotai;


-- WE DEFINE ALL TRIGGERS FOR UPDATE_AT
create trigger repository_updated_at before update
    on knotai.repository
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger project_updated_at before update
    on knotai.project
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger role_updated_at before update
    on knotai.role
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger user_updated_at before update
    on knotai.user_profile
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger sprint_updated_at before update
    on knotai.sprint
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger proficiency_updated_at before update
    on knotai.proficiency
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger story_updated_at before update
    on knotai.story
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger task_updated_at before update
    on knotai.task
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger tag_updated_at before update
    on knotai.tag
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger comment_updated_at before update
    on knotai.comment
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger work_unit_updated_at before update
    on knotai.work_unit
    for each row
    execute procedure knotai_private.set_updated_at();
create trigger system_updated_at before update
    on knotai.system
    for each row
    execute procedure knotai_private.set_updated_at();


-- DEFINE A FUNCTION FOR GETTING USER FULL NAME
create function user_full_name("user" knotai.user_profile) returns text as $$
    select "user".first_name || ' ' || "user".last_name
$$ language sql stable;

comment on function knotai.user_full_name(knotai.user_profile) is 'A user full name which is a concatenation of their first and last names.';


-- AND DEFINE A FUNCTION FOR REGISTERING A USER
create function register_user(
    first_name varchar(80),
    last_name varchar(120),
    email varchar(60),
    password varchar(20)
) returns knotai.user_profile as $$
declare
    "user" knotai.user_profile;
begin

    insert into knotai.user_profile (first_name, last_name) values
        (first_name, last_name)
        returning * into "user";

    insert into knotai_private.user_account (user_id, email, password_hash) values
        ("user".id, email, knotai_private.crypt(password, knotai_private.gen_salt('bf')));

    return "user";
end;
$$ language plpgsql strict security definer;

comment on function knotai.register_user(varchar, varchar, varchar, varchar) is 'Registers a user creating their private account and hashing the password.';




--//////////////////////////////
-- ROLES AND AUTHENTICATION
--//////////////////////////////


-- WE CREATE A GENERAL PURPOSE DB ACCESS ROLE FOR POSTGRAPHILE API
create role knotai_postgraphile login password '90042f76-2fb1-4891-892a-63f1d72192c9';


-- WE CREATE AN ANONYMOUS ROLE
create role knotai_anonymous;
grant knotai_anonymous to knotai_postgraphile;
-- AN EXTERNAL USER ROLE
create role knotai_ext;
grant knotai_ext to knotai_postgraphile;
-- A DEVELOPER ROLE
create role knotai_dev;
grant knotai_dev to knotai_postgraphile;
-- A COORDINATOR ROLE
create role knotai_coord;
grant knotai_coord to knotai_postgraphile;
-- A PROJECT MANAGER ROLE
create role knotai_man;
grant knotai_man to knotai_postgraphile;
-- AN APP MODERATOR ROLE
create role knotai_mod;
grant knotai_mod to knotai_postgraphile;
-- AN APP ADMIN ROLE
create role knotai_admin;
grant knotai_admin to knotai_postgraphile;


-- NOW WE DEFINE OUR JWT TOKEN
create type jwt_token as (
    "role" text,
    user_id uuid,
    exp bigint
);


-- AND THE FUNCTION THAT WILL RETURN IT
create function authenticate(
    email varchar(60),
    password varchar(20)
) returns knotai.jwt_token as $$

    declare
        account knotai_private.user_account;
    begin
        select a.* into account
        from knotai_private.user_account as a
        where a.email = $1;

        if account.password_hash = knotai_private.crypt(password, account.password_hash) then
            return ('knotai_postgraphile', account.user_id, extract(epoch from (now() + interval '2 days')))::knotai.jwt_token;
        else
            return null;
        end if;
    end;
$$ language plpgsql strict security definer;

comment on function knotai.authenticate(varchar, varchar) is 'Creates a JWT token to safely identify a user and give them access to the application.';

/*
 postgraphile \
  --connection postgres://forum_example_postgraphile:xyz@localhost/mydb \
  --default-role forum_example_anonymous
  --token knotai.jwt_token
 */


-- FUNCTION FOR RETRIEVING CURRENT AUTHENTICATED USER
create function "current_user"() returns knotai.user_profile as $$
    select *
    from knotai.user_profile
    where id = nullif(current_setting('jwt.claims.user_id', true), '')::uuid
$$ language sql stable;

comment on function knotai."current_user"() is 'Get the user who is currently authenticated.';


create function check_project_membership(
    user_id uuid,
    project_id uuid
) returns boolean as $$
        declare
            cup integer;
        begin
            select count(up.*) into cup
            from knotai.members as up
            where up.user_profile = user_id and up.project = project_id;

            if cup = 1 then return true;
            else return false;
            end if;
        end;
$$ language plpgsql strict security definer;


-- WE FINE-GRAIN OUR ACCESS PERMISSIONS
grant usage on schema knotai to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;


-- PENDING DEFINITION
-- REPOSITORY PERMISSIONS
grant select on table knotai.repository to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant insert, update, delete on table knotai.repository to knotai_coord, knotai_man, knotai_admin;
alter table knotai.repository enable row level security;
create policy select_repository on knotai.repository for select
    using (true);
create policy insert_repository on knotai.repository for insert to knotai_admin, knotai_coord, knotai_man
    with check (true);
create policy update_repository_any on knotai.repository for update to knotai_admin
    using (true);
create policy update_repository_ifmember on knotai.repository for update to knotai_coord, knotai_man
    using
    (
        knotai.check_project_membership(
            nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
            (
                select p.id from knotai.project as p where p.repository = id
            )
        )
    );
create policy delete_repository_any on knotai.repository for delete to knotai_admin
    using (true);
create policy delete_repository_ifmember on knotai.repository for delete to knotai_man
    using
    (
        knotai.check_project_membership(
            nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
            (
                select p.id from knotai.project as p where p.repository = id
            )
        )
    );

-- PROJECT PERMISSIONS
grant select on table knotai.project to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant insert, update, delete on table knotai.project to knotai_coord, knotai_man, knotai_admin;
alter table knotai.project enable row level security;
create policy select_project on knotai.project for select
    using (true);
create policy insert_project on knotai.project for insert to knotai_admin, knotai_man
    with check (true);
create policy update_project_any on knotai.project for update to knotai_admin
    using (true);
create policy update_project_ifmember on knotai.project for update to knotai_coord, knotai_man
    using
    (
        knotai.check_project_membership(
            nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
            id
        )
    );
create policy delete_project_any on knotai.project for delete to knotai_admin
    using (true);
create policy delete_project_current on knotai.project for delete to knotai_man
    using
    (
        knotai.check_project_membership(
            nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
            id
        )
    );


-- ROLE PERMISSIONS
/*grant select on table knotai.role to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.role to kknotai_mod, knotai_admin;
alter table knotai.role enable row level security;
create policy select_role on knotai.role for select
    using (true);
create policy update_role_any on knotai.role for update to knotai_admin
    using (true);
create policy update_role_current on knotai.role for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_role_any on knotai.role for delete to knotai_admin
    using (true);
create policy delete_role_current on knotai.role for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- USER_PROFILE PERMISSIONS
grant select on table knotai.user_profile to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.user_profile to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.user_profile enable row level security;
create policy select_user_profile on knotai.user_profile for select
    using (true);
create policy update_user_profile_any on knotai.user_profile for update to knotai_admin
    using (true);
create policy update_user_profile_current on knotai.user_profile for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_user_profile_any on knotai.user_profile for delete to knotai_admin
    using (true);
create policy delete_user_profile_current on knotai.user_profile for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- MEMBERS PERMISSIONS
grant select on table knotai.members to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.members to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.members enable row level security;
create policy select_members on knotai.members for select
    using (true);
create policy update_members_any on knotai.members for update to knotai_admin
    using (true);
create policy update_members_current on knotai.members for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_members_any on knotai.members for delete to knotai_admin
    using (true);
create policy delete_members_current on knotai.members for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- SPRINT PERMISSIONS
grant select on table knotai.sprint to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.sprint to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.sprint enable row level security;
create policy select_sprint on knotai.sprint for select
    using (true);
create policy update_sprint_any on knotai.sprint for update to knotai_admin
    using (true);
create policy update_sprint_current on knotai.sprint for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_sprint_any on knotai.sprint for delete to knotai_admin
    using (true);
create policy delete_sprint_current on knotai.sprint for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- PROFICIENCY PERMISSIONS
grant select on table knotai.proficiency to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.proficiency to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.proficiency enable row level security;
create policy select_proficiency on knotai.proficiency for select
    using (true);
create policy update_proficiency_any on knotai.proficiency for update to knotai_admin
    using (true);
create policy update_proficiency_current on knotai.proficiency for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_proficiency_any on knotai.proficiency for delete to knotai_admin
    using (true);
create policy delete_proficiency_current on knotai.proficiency for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- PROFICIENCIES PERMISSIONS
grant select on table knotai.proficiencies to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.proficiencies to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.proficiencies enable row level security;
create policy select_proficiencies on knotai.proficiencies for select
    using (true);
create policy update_proficiencies_any on knotai.proficiencies for update to knotai_admin
    using (true);
create policy update_proficiencies_current on knotai.proficiencies for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_proficiencies_any on knotai.proficiencies for delete to knotai_admin
    using (true);
create policy delete_proficiencies_current on knotai.proficiencies for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- STORY PERMISSIONS
grant select on table knotai.story to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.story to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.story enable row level security;
create policy select_story on knotai.story for select
    using (true);
create policy update_story_any on knotai.story for update to knotai_admin
    using (true);
create policy update_story_current on knotai.story for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_story_any on knotai.story for delete to knotai_admin
    using (true);
create policy delete_story_current on knotai.story for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- TASK PERMISSIONS
grant select on table knotai.task to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.task to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.task enable row level security;
create policy select_task on knotai.task for select
    using (true);
create policy update_task_any on knotai.task for update to knotai_admin
    using (true);
create policy update_task_current on knotai.task for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_task_any on knotai.task for delete to knotai_admin
    using (true);
create policy delete_task_current on knotai.task for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- TAG PERMISSIONS
grant select on table knotai.tag to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.tag to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.tag enable row level security;
create policy select_tag on knotai.tag for select
    using (true);
create policy update_tag_any on knotai.tag for update to knotai_admin
    using (true);
create policy update_tag_current on knotai.tag for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_tag_any on knotai.tag for delete to knotai_admin
    using (true);
create policy delete_tag_current on knotai.tag for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- TAGS PERMISSIONS
grant select on table knotai.tags to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.tags to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.tags enable row level security;
create policy select_tags on knotai.tags for select
    using (true);
create policy update_tags_any on knotai.tags for update to knotai_admin
    using (true);
create policy update_tags_current on knotai.tags for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_tags_any on knotai.tags for delete to knotai_admin
    using (true);
create policy delete_tags_current on knotai.tags for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- COMMENT PERMISSIONS
grant select on table knotai.comment to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.comment to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.comment enable row level security;
create policy select_comment on knotai.comment for select
    using (true);
create policy update_comment_any on knotai.comment for update to knotai_admin
    using (true);
create policy update_comment_current on knotai.comment for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_comment_any on knotai.comment for delete to knotai_admin
    using (true);
create policy delete_comment_current on knotai.comment for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- WORK_UNIT PERMISSIONS
grant select on table knotai.work_unit to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.work_unit to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.work_unit enable row level security;
create policy select_work_unit on knotai.work_unit for select
    using (true);
create policy update_work_unit_any on knotai.work_unit for update to knotai_admin
    using (true);
create policy update_work_unit_current on knotai.work_unit for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_work_unit_any on knotai.work_unit for delete to knotai_admin
    using (true);
create policy delete_work_unit_current on knotai.work_unit for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- SYSTEM PERMISSIONS
grant select on table knotai.system to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.system to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.system enable row level security;
create policy select_system on knotai.system for select
    using (true);
create policy update_system_any on knotai.system for update to knotai_admin
    using (true);
create policy update_system_current on knotai.system for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_system_any on knotai.system for delete to knotai_admin
    using (true);
create policy delete_system_current on knotai.system for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);

-- USER_ACCOUNT PERMISSIONS
grant select on table knotai.user_account to knotai_anonymous, knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
grant update, delete on table knotai.user_account to knotai_dev, knotai_coord, knotai_man, knotai_mod, knotai_admin;
alter table knotai.user_account enable row level security;
create policy select_user_account on knotai.user_account for select
    using (true);
create policy update_user_account_any on knotai.user_account for update to knotai_admin
    using (true);
create policy update_user_account_current on knotai.user_account for update to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
create policy delete_user_account_any on knotai.user_account for delete to knotai_admin
    using (true);
create policy delete_user_account_current on knotai.user_account for delete to knotai_dev, knotai_coord, knotai_man, knotai_mod
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::integer);
*/