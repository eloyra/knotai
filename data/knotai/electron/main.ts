import { app, BrowserWindow, ipcMain } from 'electron';
import * as path from 'path';
import * as url from 'url';

require('electron-reload')(path.join(__dirname, `../../dist/knotai/index.html`));

let win: BrowserWindow;

app.on('ready', createWindow);

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});

function createWindow() {
  win = new BrowserWindow({ show: false, frame: true });

  win.once('ready-to-show', () => {
    win.show();
  });

  // win.setMenu(null);

  // win.removeMenu();

  win.setMenuBarVisibility(false);

  /*win.loadURL(
    url.format({
      pathname: path.join(__dirname, `../../dist/knotai/index.html`),
      protocol: 'file:',
      slashes: true,
    })
  );*/

  win.loadURL('http://localhost:4200/');



  win.webContents.openDevTools();

  win.on('closed', () => {
    win = null;
  });
}

/*function openModal() {
  const { BrowserWindow } = require('electron');
  const modal = new BrowserWindow({ parent: win, modal: true, show: false });
  modal.loadURL('https://www.sitepoint.com');
  modal.once('ready-to-show', () => {
    modal.show();
  });
}

ipcMain.on('openModal', (event, arg) => {
  openModal();
});*/

