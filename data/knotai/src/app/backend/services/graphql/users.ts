import gql from 'graphql-tag';
import { Injectable, NgIterable, QueryList } from '@angular/core';
import { Uuid } from '../../../shared/types/uuid';
import { Mutation, Query } from 'apollo-angular';

export interface Item {
  id: Uuid;
  createdAt?: any;
  updatedAt?: any;
  firstName: string;
  lastName: string;
  picture?: any;
  role?: {id: Uuid, name: string};
}

export interface Pagination {
  users?: Item[];
  total?: number;
  pagination: {};
  edges: {};
}

export type Wrapper = NgIterable<Pagination> | QueryList<Pagination>;

const GetUsersDocument = gql`
  query getUsers {
    userProfiles {
      totalCount
      nodes {
        id
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetUsersAllInfoDocument = gql`
  query getUsersAllInfo {
    userProfiles {
      totalCount
      nodes {
        id
        createdAt
        updatedAt
        firstName,
        lastName,
        picture,
        role: roleByRole {
          id,
          name
        }
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetUserDocument = gql`
  query getUser($id: UUID!) {
    userProfile(id: $id) {
      id
      createdAt
      updatedAt
      firstName,
      lastName,
      picture,
      role: roleByRole {
        id,
        name
      }
    }
  }
`;

const CreateUserDocument = gql`
  mutation createUser(
    $firstName: String!,
    $lastName: String!,
    $picture: String,
    $role: UUID
  ) {
    createUserProfile(input: {userProfile: {
      firstName: $firstName,
      lastName: $lastName,
      picture: $picture,
      role: $role,
    }}) {
      userProfile {
        id,
        firstName,
        lastName,
        picture,
        role: roleByRole {
          id,
          name
        }
      }
    }
  }
`;

const UpdateUserDocument = gql`
  mutation updateUser(
    $id: UUID!,
    $firstName: String,
    $lastName: String,
    $picture: String,
    $role: UUID
  ) {
    updateUserProfile(input: {id: $id, patch: {
      firstName: $firstName,
      lastName: $lastName,
      picture: $picture,
      role: $role,
    }}) {
      userProfile {
        id,
        firstName,
        lastName,
        picture,
        role: roleByRole {
          id,
          name
        }
      }
    }
  }
`;

const DeleteUserDocument = gql`
  mutation deleteUser($id: UUID!) {
    deleteUserProfile(input: {id: $id}) {
      deletedUserProfileNodeId,
      userProfile {
        id
      }
    }
  }
`;


@Injectable({
  providedIn: 'root'
})
export class GetMany extends Query<any, {}> {
  document = GetUsersDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetManyAllInfo extends Query<any, {}> {
  document = GetUsersAllInfoDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetOne extends Query<any, { id: Uuid }> {
  document = GetUserDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Create extends Mutation<any, {}> {
  document = CreateUserDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Update extends Mutation<any, {
  id: Uuid,
  firstName: string,
  lastName: string,
  picture: string,
  role: {id: Uuid, name: string}
}> {
  document = UpdateUserDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Delete extends Mutation<any, { id: Uuid }> {
  document = DeleteUserDocument;
}
