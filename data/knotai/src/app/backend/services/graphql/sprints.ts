import gql from 'graphql-tag';
import { Injectable, NgIterable, QueryList } from '@angular/core';
import { Uuid } from '../../../shared/types/uuid';
import { Mutation, Query } from 'apollo-angular';
import {StoryAPI} from './index';

export interface Item {
  id: Uuid;
  name?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
  start?: Date;
  finishEstimate?: Date;
  finishReal?: Date;
  project?: Uuid;
  stories?: StoryAPI.Type;
}

export interface Pagination {
  sprints?: Item[];
  total?: number;
  pagination: {};
  edges: {};
}

export interface Type {
  id?: Uuid;
  totalCount?: number;
  nodes?: Item[];
}

export type Wrapper = NgIterable<Pagination> | QueryList<Pagination>;

const GetSprintsDocument = gql`
  query getSprints {
    sprints {
      totalCount
      nodes {
        id
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetSprintsAllInfoDocument = gql`
  query getSprintsAllInfo {
    sprints {
      totalCount
      nodes {
        id
        name
        description
        createdAt
        updatedAt
        start
        finishEstimate
        finishReal
        project
        stories: storiesBySprint {
          totalCount
          nodes {
            id
          }
        }
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetSprintDocument = gql`
  query getSprint($id: UUID!) {
    sprint(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
      start
      finishEstimate
      finishReal
      project
      stories: storiesBySprint {
        totalCount
        nodes {
          id
        }
      }
    }
  }
`;

const CreateSprintDocument = gql`
  mutation createSprint(
    $name: String!,
    $desc: String,
    $start: Datetime,
    $finishEstimate: Datetime,
    $finishReal: Datetime,
    $project: UUID!
  ) {
    createSprint(input: {sprint: {
      name: $name,
      description: $desc,
      start: $start,
      finishEstimate: $finishEstimate,
      finishReal: $finishReal,
      project: $project
    }}) {
      sprint {
        id,
        name,
        description,
        start,
        finishEstimate,
        finishReal,
        project
        stories: storiesBySprint {
          totalCount
          nodes {
            id
          }
        }
      }
    }
  }
`;

const UpdateSprintDocument = gql`
  mutation updateSprint(
    $id: UUID!,
    $name: String,
    $desc: String,
    $start: Datetime,
    $finishEstimate: Datetime,
    $finishReal: Datetime,
    $project: UUID
  ) {
    updateSprint(input: {id: $id, patch: {
      name: $name,
      description: $desc,
      start: $start,
      finishEstimate: $finishEstimate,
      finishReal: $finishReal,
      project: $project
    }}) {
      sprint {
        id
        name
        description
        start
        finishEstimate
        finishReal
        project
        stories: storiesBySprint {
          totalCount
          nodes {
            id
          }
        }
      }
    }
  }
`;

const DeleteSprintDocument = gql`
  mutation deleteSprint($id: UUID!) {
    deleteSprint(input: {id: $id}) {
      deletedSprintNodeId,
      sprint {
        id
      }
    }
  }
`;


@Injectable({
  providedIn: 'root'
})
export class GetMany extends Query<any, {}> {
  document = GetSprintsDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetManyAllInfo extends Query<any, {}> {
  document = GetSprintsAllInfoDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetOne extends Query<any, { id: Uuid }> {
  document = GetSprintDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Create extends Mutation<any, {}> {
  document = CreateSprintDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Update extends Mutation<any, {
  id: Uuid,
  name: string,
  desc: string,
  start: any,
  finishEstimate: any,
  finishReal: any,
  project: Uuid
}> {
  document = UpdateSprintDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Delete extends Mutation<any, { id: Uuid }> {
  document = DeleteSprintDocument;
}
