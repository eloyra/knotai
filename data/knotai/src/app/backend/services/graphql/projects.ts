import gql from 'graphql-tag';
import { Injectable, NgIterable, QueryList } from '@angular/core';
import { Uuid } from '../../../shared/types/uuid';
import { Mutation, Query } from 'apollo-angular';
import { StateType } from '../../../shared/types/state';
import { PriorityType } from '../../../shared/types/priority';
import {RepositoryAPI, SprintAPI} from './index';

export interface Item {
  id: Uuid;
  name?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
  start?: Date;
  finishEstimate?: Date;
  finishReal?: Date;
  state?: StateType;
  sprints?: SprintAPI.Type;
  priority?: PriorityType;
  repository?: RepositoryAPI.Type;
  wiki?: string;
}

export interface Pagination {
  projects?: Item[];
  total?: number;
  pagination: {};
  edges: {};
}

export type Wrapper = NgIterable<Pagination> | QueryList<Pagination>;

const GetProjectsDocument = gql`
  query getProjects {
    projects {
      totalCount
      nodes {
        id
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetProjectsAllInfoDocument = gql`
  query getProjectsAllInfo {
    projects {
      totalCount
      nodes {
        id
        name
        description
        createdAt
        updatedAt
        start
        finishEstimate
        finishReal
        state
        sprints: sprintsByProject {
          totalCount
          nodes {
            id
          }
        }
        priority
        repository: repositoryByRepository {
          id
          createdAt
          updatedAt
          url
        }
        wiki
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetProjectDocument = gql`
  query getProject($id: UUID!) {
    project(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
      start
      finishEstimate
      finishReal
      state
      sprints: sprintsByProject {
        totalCount
        nodes {
          id
        }
      }
      priority
      repository: repositoryByRepository {
        id
        createdAt
        updatedAt
        url
      }
      wiki
    }
  }
`;

const GetProjectWithSprintNamesDocument = gql`
  query getProject($id: UUID!) {
    project(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
      start
      finishEstimate
      finishReal
      state
      sprints: sprintsByProject {
        totalCount
        nodes {
          id
          name
        }
      }
      priority
      repository: repositoryByRepository {
        id
        createdAt
        updatedAt
        url
      }
      wiki
    }
  }
`;

const GetProjectInDepthDocument = gql`
  query getProjectInDepth($id: UUID!) {
    project(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
      start
      finishEstimate
      finishReal
      state
      sprints: sprintsByProject {
        totalCount
        nodes {
          id
          createdAt
          updatedAt
          name
          description
          start
          finishEstimate
          finishReal
          stories: storiesBySprint {
            totalCount
            nodes {
              id
              createdAt
              updatedAt
              name
              description
              start
              finishEstimate
              finishReal
              state
              points
              tasks: tasksByStory {
                totalCount
                nodes {
                  id
                  createdAt
                  updatedAt
                  name
                  description
                  type
                }
              }
            }
          }
        }
      }
      priority
      repository: repositoryByRepository {
        id
        createdAt
        updatedAt
        url
      }
      wiki
    }
  }
`;

const CreateProjectDocument = gql`
  mutation createProject(
    $name: String!,
    $desc: String,
    $start: Datetime,
    $finishEstimate: Datetime,
    $finishReal: Datetime,
    $state: States,
    $priority: Priorities,
    $repo: UUID,
    $wiki: String,
  ) {
    createProject(input: {project: {
      name: $name,
      description: $desc,
      start: $start,
      finishEstimate: $finishEstimate,
      finishReal: $finishReal,
      state: $state,
      priority: $priority,
      repository: $repo,
      wiki: $wiki
    }}) {
      project {
        id,
        name,
        description,
        start,
        finishEstimate,
        finishReal,
        state,
        priority,
        repository: repositoryByRepository {
          id,
          createdAt,
          updatedAt,
          url
        },
        wiki
      }
    }
  }
`;

const UpdateProjectDocument = gql`
  mutation updateProject(
    $id: UUID!,
    $name: String,
    $desc: String,
    $start: Datetime,
    $finishEstimate: Datetime,
    $finishReal: Datetime,
    $state: States,
    $priority: Priorities,
    $repo: UUID,
    $wiki: String,
  ) {
    updateProject(input: {id: $id, patch: {
      name: $name,
      description: $desc,
      start: $start,
      finishEstimate: $finishEstimate,
      finishReal: $finishReal,
      state: $state,
      priority: $priority,
      repository: $repo,
      wiki: $wiki
    }}) {
      project {
        id,
        name,
        description,
        start,
        finishEstimate,
        finishReal,
        state,
        priority,
        repository: repositoryByRepository {
          id,
          createdAt,
          updatedAt,
          url
        },
        wiki
      }
    }
  }
`;

const DeleteProjectDocument = gql`
  mutation deleteProject($id: UUID!) {
    deleteProject(input: {id: $id}) {
      deletedProjectNodeId,
      project {
        id
      }
    }
  }
`;


@Injectable({
  providedIn: 'root'
})
export class GetMany extends Query<any, {}> {
  document = GetProjectsDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetManyAllInfo extends Query<any, {}> {
  document = GetProjectsAllInfoDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetOne extends Query<any, { id: Uuid }> {
  document = GetProjectDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetOneWithSprintNames extends Query<any, { id: Uuid }> {
  document = GetProjectWithSprintNamesDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetOneFull extends Query<any, { id: Uuid }> {
  document = GetProjectInDepthDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Create extends Mutation<any, {}> {
  document = CreateProjectDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Update extends Mutation<any, {
  id: Uuid,
  name: string,
  desc: string,
  start: any,
  finishEstimate: any,
  finishReal: any,
  state: StateType,
  priority: PriorityType,
  repo: Uuid,
  wiki: string,
}> {
  document = UpdateProjectDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Delete extends Mutation<any, { id: Uuid }> {
  document = DeleteProjectDocument;
}
