import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import { Uuid } from '../../../shared/types/uuid';
import { Mutation, Query } from 'apollo-angular';

export interface Item {
  id: Uuid;
  createdAt?: any;
  updatedAt?: any;
  url: string;
}

export interface Type {
  id: Uuid;
  createdAt?: any;
  updatedAt?: any;
  url: string;
}

const GetRepositoryDocument = gql`
  query getRepository($id: UUID!) {
    repository(id: $id) {
      id
      createdAt
      updatedAt
      url
    }
  }
`;

const CreateRepositoryDocument = gql`
  mutation createRepository(
    $url: String!
  ) {
    createRepository(input: {repository: {
      url: $url
    }}) {
      repository {
        id
      }
    }
  }
`;

const UpdateRepositoryDocument = gql`
  mutation updateRepository(
    $id: UUID!,
    $url: String!
  ) {
    updateRepository(input: {id: $id, patch: {
      url: $url
    }}) {
      repository {
        id,
        createdAt,
        updatedAt,
        url
      }
    }
  }
`;

@Injectable({
  providedIn: 'root'
})
export class GetOne extends Query<any, { id: Uuid }> {
  document = GetRepositoryDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Create extends Mutation<any, { url: string }> {
  document = CreateRepositoryDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Update extends Mutation<any, {
  id: Uuid,
  url: string
}> {
  document = UpdateRepositoryDocument;
}
