import gql from 'graphql-tag';
import { Injectable, NgIterable, QueryList } from '@angular/core';
import { Uuid } from '../../../shared/types/uuid';
import { Mutation, Query } from 'apollo-angular';
import {StoryAPI} from './index';
import {TaskType} from '../../../shared/types/task-type';

export interface Item {
  id: Uuid;
  name?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
  type: TaskType;
  story: StoryAPI.Type;
}

export interface Pagination {
  tasks?: Item[];
  total?: number;
  pagination: {};
  edges: {};
}

export interface Type {
  id?: Uuid;
  totalCount?: number;
  nodes?: Item[];
}

export type Wrapper = NgIterable<Pagination> | QueryList<Pagination>;

const GetTasksDocument = gql`
  query getTasks {
    tasks {
      totalCount
      nodes {
        id
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetTasksAllInfoDocument = gql`
  query getTasksAllInfo {
    tasks {
      totalCount
      nodes {
        id
        name
        description
        createdAt
        updatedAt
        type
        story
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetTaskDocument = gql`
  query getTask($id: UUID!) {
    task(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
      type
      story
    }
  }
`;

const CreateTaskDocument = gql`
  mutation createTask(
    $name: String!,
    $desc: String,
    $type: TaskType,
    $story: UUID!
  ) {
    createTask(input: {task: {
      name: $name,
      description: $desc,
      type: $type,
      story: $story
    }}) {
      task {
        id
        name
        description
        createdAt
        updatedAt
        type
        story
      }
    }
  }
`;

const UpdateTaskDocument = gql`
  mutation updateTask(
    $id: UUID!,
    $name: String!,
    $desc: String,
    $type: TaskType,
    $story: UUID!
  ) {
    updateTask(input: {id: $id, patch: {
      name: $name,
      description: $desc,
      type: $type,
      story: $story
    }}) {
      task {
        id
        name
        description
        createdAt
        updatedAt
        type
        story
      }
    }
  }
`;

const DeleteTaskDocument = gql`
  mutation deleteTask($id: UUID!) {
    deleteTask(input: {id: $id}) {
      deletedTaskNodeId,
      task {
        id
      }
    }
  }
`;


@Injectable({
  providedIn: 'root'
})
export class GetMany extends Query<any, {}> {
  document = GetTasksDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetManyAllInfo extends Query<any, {}> {
  document = GetTasksAllInfoDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetOne extends Query<any, { id: Uuid }> {
  document = GetTaskDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Create extends Mutation<any, {}> {
  document = CreateTaskDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Update extends Mutation<any, {
  id: Uuid;
  name?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
  type?: TaskType;
  story?: StoryAPI.Type;
}> {
  document = UpdateTaskDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Delete extends Mutation<any, { id: Uuid }> {
  document = DeleteTaskDocument;
}
