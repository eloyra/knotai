import gql from 'graphql-tag';
import { Injectable, NgIterable, QueryList } from '@angular/core';
import { Uuid } from '../../../shared/types/uuid';
import { Mutation, Query } from 'apollo-angular';
import { StateType } from '../../../shared/types/state';
import {SprintAPI, TaskAPI} from './index';

export interface Item {
  id: Uuid;
  name?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
  start?: Date;
  finishEstimate?: Date;
  finishReal?: Date;
  state?: StateType;
  points: number;
  sprint: SprintAPI.Type;
  tasks?: TaskAPI.Type;
}

export interface Pagination {
  stories?: Item[];
  total?: number;
  pagination: {};
  edges: {};
}

export interface Type {
  id?: Uuid;
  totalCount?: number;
  nodes?: Item[];
}

export type Wrapper = NgIterable<Pagination> | QueryList<Pagination>;

const GetStoriesDocument = gql`
  query getStories {
    stories {
      totalCount
      nodes {
        id
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetStoriesAllInfoDocument = gql`
  query getStoriesAllInfo {
    stories {
      totalCount
      nodes {
        id
        name
        description
        createdAt
        updatedAt
        start
        finishEstimate
        finishReal
        state
        points
        sprint
        tasks: tasksByStory {
          totalCount
          nodes {
            id
          }
        }
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetStoryDocument = gql`
  query getStory($id: UUID!) {
    story(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
      start
      finishEstimate
      finishReal
      state
      points
      sprint
      tasks: tasksByStory {
        totalCount
        nodes {
          id
        }
      }
    }
  }
`;

const CreateStoryDocument = gql`
  mutation createStory(
    $name: String!,
    $desc: String,
    $start: Datetime,
    $finishEstimate: Datetime,
    $finishReal: Datetime,
    $state: States,
    $points: Int!,
    $sprint: UUID!
  ) {
    createStory(input: {story: {
      name: $name,
      description: $desc,
      start: $start,
      finishEstimate: $finishEstimate,
      finishReal: $finishReal,
      state: $state,
      points: $points,
      sprint: $sprint
    }}) {
      story {
        id
        name
        description
        start
        finishEstimate
        finishReal
        state
        sprint
        tasks: tasksByStory {
          totalCount
          nodes {
            id
          }
        }
      }
    }
  }
`;

const UpdateStoryDocument = gql`
  mutation updateStory(
    $id: UUID!,
    $name: String!,
    $desc: String,
    $start: Datetime,
    $finishEstimate: Datetime,
    $finishReal: Datetime,
    $state: States,
    $points: Int!,
    $sprint: UUID!
  ) {
    updateStory(input: {id: $id, patch: {
      name: $name,
      description: $desc,
      start: $start,
      finishEstimate: $finishEstimate,
      finishReal: $finishReal,
      state: $state,
      points: $points,
      sprint: $sprint
    }}) {
      story {
        id
        name
        description
        start
        finishEstimate
        finishReal
        state
        sprint
        tasks: tasksByStory {
          totalCount
          nodes {
            id
          }
        }
      }
    }
  }
`;

const DeleteStoryDocument = gql`
  mutation deleteStory($id: UUID!) {
    deleteStory(input: {id: $id}) {
      deletedStoryNodeId,
      story {
        id
      }
    }
  }
`;


@Injectable({
  providedIn: 'root'
})
export class GetMany extends Query<any, {}> {
  document = GetStoriesDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetManyAllInfo extends Query<any, {}> {
  document = GetStoriesAllInfoDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetOne extends Query<any, { id: Uuid }> {
  document = GetStoryDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Create extends Mutation<any, {}> {
  document = CreateStoryDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Update extends Mutation<any, {
  id: Uuid,
  name: string,
  desc: string,
  start: any,
  finishEstimate: any,
  finishReal: any,
  project: Uuid
}> {
  document = UpdateStoryDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Delete extends Mutation<any, { id: Uuid }> {
  document = DeleteStoryDocument;
}
