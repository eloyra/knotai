import gql from 'graphql-tag';
import { Injectable, NgIterable, QueryList } from '@angular/core';
import { Uuid } from '../../../shared/types/uuid';
import { Query } from 'apollo-angular';

export interface Item {
  id: Uuid;
  createdAt?: Date;
  updatedAt?: Date;
  name: string;
}

export interface Pagination {
  roles?: Item[];
  total?: number;
  pagination: {};
  edges: {};
}

export type Wrapper = NgIterable<Pagination> | QueryList<Pagination>;

const GetRolesDocument = gql`
  query getRoles {
    roles {
      totalCount
      nodes {
        id
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;

const GetRolesAllInfoDocument = gql`
  query getRolesAllInfo {
    roles {
      totalCount
      nodes {
        id
        createdAt
        updatedAt
        name
      }
      edges {
        node {
          id
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;



@Injectable({
  providedIn: 'root'
})
export class GetMany extends Query<any, {}> {
  document = GetRolesDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetManyAllInfo extends Query<any, {}> {
  document = GetRolesAllInfoDocument;
}
