import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagementOverviewComponent } from './management-overview/management-overview.component';


const routes: Routes = [
  {
    path: 'management',
    children: [
      {
        path: '',
        component: ManagementOverviewComponent
      },
      {
        path: 'overview',
        component: ManagementOverviewComponent
      },
      {
        path: '**',
        component: ManagementOverviewComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule { }
