import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagementRoutingModule } from './management-routing.module';
import { ManagementOverviewComponent } from './management-overview/management-overview.component';
import { MatIconModule } from '@angular/material';


@NgModule({
  declarations: [ManagementOverviewComponent],
  imports: [
    CommonModule,
    ManagementRoutingModule,
    MatIconModule
  ],
  exports: [ManagementOverviewComponent]
})
export class ManagementModule { }
