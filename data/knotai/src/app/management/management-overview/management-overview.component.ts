import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { KnotaiProjectModalComponent } from '../../shared/components/knotai-project-modal/knotai-project-modal.component';

@Component({
  selector: 'app-management-overview',
  templateUrl: './management-overview.component.html',
  styleUrls: ['./management-overview.component.scss']
})
export class ManagementOverviewComponent implements OnInit {

  constructor(
    public projectDialog: MatDialog
  ) { }

  ngOnInit() {
  }

  newProject() {
    this.projectDialog.open(KnotaiProjectModalComponent, {
      height: 'auto',
      width: 'auto',
      maxWidth: '80%',
      panelClass: 'project-modal',
    });
  }

}
