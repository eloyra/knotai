import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrationRoutingModule } from './administration-routing.module';
import { AdministerProjectsComponent } from './administer-projects/administer-projects.component';
import {
  MatFormFieldModule, MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { AdministerUsersComponent } from './administer-users/administer-users.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [AdministerProjectsComponent, AdministerUsersComponent],
    imports: [
        CommonModule,
        AdministrationRoutingModule,
        MatTableModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatInputModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatIconModule,
        SharedModule
    ],
  exports: [AdministerProjectsComponent, AdministerUsersComponent]
})
export class AdministrationModule { }
