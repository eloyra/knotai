import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministerProjectsComponent } from './administer-projects/administer-projects.component';
import { AdministerUsersComponent } from './administer-users/administer-users.component';


const routes: Routes = [
  {
    path: 'administration',
    children: [
      {
        path: '',
        component: AdministerProjectsComponent
      },
      {
        path: 'projects',
        component: AdministerProjectsComponent
      },
      {
        path: 'users',
        component: AdministerUsersComponent
      },
      {
        path: '**',
        component: AdministerProjectsComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
