/* tslint:disable:no-unused-expression */

import { Component, OnInit, ViewChild } from '@angular/core';
import { Uuid } from '../../shared/types/uuid';
import { MatTableDataSource } from '@angular/material/table';
import { UserAPI } from '../../backend/services/graphql';
import { DEFAULT_IMAGES, ROLE_LABEL } from '../../shared/storage/constants';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material';
import { map } from 'rxjs/operators';
import { KnotaiConfirmationDialogComponent } from '../../shared/components/knotai-confirmation-dialog/knotai-confirmation-dialog.component';
import { KnotaiUserModalComponent } from '../../shared/components/knotai-user-modal/knotai-user-modal.component';

export interface Action {
  icon: string;
  label: string;
  action: (id: Uuid, row: number) => any;
}

const DEFAULT_TABLE_COLUMNS = ['picture', 'name', 'role', 'actions'];

@Component({
  selector: 'app-administer-users',
  templateUrl: './administer-users.component.html',
  styleUrls: ['./administer-users.component.scss']
})
export class AdministerUsersComponent implements OnInit {

  public rolesLabel = ROLE_LABEL;
  public defaultImage = DEFAULT_IMAGES.avatar;

  public displayedColumns: string[] = DEFAULT_TABLE_COLUMNS;
  public actions: Action[] = [
    {
      icon: 'create',
      label: 'Edit',
      action: (id: Uuid, row: number) => {
        this.edit(id);
      },
    },
    {
      icon: 'clear',
      label: 'Delete',
      action: (id: Uuid, row: number) => {
        this.confirmDelete(id, row);
      },
    }
  ];
  public dataSource: MatTableDataSource<UserAPI.Item>;
  public loading = true;

  @ViewChild(MatPaginator, { static: false }) set pagination(paginator: MatPaginator) {
    this.dataSource && (this.dataSource.paginator = paginator);
  }

  @ViewChild(MatSort, { static: false }) set sorting(sort: MatSort) {
    this.dataSource && (this.dataSource.sort = sort);
  }

  constructor(
    private getUsers: UserAPI.GetManyAllInfo,
    private getUser: UserAPI.GetOne,
    private deleteUser: UserAPI.Delete,
    public editionDialog: MatDialog,
    public deleteDialog: MatDialog
  ) { }

  ngOnInit() {
    this.getUsers.watch()
      .valueChanges
      .pipe(
        map(result => {
          return result.data.userProfiles.nodes;
        })
      )
      .subscribe(usersWrapper => {
          const users = usersWrapper as unknown as UserAPI.Item[];
          this.dataSource = new MatTableDataSource(users);
          this.loading = false;
        }
        , err => console.error(err), () => {
          this.loading = false;
        });
  }

  public filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public edit(id: Uuid) {
    const currentEdit = this.getUser.watch({ id })
      .valueChanges
      .pipe(
        map(result => {
          return result.data.userProfile;
        })
      )
      .subscribe(user => {
        this.editionDialog.open(KnotaiUserModalComponent, {
          height: 'auto',
          width: 'auto',
          maxWidth: '80%',
          panelClass: 'project-modal',
          data: {
            user
          }
        })
          .afterOpened()
          .toPromise()
          .then(() => {
            currentEdit.unsubscribe();
          });
      });
  }

  public confirmDelete(id: Uuid, row: number) {
    this.deleteDialog.open(KnotaiConfirmationDialogComponent, {
      height: 'auto',
      width: 'auto',
      maxWidth: '80%',
      panelClass: 'user-delete-confirmation-dialog',
      data: {
        text: 'Are you sure you want to delete this user?'
      }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.delete(id)
            .then((deletedId) => {
              if (deletedId !== null) {
                this.dataSource.data.splice(row, 1);
                this.dataSource._updateChangeSubscription();
              }
            })
            .catch(error => {
              console.error(error);
            });
        }
      });
  }

  private delete(id: Uuid): Promise<any> {
    return new Promise(((resolve, reject) => {
      this.deleteUser.mutate({ id })
        .toPromise()
        .then((value) => {
          resolve(value.data.deleteUserProfile.userProfile.id);
        })
        .catch(reason => {
          reject(reason);
        });
    }));
  }

}
