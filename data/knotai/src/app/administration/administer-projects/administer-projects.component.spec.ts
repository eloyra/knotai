import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministerProjectsComponent } from './administer-projects.component';

describe('AdministerProjectsComponent', () => {
  let component: AdministerProjectsComponent;
  let fixture: ComponentFixture<AdministerProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministerProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministerProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
