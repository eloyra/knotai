/* tslint:disable:no-unused-expression */

import { Component, OnInit, ViewChild } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { map } from 'rxjs/operators';

import { ProjectAPI } from '../../backend/services/graphql';
import { Uuid } from '../../shared/types/uuid';
import { KnotaiProjectModalComponent } from '../../shared/components/knotai-project-modal/knotai-project-modal.component';
import { MatDialog } from '@angular/material';
import { KnotaiConfirmationDialogComponent } from '../../shared/components/knotai-confirmation-dialog/knotai-confirmation-dialog.component';
import { STATES } from '../../shared/storage/constants';


export interface Action {
  icon: string;
  label: string;
  action: (id: Uuid, row: number) => any;
}

const DEFAULT_TABLE_COLUMNS = ['name', 'state', 'actions'];

@Component({
  selector: 'app-administer-projects',
  templateUrl: './administer-projects.component.html',
  styleUrls: ['./administer-projects.component.scss'],
})
export class AdministerProjectsComponent implements OnInit {

  public displayedColumns: string[] = DEFAULT_TABLE_COLUMNS;
  public actions: Action[] = [
    {
      icon: 'create',
      label: 'Edit',
      action: (id: Uuid, row?: number) => {
        this.edit(id);
      },
    },
    {
      icon: 'clear',
      label: 'Delete',
      action: (id: Uuid, row: number) => {
        this.confirmDelete(id, row);
      },
    }
  ];
  public dataSource: MatTableDataSource<ProjectAPI.Item>;
  public states = STATES;
  public loading = true;

  @ViewChild(MatPaginator, { static: false }) set pagination(paginator: MatPaginator) {
    this.dataSource && (this.dataSource.paginator = paginator);
  }

  @ViewChild(MatSort, { static: false }) set sorting(sort: MatSort) {
    this.dataSource && (this.dataSource.sort = sort);
  }

  constructor(
    private getProjects: ProjectAPI.GetManyAllInfo,
    private getProject: ProjectAPI.GetOneWithSprintNames,
    private deleteProject: ProjectAPI.Delete,
    public editionDialog: MatDialog,
    public deleteDialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.getProjects.watch()
      .valueChanges
      .pipe(
        map(result => {
          return result.data.projects.nodes;
        })
      )
      .subscribe(projectsWrapper => {
          const projects = projectsWrapper as unknown as ProjectAPI.Item[];
          this.dataSource = new MatTableDataSource(projects);
          this.loading = false;
        }
        , err => console.error(err), () => {
          this.loading = false;
        });
  }

  public filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public edit(id: Uuid) {
    const currentEdit = this.getProject.watch({ id })
      .valueChanges
      .pipe(
        map(result => {
          return result.data.project;
        })
      )
      .subscribe(project => {
        this.editionDialog.open(KnotaiProjectModalComponent, {
          height: 'auto',
          width: 'auto',
          maxWidth: '80%',
          panelClass: 'project-modal',
          data: {
            project
          }
        })
          .afterOpened()
          .toPromise()
          .then(() => {
            currentEdit.unsubscribe();
          });
      });
  }

  public confirmDelete(id: Uuid, row: number) {
    this.deleteDialog.open(KnotaiConfirmationDialogComponent, {
      height: 'auto',
      width: 'auto',
      maxWidth: '80%',
      panelClass: 'project-delete-confirmation-dialog',
      data: {
        text: 'Are you sure you want to delete this project?'
      }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.delete(id)
            .then((deletedId) => {
              if (deletedId !== null) {
                this.dataSource.data.splice(row, 1);
                this.dataSource._updateChangeSubscription();
              }
            })
            .catch(error => {
              console.error(error);
            });
        }
      });
  }

  private delete(id: Uuid): Promise<any> {
    return new Promise(((resolve, reject) => {
      this.deleteProject.mutate({ id })
        .toPromise()
        .then((value) => {
          resolve(value.data.deleteProject.project.id);
        })
        .catch(reason => {
         reject(reason);
        });
    }));
  }
}
