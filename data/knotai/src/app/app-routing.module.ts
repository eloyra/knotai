import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeProjectsComponent } from './home/home-projects/home-projects.component';


const routes: Routes = [
/*  {
    path: 'home',
    component: HomeProjectsComponent
  },*/
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    redirectTo: 'home'
  },
  {
    path: 'administration',
    redirectTo: 'administration'
  },
  {
    path: 'management',
    redirectTo: 'management'
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
