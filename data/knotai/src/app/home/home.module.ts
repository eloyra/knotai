import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeProjectsComponent } from './home-projects/home-projects.component';
import { HomeRoutingModule } from './home-routing.module';
import { ApolloModule } from 'apollo-angular';



@NgModule({
  declarations: [HomeProjectsComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ApolloModule
  ],
  exports: [HomeProjectsComponent]
})
export class HomeModule { }
