import { Component, NgIterable, OnInit, QueryList } from '@angular/core';
import { Observable } from 'rxjs';
/*import {
  GetProjectGQL,
  GetProjectsAllInfoGQL, Project,
  ProjectsWrapper,
} from '../../backend/services/graphql/definitions/projects';*/
import { map } from 'rxjs/operators';
import { Query } from 'apollo-angular';
import { ProjectAPI } from '../../backend/services/graphql';


@Component({
  selector: 'app-home-projects',
  templateUrl: './home-projects.component.html',
  styleUrls: ['./home-projects.component.scss']
})
export class HomeProjectsComponent implements OnInit {

  public projectsGQL: Observable<ProjectAPI.Wrapper>;
  // public projectGQL: Observable<Project>;
  // public project: Project;

  // private loading = false;

  constructor(
    getProjects: ProjectAPI.GetManyAllInfo,
    // getProject: GetProjectGQL
  ) {
    this.projectsGQL = getProjects.watch()
      .valueChanges
      .pipe(
        map(result => {
          console.log(result);
          return result.data.projects.nodes;
          // return result.data.projects.nodes;
        })
      );
    // this.loading = true;
    /*this.projectGQL = getProject.watch({
      id: '003c2f0c-d446-4c04-951d-f0f8eabfd902'
    })
      .valueChanges
      .pipe(
        map(result => {
          return result.data.project;
        })
      );*/
    /*this.projectGQL.subscribe(project => {
      console.log(project);
      this.project = project;
      this.loading = false;
    }, err => {}, () => {
      this.loading = false;
    });*/
  }

  ngOnInit() {
    /*this.projectsQuery.subscribe(recvProjs => {
      this.projects = recvProjs;
    });*/
    // this.projectGQL.subscribe(recvProj => {console.log(recvProj);});
  }

}
