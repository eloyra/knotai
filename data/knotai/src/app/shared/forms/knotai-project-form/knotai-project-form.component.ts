import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ProjectAPI, RepositoryAPI } from '../../../backend/services/graphql';
import { Observable, Subscription } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PRIORITIES, STATES } from '../../storage/constants';
import { Uuid } from '../../types/uuid';
import { map } from 'rxjs/operators';
import { SubmissionEvent } from '../../interfaces/submission-event';

@Component({
  selector: 'app-knotai-project-form',
  templateUrl: './knotai-project-form.component.html',
  styleUrls: ['./knotai-project-form.component.scss']
})
export class KnotaiProjectFormComponent implements OnInit, OnDestroy {

  @Input() project: ProjectAPI.Item | Uuid = null;
  @Input() enableCreation = true;
  @Input() enableDelete = false;
  @Input() ownControls = false;
  @Input() submitListener: Observable<void>;
  @Output() submittedEvent = new EventEmitter<SubmissionEvent>();

  private submitSubscription: Subscription;
  private repository: RepositoryAPI.Item = null;

  public ready = false;
  public projectForm: FormGroup = null;
  public states = STATES;
  public priorities = PRIORITIES;

  public submitFunction = () => this.submit();

  constructor(
    private getProject: ProjectAPI.GetOneFull,
    private getRepository: RepositoryAPI.GetOne,
    private updateRepository: RepositoryAPI.Update,
    private createRepository: RepositoryAPI.Create,
    private updateProject: ProjectAPI.Update,
    private createProject: ProjectAPI.Create,
    private fb: FormBuilder,
  ) {
  }

  isProjectItem(object: ProjectAPI.Item | Uuid): object is ProjectAPI.Item {
    return ( object as ProjectAPI.Item ).id !== undefined;
  }

  isRepositoryItem(object: RepositoryAPI.Item | Uuid): object is RepositoryAPI.Item {
    return ( object as RepositoryAPI.Item ).id !== undefined;
  }

  ngOnInit() {
    this.submitSubscription = this.submitListener.subscribe(() => {
      this.submitFunction();
    });

    if (!this.project) {
      this.buildForm(null);
    } else if (this.isProjectItem(this.project)) {
      this.repository = this.project.repository;
      this.buildForm(this.project);
    } else if (this.project) {
      this.getProject.watch({ id: this.project })
        .valueChanges
        .pipe(
          map(result => {
            return result.data.project;
          })
        )
        .subscribe(project => {
          this.project = project;
          this.repository = project.repository;
          this.buildForm(project);
        });
    }

  }

  private buildForm(project: ProjectAPI.Item) {
    if (project) {
      this.projectForm = this.fb.group({
        name: [project.name || ''],
        description: [project.description || ''],
        start: [project.start || ''],
        finishReal: [project.finishReal || ''],
        finishEstimate: [project.finishEstimate || ''],
        state: [project.state || ''],
        priority: [project.priority || ''],
        repositoryUrl: [project.repository.url || ''],
      });
    } else {
      this.projectForm = this.fb.group({
        name: [''],
        description: [''],
        start: [''],
        finishReal: [''],
        finishEstimate: [''],
        state: [''],
        priority: [''],
        repositoryUrl: [''],
      });
    }
    this.ready = true;
  }

  private submit(): void {
    if (this.projectForm.controls.repositoryUrl.dirty) {
      this.createOrUpdateRepository()
        .then(success => {
          if (success) {
            this.createOrUpdateProject(this.project as ProjectAPI.Item)
              .then(status => {
                this.submittedEvent.emit({ status });
              })
              .catch(reason => this.submittedEvent.emit({
                status: false,
                message: reason
              }));
          } else {
            this.submittedEvent.emit({
              status: false,
              message: 'Unable to save repository changes.'
            });
          }
        })
        .catch(reason => this.submittedEvent.emit({
          status: false,
          message: reason
        }));
    } else if (this.projectForm.dirty) {
      this.createOrUpdateProject(this.project as ProjectAPI.Item)
        .then(() => {
          this.submittedEvent.emit({ status: true });
        })
        .catch(reason => this.submittedEvent.emit({
          status: false,
          message: reason
        }));
    } else {
      this.submittedEvent.emit({ status: true });
    }
  }

  private createOrUpdateRepository(): Promise<boolean> {
    return new Promise(( (resolve, reject) => {
      if (this.repository) {
        this.updateRepo()
          .then(() => {
            resolve(true);
          })
          .catch(reason => {
            reject(reason);
          });
      } else {
        if (this.projectForm.controls.repositoryUrl && this.projectForm.controls.repositoryUrl.value.length > 0) {
          this.createRepo()
            .then(result => {
              this.repository = result.data.createRepository.repository;
              resolve(true);
            })
            .catch(reason => reject(reason));
        } else {
          this.repository = null;
          reject('Unable to create new repository because the URL is invalid.');
        }
      }
    } ));
  }

  private updateRepo(): Promise<any> {
    return this.updateRepository.mutate({
      id: this.repository.id,
      url: this.projectForm.controls.repositoryUrl ? this.projectForm.controls.repositoryUrl.value : this.repository.url
    }).toPromise();
  }

  private createRepo(): Promise<any> {
    return this.createRepository.mutate({
      url: this.projectForm.controls.repositoryUrl.value
    }).toPromise();
  }

  private createOrUpdateProject(project: ProjectAPI.Item): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (project) {
        this.update(project)
          .then(() => {
            resolve(true);
          })
          .catch(reason => reject(reason));
      } else {
        if (this.projectForm.controls.name && this.projectForm.controls.name.value.length > 0) {
          this.create()
            .then(result => {
              project = result.data.createProject.project;
              resolve(true);
            })
            .catch(reason => reject(reason));
        } else {
          reject('Unable to create project because there is no valid name.');
        }
      }
    });
  }

  private update(project: ProjectAPI.Item): Promise<any> {
    return this.updateProject.mutate({
      id: project.id,
      name: this.projectForm.controls.name.value,
      desc: this.projectForm.controls.description.value,
      start: this.projectForm.controls.start.value,
      finishEstimate: this.projectForm.controls.finishEstimate.value,
      finishReal: this.projectForm.controls.finishReal.value,
      state: this.projectForm.controls.state.value,
      priority: this.projectForm.controls.priority.value,
      repo: this.repository ? this.repository.id : null,
      wiki: project.wiki,
    }).toPromise();
  }

  private create(): Promise<any> {
    return this.createProject.mutate({
      name: this.projectForm.controls.name.value,
      desc: this.projectForm.controls.description.value,
      start: this.projectForm.controls.start.value,
      finishEstimate: this.projectForm.controls.finishEstimate.value,
      finishReal: this.projectForm.controls.finishReal.value,
      state: this.projectForm.controls.state.value,
      priority: this.projectForm.controls.priority.value,
      repo: this.repository ? this.repository.id : null,
      wiki: ( this.project as ProjectAPI.Item ).wiki,
    }).toPromise();
  }

  ngOnDestroy() {
    this.submitSubscription.unsubscribe();
  }
}
