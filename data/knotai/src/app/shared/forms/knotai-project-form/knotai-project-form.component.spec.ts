import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiProjectFormComponent } from './knotai-project-form.component';

describe('KnotaiProjectFormComponent', () => {
  let component: KnotaiProjectFormComponent;
  let fixture: ComponentFixture<KnotaiProjectFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiProjectFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiProjectFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
