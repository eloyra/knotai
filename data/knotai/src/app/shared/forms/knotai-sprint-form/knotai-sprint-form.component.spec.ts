import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiSprintFormComponent } from './knotai-sprint-form.component';

describe('KnotaiSprintFormComponent', () => {
  let component: KnotaiSprintFormComponent;
  let fixture: ComponentFixture<KnotaiSprintFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiSprintFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiSprintFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
