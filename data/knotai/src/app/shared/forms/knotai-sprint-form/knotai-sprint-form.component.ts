import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { SprintAPI } from '../../../backend/services/graphql';
import { Uuid } from '../../types/uuid';
import { Observable, Subscription } from 'rxjs';
import { SubmissionEvent } from '../../interfaces/submission-event';
import { FormBuilder, FormGroup } from '@angular/forms';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-knotai-sprint-form',
  templateUrl: './knotai-sprint-form.component.html',
  styleUrls: ['./knotai-sprint-form.component.scss']
})
export class KnotaiSprintFormComponent implements OnInit, OnDestroy {

  @Input() sprint: SprintAPI.Item | Uuid = null;
  @Input() enableCreation = true;
  @Input() enableDelete = false;
  @Input() ownControls = false;
  @Input() submitListener: Observable<void>;
  @Output() submittedEvent = new EventEmitter<SubmissionEvent>();

  private submitSubscription: Subscription;

  public ready = false;
  public sprintForm: FormGroup = null;

  public submitFunction = () => this.submit();

  constructor(
    private getSprint: SprintAPI.GetOne,
    private updateSprint: SprintAPI.Update,
    private createSprint: SprintAPI.Create,
    private fb: FormBuilder
  ) {}

  isSprintItem(object: SprintAPI.Item | Uuid): object is SprintAPI.Item {
    return ( object as SprintAPI.Item ).id !== undefined;
  }

  ngOnInit() {
    this.submitSubscription = this.submitListener.subscribe(() => {
      this.submitFunction();
    });

    if (!this.sprint) {
      this.buildForm(null);
    } else if (this.isSprintItem(this.sprint)) {
      this.buildForm(this.sprint);
    } else {
      this.getSprint.watch({ id: this.sprint })
        .valueChanges
        .pipe(
          map(result => {
            return result.data.sprint;
          })
        )
        .subscribe(sprint => {
          this.sprint = sprint;
          this.buildForm(sprint);
        });
    }
  }

  private buildForm(sprint: SprintAPI.Item) {
    if (sprint) {
      this.sprintForm = this.fb.group({
        name: [sprint.name || ''],
        description: [sprint.description || ''],
        start: [sprint.start || ''],
        finishReal: [sprint.finishReal || ''],
        finishEstimate: [sprint.finishEstimate || ''],
        project: [sprint.project || '']
      });
    } else {
      this.sprintForm = this.fb.group({
        name: [''],
        description: [''],
        start: [''],
        finishReal: [''],
        finishEstimate: [''],
        project: ['']
      });
    }
    this.ready = true;
  }

  private submit() {
    if (this.sprintForm.dirty) {
      this.createOrUpdate(this.sprint as SprintAPI.Item)
        .then(status => {
          this.submittedEvent.emit({ status });
        })
        .catch(reason => this.submittedEvent.emit({
          status: false,
          message: reason
        }));
    } else {
      this.submittedEvent.emit({ status: true });
    }
  }

  private createOrUpdate(sprint: SprintAPI.Item): Promise<boolean> {
    console.log(sprint);
    return new Promise(((resolve, reject) => {
      if (sprint) {
        this.update(sprint)
          .then(() => {
            resolve(true);
          })
          .catch(reason => reject(reason));
      } else {
        this.create()
          .then(() => {
            resolve(true);
          })
          .catch(reason => reject(reason));
      }
    }));
  }

  private update(sprint: SprintAPI.Item): Promise<any> {
    return this.updateSprint.mutate({
      id: sprint.id,
      name: this.sprintForm.controls.name.value,
      desc: this.sprintForm.controls.description.value,
      start: this.sprintForm.controls.start.value,
      finishEstimate: this.sprintForm.controls.finishEstimate.value,
      finishReal: this.sprintForm.controls.finishReal.value,
      project: this.sprintForm.controls.project.value
    }).toPromise();
  }

  private create(): Promise<any> {
    return this.createSprint.mutate({
      name: this.sprintForm.controls.name.value,
      desc: this.sprintForm.controls.description.value,
      start: this.sprintForm.controls.start.value,
      finishEstimate: this.sprintForm.controls.finishEstimate.value,
      finishReal: this.sprintForm.controls.finishReal.value,
      project: this.sprintForm.controls.project.value
    })
      .toPromise();
  }

  ngOnDestroy() {
    this.submitSubscription.unsubscribe();
  }

}
