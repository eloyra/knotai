import { Component, Inject, OnInit } from '@angular/core';
import { RolesAPI, UserAPI } from '../../../backend/services/graphql';
import { DEFAULT_IMAGES, ROLE_LABEL } from '../../storage/constants';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-knotai-user-modal',
  templateUrl: './knotai-user-modal.component.html',
  styleUrls: ['./knotai-user-modal.component.scss']
})
export class KnotaiUserModalComponent implements OnInit {

  public user: UserAPI.Item;
  public userForm: FormGroup;
  public roles: RolesAPI.Item[];
  public roleLabels = ROLE_LABEL;
  public defaultImage = DEFAULT_IMAGES.avatar;
  public currentImage: string = null;

  constructor(
    public dialogRef: MatDialogRef<KnotaiUserModalComponent>,
    private updateUser: UserAPI.Update,
    private getRoles: RolesAPI.GetManyAllInfo,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public entryData: {user: UserAPI.Item}
  ) { }

  ngOnInit() {
    this.user = this.entryData.user;
    this.currentImage = this.user.picture;

    this.userForm = this.fb.group({
      firstName: [this.user.firstName || ''],
      lastName: [this.user.lastName || ''],
      role: [this.user.role.id || '']
    });

    this.getRoles.watch()
      .valueChanges
      .pipe(
        map(result => {
          return result.data.roles.nodes;
        })
      )
      .subscribe(rolesWrapper => {
        this.roles = rolesWrapper as unknown as RolesAPI.Item[];
      });
  }

  public uploadImage(imageInput: any) {
    const image: File = imageInput.files[0];
    const reader = new FileReader();
    reader.onload = _ => this.currentImage = reader.result.toString();
    reader.readAsDataURL(image);
  }

  public close(): void {
    this.dialogRef.close(false);
  }

  public update(): void {
    this.updateUser.mutate({
      id: this.user.id,
      firstName: this.userForm.controls.firstName.value,
      lastName: this.userForm.controls.lastName.value,
      role: this.userForm.controls.role.value,
      picture: this.currentImage,
    })
      .toPromise()
      .then(() => {
        this.dialogRef.close(true);
      })
      .catch(reason => {
        console.error(reason);
      });

  }

}
