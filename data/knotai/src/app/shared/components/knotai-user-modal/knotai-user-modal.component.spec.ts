import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiUserModalComponent } from './knotai-user-modal.component';

describe('KnotaiUserModalComponent', () => {
  let component: KnotaiUserModalComponent;
  let fixture: ComponentFixture<KnotaiUserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiUserModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
