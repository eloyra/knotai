import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiProjectModalComponent } from './knotai-project-modal.component';

describe('KnotaiProjectModalComponent', () => {
  let component: KnotaiProjectModalComponent;
  let fixture: ComponentFixture<KnotaiProjectModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiProjectModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiProjectModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
