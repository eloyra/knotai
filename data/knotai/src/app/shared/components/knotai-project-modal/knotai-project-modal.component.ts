import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ProjectAPI, RepositoryAPI } from '../../../backend/services/graphql';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-knotai-project-modal',
  templateUrl: './knotai-project-modal.component.html',
  styleUrls: ['./knotai-project-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class KnotaiProjectModalComponent implements OnInit {

  public project: ProjectAPI.Item;
  private repository: RepositoryAPI.Item;

  private formQueue: Map<string, {[key: string]: boolean}> = new Map<string, {[p: string]: boolean}>();

  public formSubmitSubject: Subject<void> = new Subject<void>();
  public projectSubmittedEvent = event => this.formIsSubmitted('project', event);
  public sprintSubmittedEvent = (sprint, event) => this.formIsSubmitted(sprint, event);

  constructor(
    public dialogRef: MatDialogRef<KnotaiProjectModalComponent>,
    @Inject(MAT_DIALOG_DATA) public entryData: { project: ProjectAPI.Item }
  ) {
  }

  ngOnInit() {
    this.initializeData();
    this.formQueue.set('project', { processed: false, status: false });
  }

  private initializeData() {
    this.project = this.entryData ? this.entryData.project || null : null;
    this.repository = this.project ? this.project.repository || null : null;
  }

  public close(): void {
    this.dialogRef.close(false);
  }

  public update(): void {
    this.formSubmitSubject.next();
    // Subscribe to formIsSubmitted changes
  }

  public formIsSubmitted(form, {status}) {
    this.formQueue.set(form, { processed: true, status });
    this.dialogRef.close(true);
    // this.close();
  }

}
