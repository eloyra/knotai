import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiProjectCardComponent } from './knotai-project-card.component';

describe('KnotaiProjectCardComponent', () => {
  let component: KnotaiProjectCardComponent;
  let fixture: ComponentFixture<KnotaiProjectCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiProjectCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiProjectCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
