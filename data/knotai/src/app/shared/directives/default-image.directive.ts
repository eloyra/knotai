import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'img[appDefaultImage]',
  host: {
    '[src]': 'src'
  }
})
export class DefaultImageDirective {

  @Input() src: string;
  @Input('appDefaultImage') default: string;

  constructor() { }

  @HostListener('error') onError() {
    this.src = this.default;
  }

}
