import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KnotaiSidenavComponent } from './navigation/knotai-sidenav/knotai-sidenav.component';
import { KnotaiToolbarComponent } from './navigation/knotai-toolbar/knotai-toolbar.component';
import {
    MatButtonModule, MatDatepickerModule,
    MatDialogModule, MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule, MatNativeDateModule, MatProgressSpinnerModule, MatSelectModule,
    MatSidenavModule, MatTabsModule,
    MatToolbarModule
} from '@angular/material';
import { KnotaiProjectCardComponent } from './components/knotai-project-card/knotai-project-card.component';
import { RouterModule } from '@angular/router';
import { KnotaiProjectModalComponent } from './components/knotai-project-modal/knotai-project-modal.component';
import { KnotaiConfirmationDialogComponent } from './components/knotai-confirmation-dialog/knotai-confirmation-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DefaultImageDirective } from './directives/default-image.directive';
import { KnotaiUserModalComponent } from './components/knotai-user-modal/knotai-user-modal.component';
import { KnotaiProjectFormComponent } from './forms/knotai-project-form/knotai-project-form.component';
import { KnotaiSprintFormComponent } from './forms/knotai-sprint-form/knotai-sprint-form.component';



@NgModule({
  declarations: [KnotaiSidenavComponent, KnotaiToolbarComponent, KnotaiProjectCardComponent, KnotaiProjectModalComponent, KnotaiConfirmationDialogComponent, DefaultImageDirective, KnotaiUserModalComponent, KnotaiProjectFormComponent, KnotaiSprintFormComponent],
    imports: [
        CommonModule,
        MatSidenavModule,
        MatIconModule,
        MatToolbarModule,
        RouterModule,
        MatFormFieldModule,
        MatDialogModule,
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        MatNativeDateModule,
        MatDatepickerModule,
        ReactiveFormsModule,
        MatExpansionModule,
        MatTabsModule,
        MatProgressSpinnerModule
    ],
  exports: [KnotaiSidenavComponent, KnotaiToolbarComponent, KnotaiProjectCardComponent, KnotaiProjectModalComponent, KnotaiConfirmationDialogComponent, DefaultImageDirective, KnotaiUserModalComponent, KnotaiProjectFormComponent, KnotaiSprintFormComponent],
  entryComponents: [KnotaiProjectModalComponent, KnotaiConfirmationDialogComponent, KnotaiUserModalComponent]
})
export class SharedModule { }
