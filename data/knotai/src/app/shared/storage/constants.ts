import { environment } from '../../../environments/environment';

export const DEFAULT_IMAGES = {
  avatar:  environment.deployUrl + environment.imagesPath + 'default-avatar.png'
};

const STATES_LIST = [
  'NEW',
  'PENDING',
  'ONGOING',
  'BLOCKED',
  'REVISION',
  'FINISHED'
];

enum States {
  'NEW' = 'New',
  'PENDING' = 'Pending',
  'ONGOING' = 'Ongoing',
  'BLOCKED' = 'Blocked',
  'REVISION' = 'Revision',
  'FINISHED' = 'Finished'
}

export const STATES = {
  keys: STATES_LIST,
  enum: States
};



const PRIORITIES_LIST = [
  'HIGH',
  'MEDIUM',
  'LOW'
];

enum Priorities {
  'HIGH' = 'High',
  'MEDIUM' = 'Medium',
  'LOW' = 'Low'
}

export const PRIORITIES = {
  keys: PRIORITIES_LIST,
  enum: Priorities
};



const TASK_TYPES_LIST = [
  'ANALYSIS',
  'DESIGN',
  'FRONT',
  'BACK',
  'TEST',
  'SYSTEMS'
];

enum TaskTypes {
  'ANALYSIS' = 'Analysis',
  'DESIGN' = 'Design',
  'FRONT' = 'Frontend',
  'BACK' = 'Backend',
  'TEST' = 'Test',
  'SYSTEMS' = 'Systems'
}

export const TASK_TYPES = {
  keys: TASK_TYPES_LIST,
  enum: TaskTypes
};



export const ROLE_LABEL = {
  admin: 'Administrator',
  mod: 'Moderator',
  proj_man: 'Project manager',
  proj_coord: 'Project coordinator',
  dev: 'Developer',
  external: 'External'
};
