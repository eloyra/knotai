export interface SubmissionEvent {
  status: boolean;
  message?: string;
}
