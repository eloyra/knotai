import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/typings/sidenav';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';


export interface SidenavParams {
  mode ?: MatSidenav['mode'];
  opened ?: MatSidenav['opened'];
  topGap ?: MatSidenav['fixedTopGap'];
}

export interface SidenavOption {
  icon: string;
  name: string;
  route: string;
}

const SIDENAV_DEFAULT_PARAMS: SidenavParams = {
  mode: 'side',
  opened: true,
  topGap: 70
};

const VALID_ROUTES = {
  '/home': '/home',
  '/administration': '/administration',
  '/management': '/management'
};

const HOME_SECTIONS: SidenavOption[] = [
  {
    icon: 'folder',
    name: 'Projects',
    route: ''
  }
];

const ADMINISTRATION_SECTIONS: SidenavOption[] = [
  {
    icon: 'folder',
    name: 'Projects',
    route: '/administration/projects'
  },
  {
    icon: 'folder',
    name: 'Users',
    route: '/administration/users'
  }
];

const MANAGEMENT_SECTIONS: SidenavOption[] = [
  {
    icon: 'folder',
    name: 'Overview',
    route: '/management/overview'
  }
];

const SIDENAV_OPTIONS: {[key: string]: SidenavOption[]} = {
  default : HOME_SECTIONS,
  '/home': HOME_SECTIONS,
  '/administration': ADMINISTRATION_SECTIONS,
  '/management': MANAGEMENT_SECTIONS
};

@Component({
  selector: 'app-knotai-sidenav',
  templateUrl: './knotai-sidenav.component.html',
  styleUrls: ['./knotai-sidenav.component.scss']
})
export class KnotaiSidenavComponent implements OnInit {

  public sidenavMode: SidenavParams['mode'] = SIDENAV_DEFAULT_PARAMS.mode;
  public sidenavState: SidenavParams['opened'] = SIDENAV_DEFAULT_PARAMS.opened;
  public sidenavTopGap: SidenavParams['topGap'] = SIDENAV_DEFAULT_PARAMS.topGap;

  public sidenavOptions: SidenavOption[] = SIDENAV_OPTIONS['/home'];

  constructor(
    router: Router
  ) {
    router.events
      .pipe(
        filter(e => e instanceof NavigationEnd)
      )
      .subscribe((e: NavigationEnd) => {
        this.updateSidenav(e.urlAfterRedirects);
      });
  }

  ngOnInit() {
  }

  updateSidenav(location: string): void {
    const topLevelLocation = location
      .substring(0,
        location.indexOf('/', 1) !== -1 ? location.indexOf('/', 1) : location.length
      );
    VALID_ROUTES[topLevelLocation]
      ? this.sidenavOptions = SIDENAV_OPTIONS[topLevelLocation]
      : this.sidenavOptions = SIDENAV_OPTIONS.default;
  }

}
