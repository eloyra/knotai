import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiSidenavComponent } from './knotai-sidenav.component';

describe('KnotaiSidenavComponent', () => {
  let component: KnotaiSidenavComponent;
  let fixture: ComponentFixture<KnotaiSidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiSidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
