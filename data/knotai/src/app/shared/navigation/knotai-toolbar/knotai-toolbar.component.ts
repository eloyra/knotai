import { Component, Input, OnInit } from '@angular/core';

export interface ToolbarOption {
  icon: string;
  text?: string;
  route: string;
}

const TOOLBAR_DEFAULT_OPTIONS: ToolbarOption[] = [
  {
    icon: 'dashboard',
    text: 'Home',
    route: '/home'
  },
  {
    icon: 'inbox',
    text: 'Administration',
    route: '/administration'
  },
  {
    icon: 'create',
    text: 'Management',
    route: '/management'
  }
];

@Component({
  selector: 'app-knotai-toolbar',
  templateUrl: './knotai-toolbar.component.html',
  styleUrls: ['./knotai-toolbar.component.scss']
})
export class KnotaiToolbarComponent implements OnInit {

  @Input() title: string;
  public toolbarOptions: ToolbarOption[] = TOOLBAR_DEFAULT_OPTIONS;

  constructor() { }

  ngOnInit() {
  }

}
