import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BtnProgressDirective } from './directives/btn-progress.directive';
import { AutofocusDirective } from './directives/autofocus.directive';
import { AlertGroupComponent } from './components/alert-group/alert-group.component';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { InputFeedbackComponent } from './components/input-feedback/input-feedback.component';
import { EditableFieldComponent } from './components/editable-field/editable-field.component';
import { FormsModule } from '@angular/forms';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbAlertModule
  ],
  declarations: [
    BtnProgressDirective,
    AutofocusDirective,
    AlertGroupComponent,
    InputFeedbackComponent,
    EditableFieldComponent,
    ConfirmModalComponent
  ],
  exports: [
    BtnProgressDirective,
    AutofocusDirective,
    AlertGroupComponent,
    InputFeedbackComponent,
    EditableFieldComponent,
    ConfirmModalComponent
  ],
  entryComponents: [
    ConfirmModalComponent
  ]
})
export class SharedModule {}
