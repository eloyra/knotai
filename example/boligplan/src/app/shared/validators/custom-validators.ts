import { AbstractControl, ValidatorFn } from '@angular/forms';

export function customRequiredValidator(field: string = 'no_field', errorMessage: string = 'Den angivne værdi er ikke gyldig.'): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const forbidden = !control.value;
    return forbidden ? {'required': {value: errorMessage, field: field}} : null;
  };
}

export function customEmailValidator(field: string = 'no_field', errorMessage: string = 'Den angivne email er ikke en gyldig email-adresse.'): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    // Based on RFC 5322 Official Standard: https://emailregex.com/
    const email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const forbidden = !email_regex.test(control.value);
    return forbidden ? {'email': {value: errorMessage, field: field}} : null;
  };
}

export function customLengthValidator(maxLength: number = 0, minLength: number = -1, field: string = 'no_field', errorMessage: string = 'Den angivne værdi svarer ikke til længdebehov.'): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const maxLengthViolation = maxLength !== 0 ? control.value.length > maxLength : false;
    const minLengthViolation = minLength !== -1 ? control.value.length < minLength : false;
    let extendedErrorMessage = '';
    if (maxLengthViolation || minLengthViolation) {
      maxLengthViolation ? extendedErrorMessage = errorMessage + ` Det skal være ${maxLength} tegn eller mindre.` : extendedErrorMessage = errorMessage + ` Det skal være ${minLength} tegn eller mere.`;
      return {'length': {value: extendedErrorMessage, field: field, maxLength: maxLength, minLength: minLength}};
    }
    else { return null; }
  };
}

export function customImageFormatValidator(validFormats: string[], field: string = 'no_field', errorMessage: string = 'Det indsendte billede er ikke i et gyldigt format.'): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const imageFormat = control.value ? control.value.substr(control.value.lastIndexOf('.') + 1) : null;
    const forbidden = imageFormat ? !validFormats.includes(imageFormat) : true;
    // TODO: Ahora mismo si el formato no existe, se ignora el error porque se entiende que no hay archivo seleccionado y salta el required, pero hay que revisarlo.
    return imageFormat ? forbidden ? {'img_format': {value: errorMessage, field: field}} : null : null;
  };
}

export function customMatchValidator(pattern: RegExp, field: string = 'no_field', errorMessage: string = 'Den angivne værdi stemmer ikke overens.'): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    console.log(pattern);
    const forbidden = control.value ? !pattern.test(control.value) : true;
    return forbidden ? {'match': {value: errorMessage, field: field}} : null;
  };
}
