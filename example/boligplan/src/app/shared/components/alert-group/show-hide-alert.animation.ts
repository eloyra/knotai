import { animate, style, transition, trigger } from '@angular/animations';

export const showHideAlert = trigger('showHideAlert', [
  transition(':leave', [
    animate('80ms ease-out', style({
      opacity: 0,
      height: 0,
      paddingTop: 0,
      paddingBottom: 0
    }))
  ]),
  transition(':enter', [
    style({
      opacity: 0,
      height: 0,
      paddingTop: 0,
      paddingBottom: 0
    }),
    animate('100ms ease-out', style({
      opacity: 1,
      height: '*',
      paddingTop: '*',
      paddingBottom: '*'
    }))
  ])
]);
