import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-editable-field',
  templateUrl: './editable-field.component.html',
  styleUrls: ['./editable-field.component.scss']
})
export class EditableFieldComponent implements OnInit {

  @Input() title = '';
  @Input() text = '';
  originalText: string;
  editing = false;
  submitting = false;

  @Output() editComplete: EventEmitter<string> = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {
    this.originalText = this.text;
  }

  showSubmitting() {
    this.submitting = true;
  }

  show() {
    this.submitting = false;
    this.editing = true;
  }

  hide() {
    this.text = this.originalText;
    this.submitting = false;
    this.editing = false;
  }

  toggle() {
    this.editing ? this.hide() : this.show();
  }

  save() {
    if (this.originalText === this.text) {
      return false;
    }
    this.originalText = this.text;
    this.editComplete.emit(this.text);
  }

}
