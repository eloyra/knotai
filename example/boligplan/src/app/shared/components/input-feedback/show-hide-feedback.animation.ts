import { animate, group, query, stagger, style, transition, trigger } from '@angular/animations';

export const showHideFeedback = trigger('showHideFeedback', [
  transition(':enter', [
    group([
      style({
        margin: 0
      }),
      animate('50ms ease-out', style({
        margin: '*'
      })),
      query('.msg', [
        style({
          height: 0,
          opacity: 0
        }),
        stagger(50, animate('100ms ease-out', style({
          height: '*',
          opacity: 1
        })))
      ], {optional: true})
    ])
  ])
]);
