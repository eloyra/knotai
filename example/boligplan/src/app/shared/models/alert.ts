export class Alert {

  type: string;
  title?: string;
  message: string;
  dismissible?: boolean;

}
