import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class HomeLoggedGuard implements CanLoad, CanActivate {

  constructor(
    private auth: AuthenticationService,
    private router: Router
  ) {}

  canLoad(route: Route): Promise<boolean> | boolean {
    return this.auth.isAuthenticated()
      .then(() => {
        this.router.navigate(['/user/campaigns']).catch(err => console.error(err));
        return false;
      })
      .catch(() => true);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    return this.canLoad(route.routeConfig);
  }

}
