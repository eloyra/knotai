import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { homeRouterTransition } from '../../home.router.animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [homeRouterTransition]
})
export class HomeComponent implements OnInit {

  activeForm = false;

  constructor() {}

  ngOnInit() {}

  getState(outlet: RouterOutlet) {
    if (outlet && outlet.activatedRouteData && outlet.activatedRouteData['state']) {
      this.activeForm = true;
      return outlet.activatedRouteData['state'];
    }
    this.activeForm = false;
  }

}
