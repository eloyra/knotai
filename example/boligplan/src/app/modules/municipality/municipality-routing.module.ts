import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteAuthGuard } from '../../core/authentication/route-auth.gard';
import { MunicipalityAddComponent } from './pages/municipality-add/municipality-add.component';
import { MunicipalityEditComponent } from './pages/municipality-edit/municipality-edit.component';
import { MunicipalityListComponent } from './pages/municipality-list/municipality-list.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteAuthGuard],
    data: {
      roles: ['administator'],
    },
    children: [
      {
        path: 'create',
        component: MunicipalityAddComponent
      },
      {
        path: 'edit/:id',
        component: MunicipalityEditComponent
      },
      {
        path: '',
        component: MunicipalityListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MunicipalityRoutingModule { }
