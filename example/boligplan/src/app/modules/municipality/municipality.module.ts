import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MunicipalityAddComponent } from './pages/municipality-add/municipality-add.component';
import { MunicipalityListComponent } from './pages/municipality-list/municipality-list.component';
import { MunicipalityEditComponent } from './pages/municipality-edit/municipality-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { MunicipalityRoutingModule } from './municipality-routing.module';
import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MunicipalityRoutingModule,
    CKEditorModule,
  ],
  declarations: [
    MunicipalityAddComponent,
    MunicipalityListComponent,
    MunicipalityEditComponent
  ]
})
export class MunicipalityModule {}
