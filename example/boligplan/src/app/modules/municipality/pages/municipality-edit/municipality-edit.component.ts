import { Component, OnInit } from '@angular/core';
import { User } from '../../../../core/models/user';
import { Municipality } from '../../../../core/models/municipality';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService } from '../../../../core/http/crud.service';
import { Globals } from '../../../../Globals';
import { UserService } from '../../../../core/http/user.service';
import { FormErrorHandlerService } from '../../../../core/services/form-error-handler.service';
import { FileService } from '../../../../core/http/file.service';
import { File as FileModel } from '../../../../core/models/file';
import {
  customImageFormatValidator,
  customLengthValidator,
  customRequiredValidator
} from '../../../../shared/validators/custom-validators';
import { Location } from '@angular/common';

@Component({
  selector: 'app-municipality-edit',
  templateUrl: './municipality-edit.component.html',
  styleUrls: ['./municipality-edit.component.scss']
})
export class MunicipalityEditComponent implements OnInit {

  editMunicipalityForm = this.fb.group({});

  public currentUser: User;
  public municipalityToEdit: Municipality;
  public municipalityLogo: string;
  public municipalities: Municipality[];

  public allowedLogoTypes = this.globals.generalConfig.allowedLogoTypes.join(',');
  public allowedLogoTypesUserFriendly = this.globals.generalConfig.allowedLogoTypes.map((value: string) => value.split('/')[1]).join(', ');
  private file: File = null;
  private newLogo: FileModel = null;

  public errors = {};

  public show = false;
  public submitting = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private crud: CrudService,
    private globals: Globals,
    private users: UserService,
    private errorHandler: FormErrorHandlerService,
    private fb: FormBuilder,
    private files: FileService
  ) {}

  ngOnInit() {
    const uuid = this.route.snapshot.paramMap.get('id');
    this.crud.getIndividual(Municipality, uuid, ['image'])
      .toPromise()
      .then((municipality: Municipality) => {
        this.municipalityToEdit = municipality;
        this.municipalityLogo = this.municipalityToEdit.image ? this.globals.host + this.municipalityToEdit.image.url : this.globals.host + this.globals.generalConfig.noLogoPlaceholder;
        this.buildFormControls();
        this.users.getCurrentUser()
          .then((currentUser: User) => {
            this.currentUser = currentUser;
            if (!this.checkLegitimateAccess()) {
              console.warn(`You can't access this page.`);
              this.location.back();
            } else {
              this.show = true;
            }
          })
          .catch((err) => {
            console.warn(`Can't initialize user-edit component.`);
            console.error(err);
          });
      });
  }

  private buildFormControls(): void {
    this.editMunicipalityForm.addControl(`att_code`, new FormControl(this.municipalityToEdit.code, [customRequiredValidator('att_code', 'Koden kan ikke være tom.'), customLengthValidator(5, -1, 'att_code', 'Den angivne kode er for lang.')]));
    this.editMunicipalityForm.addControl(`att_name`, new FormControl(this.municipalityToEdit.name, [customRequiredValidator('att_name', 'Navnet kan ikke være tomt.')]));
    this.editMunicipalityForm.addControl(`att_description`, new FormControl(this.municipalityToEdit.description['value'], [customRequiredValidator('att_description', 'Beskrivelsen kan ikke være tom.')]));
    this.editMunicipalityForm.addControl(`att_contact`, new FormControl(this.municipalityToEdit.contact['value'], [customRequiredValidator('att_contact', 'Kontaktoplysningerne kan ikke være tomme.')]));
    this.editMunicipalityForm.addControl(`logo`, new FormControl(null, [customImageFormatValidator(this.globals.generalConfig.allowedLogoTypes.map((value: string) => value.split('/')[1]), 'logo', `Logoet er ikke i et gyldigt format. Vi accepterer følgende formater: ${this.globals.generalConfig.allowedLogoTypes.map((value: string) => value.split('/')[1]).join(', ')}`)]));
    this.editMunicipalityForm.addControl(`att_active`, new FormControl({}, []));
  }

  private checkLegitimateAccess(): boolean {
    return this.users.isAdmin(this.currentUser);
  }

  /**
   * Gets the file selected in the form.
   */
  public onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      this.file = event.target.files[0];
    }
  }

  public onSubmit(): void {
    // We discard past form errors.
    this.errors = {};
    // We discard past images
    this.newLogo = null;

    if (this.validateForm(this.editMunicipalityForm)) {
      this.processParams(this.editMunicipalityForm)
        .then(() => {
          if (this.newLogo != null) { this.municipalityToEdit.image = this.newLogo; }
          this.crud.update(this.municipalityToEdit, ['image'])
            .toPromise()
            .then(() => {
              console.log(`Municipality edited successfuly.`);
              this.router.navigate(['admin', 'municipalities']).catch(err => console.error(err));
            })
            .catch((err) => {
              this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
              console.error(this.errors);
            });
        })
        .catch((err) => {
          this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
        })
        .then(() => this.submitting = false);
    }
    else {
      console.error(this.errors);
    }
  }

  private validateForm(form: FormGroup): boolean {
    this.errors = {};
    let isCorrect = true;

    if (form.dirty) {
      if (form.invalid) {
        isCorrect = false;
        this.errors = this.errorHandler.handleControlErrors(this.errors, form.controls);
      }
    }

    return isCorrect;
  }

  private processParams(form: FormGroup): Promise<object> {
    const f = form.getRawValue();

    return new Promise((resolve, reject) => {
      for (const param of Object.keys(f)) {
        if (param.includes('att_')) {
          this.municipalityToEdit[param.substr(4)] = f[param];
        }
      }

      if (!form.controls['logo'].dirty) {
        resolve();
      }
      else {
        this.files.uploadFile(this.file, `taxonomy_term`, `municipality`, `image`)
          .then((drupalFileStructure) => {
            console.log(`Successful logo upload.`);
            this.crud.getIndividual(FileModel, drupalFileStructure['uuid'][0].value)
              .toPromise()
              .then((file: FileModel) => {
                this.newLogo = file;
                resolve();
              })
              .catch((err) => reject(err));
          })
          .catch((err) => reject(err));
      }
    });
  }

  public cancel(): void {
    this.router.navigate(['admin', 'municipalities']).catch(err => console.error(err));
  }

}
