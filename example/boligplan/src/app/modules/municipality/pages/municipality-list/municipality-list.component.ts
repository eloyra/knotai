import { Component, OnInit } from '@angular/core';
import { Municipality } from '../../../../core/models/municipality';
import { CrudService } from '../../../../core/http/crud.service';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Globals } from '../../../../Globals';
import { ConfirmModalComponent } from '../../../../shared/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-municipality-list',
  templateUrl: './municipality-list.component.html',
  styleUrls: ['./municipality-list.component.scss']
})
export class MunicipalityListComponent implements OnInit {

  public municipalities: Municipality[];
  public toDelete: Municipality;

  public show = false;

  public imageHost = this.globals.host;
  public logoPlaceholder = this.imageHost + this.globals.generalConfig.noLogoPlaceholder;

  constructor(private crud: CrudService,
              private router: Router,
              private modalService: NgbModal,
              private globals: Globals,
  ) {}

  ngOnInit() {
    this.load();
  }

  public load(): void {
    this.show = false;
    this.crud.getCollection(Municipality, ['image']).subscribe((municipalityModels: JsonApiQueryData<Municipality>) => {
      this.municipalities = municipalityModels.getModels();
    }).add(() => {
      this.show = true;
    });
  }

  public edit(municipality: Municipality): void {
    this.router.navigate(['admin', 'municipalities', 'edit', municipality.id]).catch(err => console.error(err));
  }

  public open(modal, municipality: Municipality) {
    this.toDelete = municipality;
    const deleteModal = this.modalService.open(ConfirmModalComponent, {centered: true});
    deleteModal.componentInstance.title = 'Slet kommune';
    deleteModal.componentInstance.text = `Er du sikker på, at du vil slette kommunen <b>${municipality.name}</b>?`;
    deleteModal.result.then((result) => {
      console.log(`Deleting municipality ${municipality.name}`);
      this.delete(municipality)
        .then(() => {
          this.load();
        })
        .catch((err) => {
          console.error(err);
        });
    }, () => {
      // Here there is a "reason" argument available if needed.
      console.log(`The delete confirmation was dismissed. No action will be taken.`);
    });
  }

  public delete(municipality: Municipality): Promise<object> {
    return this.crud.delete(Municipality, municipality.id).toPromise();
  }

}
