import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CrudService } from '../../../../core/http/crud.service';
import { UserService } from '../../../../core/http/user.service';
import { Globals } from '../../../../Globals';
import { FormErrorHandlerService } from '../../../../core/services/form-error-handler.service';
import { FileService } from '../../../../core/http/file.service';
import { File as FileModel } from '../../../../core/models/file';
import { Municipality } from '../../../../core/models/municipality';
import {
  customImageFormatValidator,
  customLengthValidator,
  customRequiredValidator
} from '../../../../shared/validators/custom-validators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-municipality-add',
  templateUrl: './municipality-add.component.html',
  styleUrls: ['./municipality-add.component.scss']
})
export class MunicipalityAddComponent implements OnInit {

  addMunicipalityForm = this.fb.group({
    att_code: ['', [customRequiredValidator('att_code', 'Koden kan ikke være tom.'), customLengthValidator(5, -1, 'att_code', 'Den angivne kode er for lang.')]],
    att_name: ['', customRequiredValidator('att_name', 'Navnet kan ikke være tomt.')],
    att_description: ['', customRequiredValidator('att_description', 'Beskrivelsen kan ikke være tom.')],
    att_contact: ['', customRequiredValidator('att_contact', 'Kontaktoplysningerne kan ikke være tomme.')],
    logo: [null, [customRequiredValidator('logo', 'Et logo er påkrævet.'),
      customImageFormatValidator(this.globals.generalConfig.allowedLogoTypes.map((value: string) => value.split('/')[1]), 'logo', `Logoet er ikke i et gyldigt format. Vi accepterer følgende formater: ${this.globals.generalConfig.allowedLogoTypes.map((value: string) => value.split('/')[1]).join(', ')}`)]],
    att_active: [true, []],
  });

  public allowedLogoTypes = this.globals.generalConfig.allowedLogoTypes.join(',');
  public allowedLogoTypesUserFriendly = this.globals.generalConfig.allowedLogoTypes.map((value: string) => value.split('/')[1]).join(', ');
  private file: File = null;

  public submitting = false;

  /* Error management */
  public errors = {};

  constructor(
    private crud: CrudService,
    private users: UserService,
    private globals: Globals,
    private errorHandler: FormErrorHandlerService,
    private router: Router,
    private fb: FormBuilder,
    private files: FileService
  ) {}

  ngOnInit() {
  }

  /**
   * Gets the file selected in the form.
   */
  public onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      this.file = event.target.files[0];
    }
  }

  public onSubmit(): void {
    if (this.validateForm(this.addMunicipalityForm)) {
      this.submitting = true;
      this.files.uploadFile(this.file, `taxonomy_term`, `municipality`, `image`)
        .then((drupalFileStructure) => {
          console.log(`Successful logo upload.`);
          this.crud.getIndividual(FileModel, drupalFileStructure['uuid'][0].value)
            .toPromise()
            .then((file: FileModel) => {
              const newMunicipality: Municipality = this.crud.create(Municipality, this.processParams(this.addMunicipalityForm));
              console.log(`New municipality created. Not saved.`);
              newMunicipality.image = file;
              console.log(`Logo asigned to new municipality.`);
              newMunicipality.save()
                .toPromise()
                .then(() => {
                  console.warn(`New municipality saved.`);
                  this.router.navigate(['admin', 'municipalities']).catch(err => console.error(err));
                })
                .catch((err) => {
                  this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
                  console.error(this.errors);
                });
            })
            .catch((err) => {
              this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
              console.error(this.errors);
            });
        })
        .catch((err) => {
          this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
          console.error(this.errors);
        })
        .then(() => this.submitting = false);
    }
    else {
      console.error(this.errors);
    }
  }

  /**
   * Handles validation of the form. Checks that the Validators are met.
   */
  private validateForm(form: FormGroup): boolean {
    this.errors = {};
    let isCorrect = true;

    if (form.invalid) {
      isCorrect = false;
      this.errors = this.errorHandler.handleControlErrors(this.errors, form.controls);
    }

    return isCorrect;
  }

  /**
   * Creates a properly formated municipality data object to create the new municipality using the 'att' prefixed form fields.
   */
  private processParams(form: FormGroup): object {
    const f = form.getRawValue();
    const municipalityData = {};

    for (const param of Object.keys(f)) {
      if (param.includes('att_')) {
        municipalityData[param.substr(4)] = f[param];
      }
    }

    return municipalityData;
  }

  public cancel(): void {
    this.router.navigate(['admin', 'municipalities']).catch(err => console.error(err));
  }

}
