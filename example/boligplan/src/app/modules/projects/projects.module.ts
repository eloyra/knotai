import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { UserHomeComponent } from './pages/user-home/user-home.component';
import { CoreModule } from '../../core/core.module';
import { ProfileComponent } from './pages/profile/profile.component';
import { ChangePasswordComponent } from './modals/change-password/change-password.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChangeEmailComponent } from './modals/change-email/change-email.component';
import { SharedModule } from '../../shared/shared.module';
import { ArchiveComponent } from './pages/archive/archive.component';
import { DummyProjectAddComponent } from './components/dummy-project-add/dummy-project-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewProjectComponent } from './modals/new-project/new-project.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    ProjectsRoutingModule,
    NgbModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProjectsComponent,
    ProjectListComponent,
    UserHomeComponent,
    ProfileComponent,
    ChangeEmailComponent,
    ChangePasswordComponent,
    ArchiveComponent,
    DummyProjectAddComponent,
    NewProjectComponent
  ],
  entryComponents: [
    ChangeEmailComponent,
    ChangePasswordComponent,
    NewProjectComponent
  ]
})
export class ProjectsModule {}
