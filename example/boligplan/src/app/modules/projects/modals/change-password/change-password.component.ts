import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../../core/models/user';
import { customRequiredValidator } from '../../../../shared/validators/custom-validators';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormErrorHandlerService } from '../../../../core/services/form-error-handler.service';
import { CrudService } from '../../../../core/http/crud.service';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  private user: User;
  changePasswordForm = this.fb.group({
    current_pass: ['', [customRequiredValidator('current_pass', `The current password can't be empty.`)]],
    new_pass: ['', [customRequiredValidator('new_pass', `The new password can't be empty.`)]],
    confirm_pass: ['', [customRequiredValidator('confirm_pass', `You need to confirm your new password to proceed.`)]]
  });
  private errors = {};
  public submitting = false;

  constructor(public activeModal: NgbActiveModal,
              public fb: FormBuilder,
              public errorHandler: FormErrorHandlerService,
              private crud: CrudService,
              private auth: AuthenticationService
  ) {}

  ngOnInit() {}

  public changePassword(): void {
    this.submitting = true;
    this.validateForm(this.changePasswordForm)
      .then((isCorrect) => {
        if (isCorrect) {
          this.user.pass = [{
            existing: this.changePasswordForm.getRawValue()['current_pass'],
            value: this.changePasswordForm.getRawValue()['new_pass'],
          }];
          this.crud.update(this.user)
            .toPromise()
            .then(() => {
              this.auth.authenticate(this.user.name, this.changePasswordForm.getRawValue()['new_pass'])
                .then(() => {
                  this.activeModal.close('success');
                })
                .catch((err) => console.error(err));
            })
            .catch(err => {
              this.errors = this.errorHandler.processDrupalErrors({...this.errors}, err);
              console.error(this.errors);
            });
        }
        else {
          // TODO: Pasar errores a componente
          console.error(this.errors);
        }
      })
      .then(() => this.submitting = false);
  }

  private validateForm(form: FormGroup): Promise<boolean> {
    this.errors = {};
    const validationErrors = {};
    let isCorrect = true;

    if (form.dirty) {
      const f = form.getRawValue();

      return new Promise((resolve) => {

        if (form.invalid) {
          isCorrect = false;
          this.errors = this.errorHandler.handleControlErrors({...this.errors}, form.controls);
          resolve(isCorrect);
        }
        else {
          if (f['new_pass'] !== f['confirm_pass']) {
            validationErrors['confirm_pass'] = `The passwords do not match.`;
            isCorrect = false;
            this.errors = this.errorHandler.handleValidationErrors({...this.errors}, validationErrors);
            resolve(isCorrect);
          }
          else {
            this.auth.authenticate(this.user.name, f['current_pass'])
              .then(() => {
                resolve(isCorrect);
              })
              .catch(() => {
                validationErrors['current_pass'] = 'The current password is incorrect.';
                isCorrect = false;
                this.errors = this.errorHandler.handleValidationErrors({...this.errors}, validationErrors);
                resolve(isCorrect);
              });
          }
        }
      });
    }
  }

}
