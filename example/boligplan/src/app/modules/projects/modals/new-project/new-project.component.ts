import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectTemplate } from '../../../../core/models/project-template';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss']
})
export class NewProjectComponent implements OnInit {

  selected: ProjectTemplate;
  templates: ProjectTemplate[] = [
    {
      isNew: true,
      name: 'Elvarme',
      description: 'Vis boligejere med elvarme deres besparelsespotentiale.',
      potential: 912666,
      filters: [
        'Energimærke: A, B, C, D, E, F, G',
        'Opvarmning: El',
        'Beregnet årligt forbrug: 0 - 500+ MWh',
        'Beregnet besparelse: 5.000-49.999'
      ]
    },
    {
      name: 'Jordvarme',
      description: 'Vis boligejere med oliefyr deres besparelsespotentiale ved at skifte.',
      potential: 2265,
      filters: [
        'Energimærke: E,F,G',
        'Opvarmning: Oliefyr',
        'Beregnet årligt forbrug: 0 - 500 mWh',
        'Besparelsespotentiale: 10.000-19.990'
      ]
    },
    {
      name: 'Energimærke E-G',
      description: 'Vis boligejere med energimærke E-G deres besparelsespotentiale ved at energirenovere kælderen.',
      potential: 2265,
      filters: [
        'Energimærke: E,F,G',
        'Opvarmning: Jordvarme',
        'Beregnet årligt forbrug: 0 - 500 mWh'
      ]
    },
    {
      name: 'Brændeovne',
      description: 'Vis boligejere med brændeovne deres besparelsespotentiale.',
      potential: 1987,
      filters: [
        'Energimærke: A',
        'Opvarmning: El',
        'Beregnet årligt forbrug: 0 - 500 mWh',
        'Besparelsespotentiale: 10.000-19.990'
      ]
    },
    {
      name: 'Ekstraordinært højt forbrug',
      description: 'Vis boligejere med et beregnet forbrug den øverste middelværdi deres besparelsespotentiale.',
      potential: 2265,
      filters: [
        'Energimærke: E,F,G',
        'Beregnet årligt forbrug: 500+ mWh'
      ]
    }
  ];

  constructor(
    private modalService: NgbModal,
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit() {}

  selectTemplate(modal, template?: ProjectTemplate) {
    this.selected = template ? template : {
      name: '',
      description: '',
      potential: 0,
      filters: []
    };
    this.modalService.open(modal, {centered: true}).result.then(() => {
      console.log(this.selected);
    }, () => false);
  }

}
