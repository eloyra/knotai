import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from '@angular/forms';
import { customEmailValidator, customRequiredValidator } from '../../../../shared/validators/custom-validators';
import { FormErrorHandlerService } from '../../../../core/services/form-error-handler.service';
import { User } from '../../../../core/models/user';
import { CrudService } from '../../../../core/http/crud.service';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit {

  private user: User;
  changeEmailForm = this.fb.group({
    new_email: ['', [customRequiredValidator('new_email', `The new email can't be empty.`), customEmailValidator('new_email', 'The provided email is not a valid email address.')]],
    password: ['', [customRequiredValidator('password', `You need to fill your password to proceed.`)]]
  });
  private errors = {};
  public submitting = false;

  constructor(public activeModal: NgbActiveModal,
              public fb: FormBuilder,
              public errorHandler: FormErrorHandlerService,
              private crud: CrudService,
              private auth: AuthenticationService,
  ) {}

  ngOnInit() {}

  public changeEmail(): void {
    this.submitting = true;
    this.validateForm(this.changeEmailForm)
      .then((isCorrect) => {
        if (isCorrect) {
          this.user.mail = this.changeEmailForm.getRawValue()['new_email'];
          this.user.pass = [ {existing: this.changeEmailForm.getRawValue()['password']} ];
          this.crud.update(this.user)
            .toPromise()
            .then(() => {
              this.auth.authenticate(this.user.name, this.changeEmailForm.getRawValue()['password'])
                .then(() => {
                  this.activeModal.close('success');
                })
                .catch((err) => console.error(err));
            })
            .catch(err => {
              this.errors = this.errorHandler.processDrupalErrors({...this.errors}, err);
              console.error(this.errors);
            });
        }
        else {
          // TODO: Pasar errores a componente
          console.error(this.errors);
        }
      })
      .then(() => this.submitting = false);
  }

  private validateForm(form: FormGroup): Promise<boolean> {
    this.errors = {};
    const validationErrors = {};
    let isCorrect = true;

    if (form.dirty) {
      const f = form.getRawValue();

      return new Promise((resolve) => {

        if (form.invalid) {
          isCorrect = false;
          this.errors = this.errorHandler.handleControlErrors({...this.errors}, form.controls);
          resolve(isCorrect);
        }
        else {
          this.auth.authenticate(this.user.name, f['password'])
            .then(() => {
              resolve(isCorrect);
            })
            .catch(() => {
              validationErrors['password'] = 'The password is incorrect.';
              isCorrect = false;
              this.errors = this.errorHandler.handleValidationErrors({...this.errors}, validationErrors);
              resolve(isCorrect);
            });
        }
      });
    }
  }

}
