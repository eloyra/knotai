import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsComponent } from './pages/projects/projects.component';
import { RouteAuthGuard } from '../../core/authentication/route-auth.gard';
import { UserHomeComponent } from './pages/user-home/user-home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ArchiveComponent } from './pages/archive/archive.component';
import { DummyProjectAddComponent } from './components/dummy-project-add/dummy-project-add.component';

const routes: Routes = [
  {
    path: '',
    component: UserHomeComponent,
    canActivate: [RouteAuthGuard],
    data: {
      roles: ['administator', 'municipality_admin', 'municipality_user'],
    },
    children: [
      {
        path: 'campaigns',
        children: [
          {
            path: '',
            component: ProjectsComponent
          },
          {
            path: 'archive',
            component: ArchiveComponent
          },
          {
            path: 'create',
            component: DummyProjectAddComponent
          },
        ]
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: '',
        redirectTo: 'campaigns'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule {}
