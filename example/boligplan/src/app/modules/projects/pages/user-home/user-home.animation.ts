import { animate, state, style, transition, trigger } from '@angular/animations';

export const showHideNewProject = trigger('showHideNewProject', [
  transition(':enter', [
    style({
      opacity: 0,
      transform: 'scale(0)'
    }),
    animate('300ms 1s ease-in-out', style({
      opacity: 1,
      transform: 'scale(1)'
    }))
  ]),
  transition(':leave', [
    animate('150ms ease-in-out', style({
      opacity: 0,
      transform: 'scale(0)'
    }))
  ])
]);

export const showHideHelpArrow = trigger('showHideHelpArrow', [
  state('true', style({
    opacity: 1,
    pointerEvents: 'all'
  })),
  state('false', style({
    opacity: 0,
    pointerEvents: 'none'
  })),
  transition('true <=> false', animate('200ms ease-in-out'))
]);
