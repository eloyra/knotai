import { Component, HostBinding, HostListener, OnInit } from '@angular/core';
import { UserService } from '../../../../core/http/user.service';
import { showHideNewProject } from './user-home.animation';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NewProjectComponent } from '../../modals/new-project/new-project.component';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss'],
  animations: [showHideNewProject]
})
export class UserHomeComponent implements OnInit {

  scrollOffset = 0;

  @HostBinding('class.with-help-arrow') helpArrowVisible = true;

  constructor(
    private userService: UserService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {}

  @HostListener('scroll', ['$event'])
  doSomethingOnInternalScroll($event: Event) {
    this.scrollOffset = $event.srcElement.scrollTop;
    if (
      (this.scrollOffset > 20 && this.helpArrowVisible) ||
      (this.scrollOffset <= 20 && !this.helpArrowVisible)
    ) {
      this.helpArrowVisible = !this.helpArrowVisible;
    }
  }

  openNewProject() {
    this.modalService.open(NewProjectComponent, {
      size: 'lg',
      windowClass: 'new-project',
      centered: true
    });
  }

}
