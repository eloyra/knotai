import { Component, OnInit } from '@angular/core';
import { User } from '../../../../core/models/user';
import { UserService } from '../../../../core/http/user.service';
import { showHideNewProject } from '../user-home/user-home.animation';
import { Campaign } from '../../../../core/models/campaign';
import { CrudService } from '../../../../core/http/crud.service';
import { CampaignService } from '../../../../core/http/campaign.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
  animations: [showHideNewProject]
})
export class ProjectsComponent implements OnInit {

  public currentUser: User;
  public campaigns: Campaign[];
  public isAdmin: boolean;

  constructor(
    private userService: UserService,
    private crud: CrudService,
    private campaignService: CampaignService,
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser()
      .then(user => {
        this.currentUser = user;
        this.isAdmin = this.userService.isAdmin(user);
        this.loadCampaigns();
      })
      .catch(() => this.currentUser = null);
  }

  private loadCampaigns(): void {
    if (this.isAdmin) {
      this.campaignService.getOrderedCampaigns()
        .then((campaigns: Campaign[]) => this.campaigns = campaigns)
        .catch(err => console.error(err));
    }
    else if (this.currentUser.municipality) {
      this.campaignService.getOrderedCampaignsByMunicipality(this.currentUser.municipality.internalId)
        .then((campaigns: Campaign[]) => this.campaigns = campaigns)
        .catch(err => console.error(err));
    }
  }

}
