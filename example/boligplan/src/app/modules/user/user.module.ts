import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { CoreModule } from '../../core/core.module';
import { UserAddComponent } from './pages/user-add/user-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { UserEditComponent } from './pages/user-edit/user-edit.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    UserRoutingModule
  ],
  declarations: [
    UserAddComponent,
    UserEditComponent
  ]
})
export class UserModule {}
