import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAddComponent } from './pages/user-add/user-add.component';
import { UserListComponent } from './pages/user-list/user-list.component';
import { UserEditComponent } from './pages/user-edit/user-edit.component';
import { RouteAuthGuard } from '../../core/authentication/route-auth.gard';

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteAuthGuard],
    data: {
      roles: ['administator', 'municipality_admin'],
    },
    children: [
      {
        path: 'create',
        component: UserAddComponent
      },
      {
        path: 'edit/:id',
        component: UserEditComponent
      },
      {
        path: '',
        component: UserListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
