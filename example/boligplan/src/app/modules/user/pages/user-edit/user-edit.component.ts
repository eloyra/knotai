import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../../../core/models/user';
import { CrudService } from '../../../../core/http/crud.service';
import { Municipality } from '../../../../core/models/municipality';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { Role } from '../../../../core/models/role';
import { Globals } from '../../../../Globals';
import { UserService } from '../../../../core/http/user.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { FormErrorHandlerService } from '../../../../core/services/form-error-handler.service';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';
import { customEmailValidator, customRequiredValidator } from '../../../../shared/validators/custom-validators';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  editUserForm = this.fb.group({});

  public currentUser: User;
  public userToEdit: User;
  public roles: Role[];
  public municipalities: Municipality[];

  public hasRoles = {};
  public errors = {};

  public show = false;
  public selectMunicipality: boolean;
  public selectStatus: boolean;
  public municipalitiesReady = false;
  public rolesReady = false;
  public submitting = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private crud: CrudService,
    private globals: Globals,
    private users: UserService,
    private errorHandler: FormErrorHandlerService,
    private auth: AuthenticationService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    const uuid = this.route.snapshot.paramMap.get('id');
    this.crud.getIndividual(User, uuid, ['roles', 'municipality'])
      .toPromise()
      .then((user: User) => {
        this.userToEdit = user;
        this.editUserForm.addControl(`att_mail`, new FormControl(this.userToEdit.mail, [customRequiredValidator('att_mail', 'E-mailen kan ikke være tom.'), customEmailValidator('att_mail', 'Den angivne email er ikke en gyldig email-adresse.')]));
        this.editUserForm.addControl(`att_firstName`, new FormControl(this.userToEdit.firstName, [customRequiredValidator('att_firstName', 'Fornavnet kan ikke være tomt.')]));
        this.editUserForm.addControl(`att_lastName`, new FormControl(this.userToEdit.lastName, [customRequiredValidator('att_lastName', 'Efternavnet kan ikke være tomt.')]));
        this.editUserForm.addControl(`att_bool_isActive`, new FormControl({
          value: this.userToEdit.isActive ? '1' : '0',
          disabled: this.userToEdit.municipality ? !this.userToEdit.municipality.active : false
        }, [customRequiredValidator('att_bool_isActive', 'Du skal vælge en brugerstatus.')]));
        this.users.getCurrentUser()
          .then((currentUser: User) => {
            this.currentUser = currentUser;
            if (this.userToEdit.id === this.currentUser.id) { this.editUserForm.addControl(`password`, new FormControl('', [customRequiredValidator('password', 'Du skal introducere dit kodeord.')])); }
            if (!this.checkLegitimateAccess()) {
              console.warn(`You can't access this page.`);
              this.location.back();
            } else {
              this.getProcessedRoles();
              this.getProcessedMunicipalities();
              this.show = true;
            }
          })
          .catch((err) => {
            console.warn(`Can't initialize user-edit component.`);
            console.error(err);
          });
      });
  }

  private checkLegitimateAccess(): boolean {
    if (this.users.isAdmin(this.currentUser)) {
      this.selectStatus = true;
      return true;
    }
    else if ((this.currentUser.municipality === this.userToEdit.municipality &&
      this.users.hasRole(this.currentUser, this.globals.generalConfig.municipalAdministratorRoles) &&
      !this.users.hasRole(this.userToEdit, this.globals.generalConfig.municipalAdministratorRoles.concat(this.globals.generalConfig.administratorRoles)))) {

      this.selectStatus = this.currentUser.internalId !== this.userToEdit.internalId;
      return true;
    }
    else if (this.currentUser.internalId === this.userToEdit.internalId) {
      this.selectStatus = false;
      return true;
    }
    else {
      this.selectStatus = false;
      return false;
    }
  }

  /**
   * Gets filtered roles based on whether they can be managed from the app and whether they can be managed by the current user.
   */
  public getProcessedRoles(): void {
    this.roles = [];
    this.hasRoles = [];

    // Check getComplexFilter docs.
    const rolesNeeded = {
      g0: {
        condition: 'OR',
        queries: {
          internalId: {
            operator: '=',
            // This generates duplicates, check.
            values: this.users.rolesToGrant(this.currentUser).concat(this.userToEdit.roles ? this.userToEdit.roles.map((value: Role) => value.internalId) : [])
          }
        }
      }
    };

    this.crud.getCollection(Role, [], {}, this.crud.getComplexFilter(rolesNeeded)).subscribe(
      (roleModels: JsonApiQueryData<Role>) => {
        this.roles = roleModels.getModels();
      },
      (err) => { console.error(err); },
      () => {
        if (this.roles) {
          for (const role of this.roles) {
            this.hasRoles[role.internalId] = this.users.hasRole(this.userToEdit, [role.internalId]);
          }
          if (this.roles.length > 1) {
            const controlValue = this.userToEdit.roles && this.userToEdit.roles.length === 1 ? {value: this.userToEdit.roles[0].internalId} : this.roles.length === 1 ? {value: this.roles[0].internalId} : null;
            const roleControl = new FormControl(controlValue, [customRequiredValidator('rel_role', 'You must select a role.')]);
            this.editUserForm.addControl(`rel_role`, roleControl);
          }
        }
        this.rolesReady = true;
      });
  }

  public getProcessedMunicipalities(): void {
    this.municipalities = [];

    // Check getComplexFilter docs.
    const municipalitiesNeeded = {
      g0: {
        condition: 'OR',
        queries: {
          active: {
            operator: '=',
            values: [1]
          },
          internalId: {
            operator: '=',
            values: [this.userToEdit.municipality ? this.userToEdit.municipality.internalId : '']
          }
        }
      }
    };

    if (this.users.isAdmin(this.currentUser)) {
      this.selectMunicipality = true;
      this.crud.getCollection(Municipality, [], {}, this.crud.getComplexFilter(municipalitiesNeeded)).subscribe(
        (municipalityModels: JsonApiQueryData<Municipality>) => {
          this.municipalities = municipalityModels.getModels();
        },
        (err) => { console.error(err); },
        () => {
          this.editUserForm.addControl('rel_municipality', new FormControl(
            this.userToEdit.municipality ? this.userToEdit.municipality.internalId : this.municipalities[0].internalId, customRequiredValidator('rel_municipality')));
          this.municipalitiesReady = true;
        });
    } else {
      this.selectMunicipality = false;
      this.municipalities.push(this.currentUser.municipality);
      this.editUserForm.addControl('rel_municipality', new FormControl({
        value: this.currentUser.municipality.name,
        disabled: true
      }, customRequiredValidator('rel_municipality')));
      this.municipalitiesReady = true;
    }
  }

  public onSubmit(): void {
    // We discard past form errors.
    this.errors = {};

    this.validateForm(this.editUserForm)
      .then((isCorrect) => {
        if (isCorrect) {
          this.submitting = true;
          this.processParams(this.editUserForm);
          console.log(this.editUserForm);
          console.log(this.userToEdit);
          this.crud.update(this.userToEdit, ['roles', 'municipality'])
            .toPromise()
            .then((editedUser: User) => {
              console.log(`User succesfully updated.`);
              if (this.currentUser.id === this.userToEdit.id) {
                this.users.relogUser(editedUser.name, this.editUserForm.getRawValue()['password'])
                  .then(() => {
                    this.router.navigate(['admin', 'users']).catch(err => console.error(err));
                  })
                  .catch((err) => {
                    console.error(`Couldn't log in again or no longer have permissions.`);
                    this.router.navigate(['']).catch(errr => console.error(errr));
                    console.warn(err);
                  });
              }
              this.router.navigate(['admin', 'users']).catch(err => console.error(err));
            })
            .catch((err) => {
              console.warn(`Error updating user.`);
              this.userToEdit.rollbackAttributes();
              this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
              console.warn(this.errors);
            })
            .then(() => this.submitting = false);
        }
        else {
          console.log(`Failed validation.`);
          // TODO: Send this object to the error component.
          console.error(this.errors);
        }
      });
  }

  /**
   * Validation logic for the form. Partial validation since Drupal also validates.
   *
   * We check here that:
   *  - required fields are filled
   *  - the email is an email address
   *  - the activation process is selected
   *  - at least one role is selected
   *  - when a municipality role is selected, the municipality field isn't empty
   */
  private validateForm(form: FormGroup): Promise<boolean> {
    this.errors = {};
    const validationErrors = {};
    let isCorrect = true;

    if (form.controls['rel_role']) {
      if (this.globals.config['roles'].administratorRoles.includes(form.controls['rel_role'].value)) {
        form.removeControl('rel_municipality');
      }
    }

    if (form.dirty) {
      const f = form.getRawValue();

      return new Promise((resolve) => {

        if (form.invalid) {
          isCorrect = false;
          this.errors = this.errorHandler.handleControlErrors(this.errors, form.controls);
        }

        if (this.currentUser.id === this.userToEdit.id) {
          this.auth.authenticate(this.currentUser.name, f['password'])
            .then(() => {
              this.errors = this.errorHandler.handleValidationErrors(this.errors, validationErrors);
              resolve(isCorrect);
            })
            .catch(() => {
              validationErrors['password'] = 'The password is incorrect.';
              isCorrect = false;
              this.errors = this.errorHandler.handleValidationErrors(this.errors, validationErrors);
              resolve(isCorrect);
            });
        }
        else {
          this.errors = this.errorHandler.handleValidationErrors(this.errors, validationErrors);
          resolve(isCorrect);
        }
      });
    }
  }

  /**
   * Handles the logic that modifies the user model.
   * Parameters in the form are preffixed for easy processing:
   *  att_: Attributes that belong to the User model.
   *  bool_: Attributes that need to represent a boolean value.
   *  rel_: Attributes that belong to the User model but as relationships, which need special handling.
   *  role_: Indicates that the parameter is the name of a role (because there can be many in a single form),
   *  for which we need a Role object.
   *
   *  No prefix (from this list) means that the parameter doesn't belong in the User model and would be ignored in this stage of processing.
   */
  private processParams(form: FormGroup): void {
    const f = form.getRawValue();
    // We reset previous roles.
    if (this.roles.length > 1) { this.userToEdit.roles = []; }
    let i: number;

    for (const param of Object.keys(f)) {
      if (param.includes('att_bool_')) {
        this.userToEdit.isActive = f[param] === '1';
      }
      else if (param.includes('att_')) {
        this.userToEdit[param.substr(4)] = f[param];
      }
      else if (param.includes('rel_')) {
        if (param.includes('role') && f[param] != null) {
          if ((i = this.roles.findIndex((role: Role) => role.internalId === f[param])) !== -1) {
            this.userToEdit.roles.push(this.roles[i]);
          }
        }
        else if (this.selectMunicipality && param.includes('municipality')) {
          if ((i = this.municipalities.findIndex((municipality: Municipality) => municipality.internalId === parseInt(f[param], 10))) !== -1) {
            this.userToEdit.municipality = this.municipalities[i];
          }
        }
      }
    }

    if (form.controls['rel_role']) {
      if (this.globals.config['roles'].administratorRoles.includes(form.controls['rel_role'].value)) {
        this.userToEdit.municipality = null;
      }
    }

    // TODO: Implement.
    // If we are making the user an administrator, the municipality is ignored.
    /*if (this.users.isAdmin(this.userToEdit)) {
      this.userToEdit.municipality = null;
    }*/
  }

  public cancel(): void {
    this.router.navigate(['admin', 'users']).catch(err => console.error(err));
  }
}
