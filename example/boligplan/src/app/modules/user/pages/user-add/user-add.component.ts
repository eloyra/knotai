import { Component, OnInit } from '@angular/core';
import { Role } from '../../../../core/models/role';
import { CrudService } from '../../../../core/http/crud.service';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { User } from '../../../../core/models/user';
import { UserService } from '../../../../core/http/user.service';
import { Globals } from '../../../../Globals';
import { FormErrorHandlerService } from '../../../../core/services/form-error-handler.service';
import { Municipality } from '../../../../core/models/municipality';
import { customEmailValidator, customRequiredValidator } from '../../../../shared/validators/custom-validators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  addUserForm = this.fb.group({
    att_mail: ['', [customRequiredValidator('att_mail', 'E-mailen kan ikke være tom.'), customEmailValidator('att_mail', 'Den angivne email er ikke en gyldig email-adresse.')]],
    att_firstName: ['', customRequiredValidator('att_firstName', 'Fornavnet kan ikke være tomt.')],
    att_lastName: ['', customRequiredValidator('att_lastName', 'Efternavnet kan ikke være tomt.')],
    att_bool_isActive: ['1', customRequiredValidator('att_bool_isActive', 'Du skal vælge en aktiveringsproces for den nye konto.')],
  });

  public currentUser: User;
  public roles: Role[];
  public municipalities: Municipality[];

  public selectMunicipality: boolean;
  public municipalitiesReady = false;
  public rolesReady = false;

  public showActivationModes = false;
  public submitting = false;

  /* Error management */
  public errors = {};

  constructor(
    private crud: CrudService,
    private users: UserService,
    private globals: Globals,
    private errorHandler: FormErrorHandlerService,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.users.getCurrentUser()
      .then((user: User) => {
        this.currentUser = user;
        this.getProcessedRoles();
        this.getProcessedMunicipalities();
      })
      .catch((err) => {
        console.warn(`Can't initialize user-add component.`);
        console.error(err);
      });
  }

  /**
   * Gets all the roles and filters them based on whether they can be managed from the app and whether they can be managed by the current user.
   */
  public getProcessedRoles(): void {
    this.roles = [];

    // Check getComplexFilter docs.
    const rolesNeeded = {
      g0: {
        condition: 'OR',
        queries: {
          internalId: {
            operator: '=',
            values: this.users.rolesToGrant(this.currentUser)
          }
        }
      }
    };

    this.crud.getCollection(Role, [], {}, this.crud.getComplexFilter(rolesNeeded)).subscribe(
      (roleModels: JsonApiQueryData<Role>) => {
        this.roles = roleModels.getModels();
      },
      () => {
      },
      () => {

        if (this.roles) {
          if (this.roles.length > 1) {
            const controlValue = this.roles.length === 1 ? {value: this.roles[0].internalId} : null;
            const roleControl = new FormControl(controlValue, [customRequiredValidator('rel_role', 'Du skal vælge en rolle for den nye konto.')]);
            this.addUserForm.addControl(`rel_role`, roleControl);
          }
        }

        this.rolesReady = true;
      });
  }

  public getProcessedMunicipalities(): void {
    this.municipalities = [];
    if (this.users.isAdmin(this.currentUser)) {
      this.selectMunicipality = true;
      this.crud.getCollection(Municipality, [], {}, {active: 1}).subscribe(
        (municipalityModels: JsonApiQueryData<Municipality>) => {
          this.municipalities = municipalityModels.getModels();
        },
        () => {},
        () => {
          this.addUserForm.addControl('rel_municipality', new FormControl('', customRequiredValidator('rel_municipality')));
          this.municipalitiesReady = true;
        });
    } else {
      this.selectMunicipality = false;
      this.municipalities.push(this.currentUser.municipality);
      this.addUserForm.addControl('rel_municipality', new FormControl({
        value: this.currentUser.municipality.name,
        disabled: true
      }, customRequiredValidator('rel_municipality')));
      this.municipalitiesReady = true;
    }
  }

  public onSubmit(): void {
    // We discard past form errors.
    this.errors = {};

    if (this.validateForm(this.addUserForm)) {
      const userData = this.processParams(this.addUserForm);
      this.submitting = true;

      // This is a necessary workaround because Drupal doesn't send the activation email when user is set as active on the moment of
      // creation. We need to set the isActive param to false, and AFTER user has been created, update it to true.
      const requiresAutomaticActivation = userData['isActive'];
      userData['isActive'] = false;

      this.crud.createAndSave(User, userData)
        .toPromise()
        .then((newUser: User) => {
          console.log(`Successfully created new user.`);
          console.log(newUser);
          if (requiresAutomaticActivation) {
            newUser.isActive = true;
            this.crud.update(newUser).subscribe(() => {},
              (err) => { console.log(`Error sending activation email:\n${err}`); },
              () => { console.log(`Successfully flagged activation email to be sent.`); });
          }
          this.router.navigate(['admin', 'users']).catch(err => console.error(err));
        })
        .catch((err) => {
          console.log(`Error creating new user.`);
          // TODO: Send this object to the error component.
          this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
          console.log(this.errors);
        })
        .then(() => this.submitting = false);
    } else {
      if (!this.addUserForm.controls['rel_municipality']) {
        this.addUserForm.addControl('rel_municipality', new FormControl('', customRequiredValidator('rel_municipality')));
      }
      console.log(`Failed validation.`);
      // TODO: Send this object to the error component.
      console.log(this.errors);
    }
  }

  public cancel(): void {
    this.router.navigate(['admin', 'users']).catch(err => console.error(err));
  }

  /**
   * Validation logic for the form. Partial validation since Drupal also validates.
   *
   * We check here that:
   *  - required fields are filled
   *  - the email is an email address
   *  - the activation process is selected
   *  - at least one role is selected
   *  - when a municipality role is selected, the municipality field isn't empty
   */
  private validateForm(form: FormGroup): boolean {
    this.errors = {};
    let isCorrect = true;

    if (form.controls['rel_role']) {
      if (this.globals.config['roles'].administratorRoles.includes(form.controls['rel_role'].value)) {
        form.removeControl('rel_municipality');
      }
    }

    if (form.invalid) {
      isCorrect = false;
      this.errors = this.errorHandler.handleControlErrors(this.errors, form.controls);
    }

    return isCorrect;
  }

  /**
   * Handles the logic that creates a properly formated data object for user creation.
   * Parameters in the form are preffixed for easy processing:
   *  att_: Attributes that belong to the User model.
   *  bool_: Attributes that need to represent a boolean value.
   *  rel_: Attributes that belong to the User model but as relationships, which need special handling.
   *  role_: Indicates that the parameter is the name of a role (because there can be many in a single form),
   *  for which we need a Role object.
   *
   *  No prefix (from this list) means that the parameter doesn't belong in the User model and would be ignored in this stage of processing.
   */
  private processParams(form: FormGroup): object {
    const f = form.getRawValue();
    const userData = {};
    userData['roles'] = [];
    userData['municipality'] = [];
    let i: number;

    for (const param of Object.keys(f)) {
      if (param.includes('att_bool_')) {
        userData[param.substr(9)] = f[param] === '1';
      }
      else if (param.includes('att_')) {
        userData[param.substr(4)] = f[param];
      }
      else if (param.includes('rel_')) {
        if (param.includes('role') && f[param] != null) {
          if ((i = this.roles.findIndex((role: Role) => role.internalId === f[param])) !== -1) {
            userData['roles'].push(this.roles[i]);
          }
        }
        else if (this.selectMunicipality && param.includes('municipality')) {
          if ((i = this.municipalities.findIndex((municipality: Municipality) => municipality.internalId === parseInt(f[param], 10))) !== -1) {
            userData['municipality'].push(this.municipalities[i]);
          }
        }
      }
    }

    if (!this.selectMunicipality) {
      userData['municipality'].push(this.currentUser.municipality);
    }

    if (this.roles.length === 1) {
      userData['roles'].push(this.roles[0]);
    }

    // TODO: Implement.
    // If we are creating an administrator, the municipality is ignored.
    /*if (userData['roles'].findIndex((role: Role) => this.globals.generalConfig.administratorRoles.includes(role.internalId)) !== -1) {
      userData['municipality'] = [];
    }*/

    // If we are not allowing the selection of an activation mode we forcefully set it
    if (!this.showActivationModes) {
      userData['isActive'] = true;
    }

    return userData;
  }

}
