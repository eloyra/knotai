import { InjectionToken, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, RouterModule, Routes } from '@angular/router';
import { ToDrupalComponent } from './core/components/to-drupal/to-drupal.component';
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';
import { RouteAuthGuard } from './core/authentication/route-auth.gard';

const externalUrlProvider = new InjectionToken('externalUrlRedirectResolver');

const routes: Routes = [
  {
    path: 'admin',
    children: [
      {
        path: 'users',
        loadChildren: './modules/user/user.module#UserModule',
        canLoad: [RouteAuthGuard],
        data: {
          roles: ['administator', 'municipality_admin', 'municipality_user'],
        },
      },
      {
        path: 'municipalities',
        loadChildren: './modules/municipality/municipality.module#MunicipalityModule',
        canLoad: [RouteAuthGuard],
        data: {
          roles: ['administator'],
        },
      }
    ]
  },
  {
    path: 'user',
    loadChildren: './modules/projects/projects.module#ProjectsModule',
    canLoad: [RouteAuthGuard],
    data: {
      roles: ['administator', 'municipality_admin', 'municipality_user'],
    },
  },
  {
    path: 'externalRedirect',
    resolve: {
      url: externalUrlProvider,
    },
    component: ToDrupalComponent
    // TODO: Añadir guardia aunque Drupal ya controle acceso?
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
  providers: [
    {
      provide: externalUrlProvider,
      useValue: (route: ActivatedRouteSnapshot) => {
        const externalUrl = route.paramMap.get('externalUrl');
        window.open(externalUrl, '_self');
      },
    },
  ],
})
export class AppRoutingModule {
}
