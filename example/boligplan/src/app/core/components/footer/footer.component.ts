import { Component, OnInit } from '@angular/core';
import { MenuLink } from '../../models/menu-link';
import { Menu } from '../../models/menu';
import { Datastore } from '../../services/datastore.service';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { generalConfig } from '../../config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  footerMenus: Menu[] = [];
  footerMenusLinks: { [id: string]: MenuLink[] } = {};

  constructor(private ds: Datastore) {}

  ngOnInit() {
    this.ds.findAll(Menu, {
      filter: {
        'name-filter': {
          condition: {
            path: 'internalId',
            operator: 'IN',
            value: generalConfig.footerMenus
          }
        }
      }
    }).subscribe(
      (menus: JsonApiQueryData<Menu>) => this.footerMenus = menus.getModels()
    );

    this.ds.findAll(MenuLink, {
      filter: {
        'name-filter': {
          condition: {
            path: 'menuInternalId',
            operator: 'IN',
            value: generalConfig.footerMenus
          }
        }
      }
    }).subscribe(
      (_links: JsonApiQueryData<MenuLink>) => {
        const links = _links.getModels();
        this.footerMenusLinks = {};
        links.forEach(link => {
          if (!this.footerMenusLinks[link.menuInternalId]) {
            this.footerMenusLinks[link.menuInternalId] = [];
          }
          this.footerMenusLinks[link.menuInternalId].push(link);
        });
      }
    );
  }

}
