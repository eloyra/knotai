import { Component, OnInit } from '@angular/core';
import { UserService } from '../../http/user.service';
import { User } from '../../models/user';
import { showHideNavLinks } from './header.animation';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from '../register/register.component';
import { Globals } from '../../../Globals';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    showHideNavLinks
  ]
})
export class HeaderComponent implements OnInit {

  currentUser: User;

  constructor(
    private userService: UserService,
    private modalService: NgbModal,
    public globals: Globals
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser()
      .then(user => this.currentUser = user)
      .catch(() => this.currentUser = null);
    this.userService.getCurrentUserChange.subscribe(user => this.currentUser = user);
  }

  openSignUp() {
    this.modalService.open(RegisterComponent, {centered: true});
  }

}
