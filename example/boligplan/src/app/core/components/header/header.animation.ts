import { animate, group, query as q, sequence, stagger, style, transition, trigger } from '@angular/animations';

const query = (selector, animation, options = {optional: true}) => q(selector, animation, options);

export const showHideNavLinks = trigger('showHideNavLinks', [
  transition('* <=> *', [
    query(':enter .nav-item', style({
      opacity: 0,
      right: -20
    })),
    sequence([
      group([
        query(':leave', animate('500ms ease-out', style({opacity: 0}))),
        query(':leave .nav-item', stagger(50, animate('400ms ease-in-out', style({
          opacity: 0,
          left: -20
        }))))
      ]),
      query(':enter', style({order: 1})),
      query(':enter .nav-item', stagger(-100, animate('400ms ease-in-out', style({
        opacity: 1,
        right: 0
      }))))
    ])
  ])
]);
