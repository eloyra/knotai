import { Component, OnInit } from '@angular/core';

import { UserService } from '../../http/user.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Alert } from '../../../shared/models/alert';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  signupForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]]
  });
  submitting = false;
  alerts: { [id: string]: Alert } = {};

  constructor(
    private regserv: UserService,
    private auth: AuthenticationService,
    private router: Router,
    private fb: FormBuilder,
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit() {}

  register(): void {
    const signupData = this.signupForm.value;
    this.submitting = true;
    this.regserv.registerWithoutPass(signupData.email, signupData.email)
      .then((res) => {
        this.activeModal.close();
        console.log(JSON.stringify(res, null, `\t`));
        this.router.navigate([`/login`]);
      })
      .catch((err) => {
        this.alerts['signup-failed'] = {
          type: 'danger',
          message: 'Unexpected error when signing up',
          dismissible: true
        };
        console.log(JSON.stringify(err, null, `\t`));
      })
      .then(() => {
        this.submitting = false;
      });
  }

}
