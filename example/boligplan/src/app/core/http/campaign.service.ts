import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';
import { Campaign } from '../models/campaign';
import { ErrorResponse, JsonApiQueryData } from 'angular2-jsonapi';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  constructor(private crud: CrudService) { }

  public getOrderedCampaigns(): Promise<Array<Campaign> | ErrorResponse> {
    return this.crud.getCollection(Campaign, ['status', 'municipality'], {}, {}, '-status.key,-updatedAt')
      .toPromise()
      .then((campaignModels: JsonApiQueryData<Campaign>) => {
        return campaignModels.getModels();
      })
      .catch((err: ErrorResponse) => {
        return err;
      });
  }

  public getOrderedCampaignsByMunicipality(municipalityId: number): Promise<Array<Campaign> | ErrorResponse> {
    return this.crud.getCollection(Campaign, ['status', 'municipality'], {}, {'municipality.internalId': municipalityId}, '-status.key,-updatedAt')
      .toPromise()
      .then((campaignModels: JsonApiQueryData<Campaign>) => {
        return campaignModels.getModels();
      })
      .catch((err: ErrorResponse) => {
        return err;
      });
  }

}
