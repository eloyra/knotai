import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JsonApiModule } from 'angular2-jsonapi';
import { Datastore } from './services/datastore.service';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { environment } from '../../environments/environment';
import { UserListComponent } from '../modules/user/pages/user-list/user-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ToDrupalComponent } from './components/to-drupal/to-drupal.component';
import { RegisterComponent } from './components/register/register.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    JsonApiModule,
    HttpClientModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [environment.jsonapi],
        sendAccessToken: true
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    UserListComponent,
    ToDrupalComponent
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    UserListComponent,
    PageNotFoundComponent,
    ToDrupalComponent,
    RegisterComponent
  ],
  providers: [
    Datastore,
    {
      provide: OAuthStorage,
      useValue: localStorage
    }
  ],
  entryComponents: [
    RegisterComponent
  ]
})
export class CoreModule {}
