export const generalConfig = {
  defaultTimezone: 'Europe/Berlin',
  administratorRoles: ['administrator'],
  municipalAdministratorRoles: ['municipality_admin'],
  userRoles: ['municipality_user'],
  accessRoles: ['administrator', 'municipality_admin', 'municipality_user'],
  manageableRoles: ['administrator', 'municipality_admin', 'municipality_user'],
  canGrant: {
    administrator: ['administrator', 'municipality_admin', 'municipality_user'],
    municipality_admin: ['municipality_user'],
  },
  goToDrupalRoles: ['administrator'],
  goToDrupalUrl: 'http://172.28.128.24/boligplan/backend/web/admin',
  allowedLogoTypes: ['image/jpeg', 'image/jpg'],
  noLogoPlaceholder: '/boligplan/backend/web/sites/default/files/no-logo-placeholder.png',
  logoutGotoUrl: '/',
  footerMenus: ['information', 'about', 'support']
};

export const rolesConfig = {
  administratorRoles: ['administrator'],
  municipalAdministratorRoles: ['municipality_admin'],
  userRoles: ['municipality_user'],
  accessRoles: ['administrator', 'municipality_admin', 'municipality_user'],
  manageableRoles: ['administrator', 'municipality_admin', 'municipality_user'],
  canGrant: {
    administrator: ['administrator', 'municipality_admin', 'municipality_user'],
    municipality_admin: ['municipality_user'],
  },
  goToDrupalRoles: ['administrator']
};

export const navigationConfig = {
  goToDrupalUrl: 'http://172.28.128.24/boligplan/backend/web/admin',
  logoutGotoUrl: '/'
};

export const imageConfig = {
  allowedLogoTypes: ['image/jpeg', 'image/jpg'],
  noLogoPlaceholder: '/boligplan/backend/web/sites/default/files/no-logo-placeholder.png'
};

export const projectConfig = {
  lastAction: {
    done: 'Henlet',
    draft: 'Gemt',
  },
};
