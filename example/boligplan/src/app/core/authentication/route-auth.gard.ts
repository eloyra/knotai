import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { UserService } from '../http/user.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})

export class RouteAuthGuard implements CanLoad, CanActivate {

  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private users: UserService,
  ) {}

  canLoad(route: Route): Promise<boolean> {
    const roles = route.data['roles'] as Array<string>;
    const isSessionActive = localStorage.getItem('access_token');
    return this.users.getCurrentUser(!isSessionActive)
      .then((currentUser: User) => {
        if (!currentUser) {
          this.auth.logout();
          throw new Error('Not authorized!');
        }
        else if (this.users.isAdmin(currentUser) || this.users.hasRole(currentUser, roles)) { return true; }
        else {
          throw new Error('Not authorized!');
        }
      })
      .catch(() => {
        console.error('Not authorized!');
        this.router.navigate(['/']).catch(err => console.error(err));
        return false;
      });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    return this.canLoad(route.routeConfig);
  }

}
