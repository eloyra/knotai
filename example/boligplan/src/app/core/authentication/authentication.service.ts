import { Injectable } from '@angular/core';
import { NullValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { Globals } from '../../Globals';

@Injectable({
  providedIn: 'root'
})

/**
 * This class handles the application's backend authentication.
 */
export class AuthenticationService {

  /**
   * Class constructor.
   *
   * @param authService
   *   The library that handles the authentication process.
   * @param globals
   *   Component with global constants and variables.
   */
  constructor(private authService: OAuthService, private globals: Globals) {
    this.configAuth();
  }

  /**
   * Makes a token request.
   *
   * @param user
   *   Username of the user that wants access.
   * @param pass
   *   Password of the user that wants access.
   * @param scope
   *   A list of the user's roles that we want to grant him during the life cycle of the token. The user must have this roles on backend
   *   or they are simply ignored.
   *   This parameter is necessary because OAuth2 defaults to the "authenticated" role + the roles granted to this application on backend.
   *   It does NOT grant user-list accessRoles their roles by default.
   */
  public authenticate(user: string, pass: string, scope: Array<string> = this.globals.generalConfig.accessRoles): Promise<object> {
    this.authService.scope = scope.join(' ');
    return this.authService.fetchTokenUsingPasswordFlow(user, pass);
  }

  /**
   * Clears accessRoles access data and returns whether the logout has been succesfull.
   */
  logout(): boolean {
    this.authService.logOut();
    localStorage.removeItem('current_user');
    return !this.hasToken();
  }

  /**
   * Checks whether the user has a valid access token. False when the token has expired.
   */
  private hasToken(): boolean {
    return this.authService.hasValidAccessToken();
  }

  /**
   * Returns the user's access token.
   */
  public getToken(): string {
    return this.authService.getAccessToken();
  }

  /**
   * Tries to refresh the access token.
   */
  private refreshToken(): Promise<object> {
    this.authService.scope = '';
    return this.authService.refreshToken();
  }

  /**
   * Checks if user is authenticated or can refresh his token.
   */
  public isAuthenticated(): Promise<object> {
    return new Promise((resolve, reject) => {
      this.hasToken() ? resolve() : this.refreshToken().then((res) => resolve(res)).catch((err) => reject(err));
      /*if (this.hasToken()) {
        resolve();
      } else {
        this.refreshToken()
          .then(() => {
            resolve();
          })
          .catch(() => {
            reject();
          });
      }*/
    });
  }

  // Basic authentication configuration common to all app users.
  private configAuth(): void {
    // Login endpoint - where the token requests are sent.
    this.authService.tokenEndpoint = this.globals.auth.token;

    // This app's ID on backend.
    this.authService.clientId = this.globals.auth.client_id;

    // This app's password on backend.
    this.authService.dummyClientSecret = this.globals.auth.client_secret;

    // If default, it allowes http only for local use. // TODO: Actualizar para producción.
    this.authService.requireHttps = false;

    // Esto no sé qué hace xD
    this.authService.tokenValidationHandler = new NullValidationHandler();

    // Ruta a la que navega por defecto al desconectarse un usuario
    this.authService.logoutUrl = this.globals.generalConfig.logoutGotoUrl;
  }

}
