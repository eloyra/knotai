import { Injectable } from '@angular/core';
import { ErrorResponse } from 'angular2-jsonapi';
import { AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormErrorHandlerService {

  private DRUPAL_ERROR_PARAM_SEPARATOR = ':';

  constructor() { }

  public processDrupalErrors(errorObject: {[key: string]: string[]}, errorResponse: ErrorResponse): object {
    const errors = errorResponse ? errorResponse.errors : null;

    if (errors) {
      for (const error of errors) {
        const separatorIndex = error.detail.indexOf(this.DRUPAL_ERROR_PARAM_SEPARATOR);
        const paramName = error.detail.substr(0, separatorIndex).trim();
        const errorMessage = error.detail.substr(separatorIndex + 1).trim();
        if (!errorObject[paramName]) { errorObject[paramName] = []; }
        errorObject[paramName].push(errorMessage);
      }
    }

    return errorObject;
  }

  public handleValidationErrors(errorObject: {[key: string]: string[]}, validationErrors: {[key: string]: string}): object {
    if (validationErrors) {
      for (const field of Object.keys(validationErrors)) {
        if (!errorObject[field]) { errorObject[field] = []; }
        errorObject[field].push(validationErrors[field]);
      }
    }
    return errorObject;
  }

  public validatorsErrorMessages(error: string, formParam: string, options: object): string {
    switch (error) {
      case 'required':
        return `The ${formParam} can't be empty.`;
      case 'maxlength':
        return `The ${formParam} is too long. ${options['max_length'] ? `It must have ${options['max_length']} characters or less.` : ''}`;
    }
  }

  public handleControlErrors(errorObject: {[key: string]: string[]}, controls: {[p: string]: AbstractControl}): {[key: string]: string[]} {
    if (controls) {
      for (const control of Object.keys(controls)) {
        if (controls[control].invalid) {
          const errors = controls[control].errors;
          for (const error of Object.keys(errors)) {
            if (!errorObject[errors[error]['field']]) { errorObject[errors[error]['field']] = []; }
            errorObject[errors[error]['field']].push(errors[error]['value']);
          }
        }
      }
    }
    return errorObject;
  }
}
