import { Attribute, BelongsTo, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';
import { User } from './user';

@JsonApiModelConfig({
  type: 'files'
})
export class File extends JsonApiModel {

  @Attribute({serializedName: 'id'})
  uuid: string;

  @Attribute()
  internalId: number;

  @Attribute()
  langcode: string;

  @Attribute()
  filename: string;

  @Attribute()
  uri: object;

  @Attribute()
  mimetype: string;

  @Attribute()
  size: number;

  @Attribute()
  isPublished: boolean;

  @Attribute()
  createdAt: Date;

  @Attribute()
  updatedAt: Date;

  @Attribute()
  url: string;

  @BelongsTo()
  owner: User;

}
