import { Attribute, BelongsTo, HasMany, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';
import { Role } from './role';
import { Municipality } from './municipality';

@JsonApiModelConfig({
  type: 'users'
})
export class User extends JsonApiModel {

  @Attribute({ serializedName: 'id' })
  uuid: string;

  @Attribute()
  internalId: number;

  @Attribute()
  name: string;

  @Attribute()
  mail: string;

  @Attribute({ serializedName: 'first_name' })
  firstName: string;

  @Attribute({ serializedName: 'last_name' })
  lastName: string;

  @Attribute()
  timezone: string;

  @Attribute()
  isActive: boolean;

  @Attribute()
  createdAt: Date;

  @Attribute()
  updatedAt: Date;

  @Attribute()
  access: number;

  @Attribute()
  lastLogin: Date;

  @HasMany()
  roles: Role[];

  @BelongsTo()
  municipality: Municipality | null;

  @Attribute()
  pass: object;

  memberfor: string;

  lastaccess: string;

  municipality_placeholder: string;

}
