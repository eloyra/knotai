import { Attribute, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'roles'
})
export class Role extends JsonApiModel {

  @Attribute({serializedName: 'id'})
  uuid: string;

  @Attribute()
  status: boolean;

  @Attribute()
  dependencies: string[];

  @Attribute()
  internalId: string;

  @Attribute()
  label: string;

  @Attribute()
  weight: number;

  @Attribute()
  is_admin: boolean;

  @Attribute()
  permissions: string[];

  disabled: boolean;

}
