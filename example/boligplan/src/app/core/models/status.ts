import { Attribute, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'status'
})
export class Status extends JsonApiModel {

  @Attribute({serializedName: 'id'})
  uuid: string;

  @Attribute()
  internalId: number;

  @Attribute()
  langcode: string;

  @Attribute()
  name: string;

  @Attribute()
  description: string;

  @Attribute()
  size: number;

  @Attribute()
  isPublished: boolean;

  @Attribute()
  updatedAt: Date;

  @Attribute()
  key: string;

}
