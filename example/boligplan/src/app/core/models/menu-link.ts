import { Attribute, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'menuLinks'
})
export class MenuLink extends JsonApiModel {

  @Attribute()
  internalId: number;

  @Attribute()
  isEnabled: boolean;

  @Attribute()
  title: string;

  @Attribute()
  description: string;

  @Attribute()
  menuInternalId: string;

  @Attribute()
  link: string;

  @Attribute()
  isExpanded: boolean;

}
