import { Attribute, BelongsTo, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';
import { File } from './file';

@JsonApiModelConfig({
  type: 'municipalities'
})
// TODO: Completar
export class Municipality extends JsonApiModel {

  @Attribute()
  internalId: number;

  @Attribute({serializedName: 'id'})
  uuid: string;

  @Attribute()
  code: string;

  @Attribute()
  active: boolean;

  @Attribute()
  langcode: string;

  @Attribute()
  isActive: boolean;

  @Attribute()
  name: string;

  @Attribute()
  updatedAt: Date;

  @Attribute()
  description: object;

  @Attribute()
  path: object;

  @Attribute()
  contact: object;

  @BelongsTo()
  image: File;
}

/*class Description {
  value: string;
  format: string;
  processed: string;
}

class Path {
  alias: string;
  pid: number;
  langcode: string;
}

class Contact {
  value: string;
  format: string;
  processed: string;
}*/
