import { Attribute, BelongsTo, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';
import { User } from './user';
import { Status } from './status';
import { Municipality } from './municipality';

@JsonApiModelConfig({
  type: 'campaigns'
})
export class Campaign extends JsonApiModel {

  @Attribute({serializedName: 'id'})
  uuid: string;

  @Attribute()
  internalId: number;

  @BelongsTo()
  createdBy: User;

  @Attribute()
  updatedAt: Date;

  @Attribute()
  title: string;

  @Attribute()
  createdAt: Date;

  @BelongsTo()
  status: Status;

  @Attribute()
  selected: number;

  @Attribute()
  visited: number;

  @BelongsTo()
  municipality: Municipality;

}
