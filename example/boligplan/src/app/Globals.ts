import { Injectable } from '@angular/core';
import { User } from './core/models/user';
import { generalConfig, imageConfig, navigationConfig, projectConfig, rolesConfig } from './core/config';
import { environment, host, oauth_endpoint } from '../environments/environment';

@Injectable()
export class Globals {

  // Though global, this should be retrieved through src/app/core/http/user.service.ts::getCurrentUser()
  public currentUser: User = null;

  public generalConfig = generalConfig;
  public config = {
    general: generalConfig,
    roles: rolesConfig,
    navigation: navigationConfig,
    image: imageConfig,
    project: projectConfig,
  };
  public env = environment;
  public auth = oauth_endpoint;
  public host = host;

  static formatTime(time: number): string {
    let seconds: number = time / 1000;
    let minutes: number = Math.floor(seconds / 60);
    seconds = Math.floor(seconds - minutes * 60);
    let hours = Math.floor(minutes / 60);
    minutes -= hours * 60;
    let days = Math.floor(hours / 24);
    hours -= days * 24;
    let weeks = Math.floor(days / 7);
    days -= weeks * 7;
    let months = Math.floor(weeks / 4);
    weeks -= months * 4;
    const years = Math.floor(months / 12);
    months -= years * 12;

    let timeString = '';
    if (years === 1) { timeString += `${years} year `; } else if (years > 1) { timeString += `${years} years `; }
    if (months === 1) { timeString += `${months} month `; } else if (months > 1) { timeString += `${months} months `; }
    if (weeks === 1) { timeString += `${weeks} week `; } else if (weeks > 1) { timeString += `${weeks} weeks `; }
    if (days === 1) { timeString += `${days} day `; } else if (days > 1) { timeString += `${days} days `; }
    if (hours === 1) { timeString += `${hours} hour `; } else if (hours > 1) { timeString += `${hours} hours `; }
    if (minutes === 1) { timeString += `${minutes} minute `; } else if (minutes > 1) { timeString += `${minutes} minutes `; }
    if (seconds === 1) { timeString += `${seconds} second`; } else if (seconds > 1) { timeString += `${seconds} seconds`; }

    return timeString;
  }

  /*@Pipe({
    name: boligDate,
  })*/


}
