// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const host = 'http://172.28.128.24';
const backend = 'http://172.28.128.24/boligplan/backend/web';
export const environment = {
  production: false,
  backend: backend,
  jsonapi: `${backend}/api`,
  register: `${backend}/user/register?_format=json`,
  login: `${backend}/user/login`,
  logout: `${backend}/user/logout`,
  logStatus: `${backend}/user/login_status`,
  fileUpload: `${backend}/file/upload`,
  boligplanUserEdit: `${backend}/boligplan_api/user`
};

const oauth = `${backend}/oauth`;
export const oauth_endpoint = {
  oauth: oauth,
  token: `${oauth}/token`,
  token_info: `${oauth}/debug?_format=json`,
  client_id: 'e4f30b40-f1be-4155-8062-fd859564343b',
  // client_id: '0d77b51e-4cd8-4461-b029-958d3dc6d2c5',
  client_secret: 'abc123',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
