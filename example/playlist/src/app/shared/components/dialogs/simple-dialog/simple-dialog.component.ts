import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { TranslatableString } from '../../../models/translatable-string/translatable-string';

export const SIMPLE_DIALOG_CONFIG: MatDialogConfig = {
  panelClass: 'simple-dialog',
  autoFocus: true
};

export interface SimpleDialogContent {
  title: TranslatableString;
  text?: TranslatableString;
  primaryAction: TranslatableString;
  secondaryAction?: TranslatableString;
  icon?: string;
}

@Component({
  selector: 'app-simple-dialog',
  templateUrl: './simple-dialog.component.html',
  styleUrls: ['./simple-dialog.component.scss']
})
export class SimpleDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SimpleDialogContent
  ) {}

  ngOnInit() {}

}
