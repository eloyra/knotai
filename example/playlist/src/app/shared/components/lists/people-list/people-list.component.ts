import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../../backend/models/user';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {

  public static readonly pageSize = 20;

  @Input() users: User[] = [];
  @Input() isLastPage = true;
  @Input() loading = false;

  @Output() nextPage = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  public onNextPage() {
    this.nextPage.emit();
  }

}
