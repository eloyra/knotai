import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Comment, IComment } from '../../../../backend/models/comment';
import { finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { EPortfolioService } from '../../../../e-portfolio/services/e-portfolio/e-portfolio.service';
import { TranslatableString } from '../../../models/translatable-string/translatable-string';
import { DialogService } from '../../../services/dialog/dialog.service';
import { ToastService } from '../../../services/toast/toast.service';
import { CommentTheme } from '../../comment/comment.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.scss']
})
export class CommentsListComponent implements OnInit {

  public static readonly pageSize = 5;

  @Input() itemId: string;
  @Input() theme: CommentTheme = 'light';
  @Input() autofocus: boolean;

  @Output() commentsChange = new EventEmitter<number>();

  public comments: Comment[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;
  public adding = false;

  private totalItems: number;

  constructor(
    private portfolioService: EPortfolioService,
    private dialog: DialogService,
    private toast: ToastService
  ) {}

  ngOnInit() {
    this.getComments();
  }

  public getComments(lastOnly?: boolean, reload?: boolean) {
    this.loading = true;
    const _page = lastOnly ? this.comments.length + 1 : ++this.page;
    const _size = lastOnly ? 1 : CommentsListComponent.pageSize;
    this.portfolioService.getItemComments(this.itemId, _page, _size)
      .pipe(finalize(() => this.loading = false))
      .subscribe(comments => {
        if (reload) {
          this.comments = comments;
        } else {
          this.comments.push(...comments);
        }
        this.totalItems = (comments as RestangularCollection<Comment>).totalItems;
        this.isLastPage = this.comments.length >= this.totalItems;
        this.commentsChange.emit(this.totalItems);
      });
  }

  public onRemove(index: number) {
    this.comments.splice(index, 1);
    this.getComments(true);
  }

  public onAdd(comment: string) {
    this.adding = true;
    const _comment: IComment = {
      comment: comment,
      idItem: this.itemId
    };
    this.portfolioService.addComment(_comment)
      .pipe(finalize(() => this.adding = false))
      .subscribe((addedComment: Comment) => {
        this.comments.unshift(addedComment);
        this.comments.pop();
        this.reload();
        this.toast.open(new TranslatableString('toasts.addedComment'));
      }, (error: HttpErrorResponse) => {
        this.toast.showError(error.status);
      });
  }

  private reload() {
    this.page = 0;
    this.getComments(false, true);
  }

}
