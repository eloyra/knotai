import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { NgxMasonryOptions } from 'ngx-masonry';
import { Subject } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit {

  public static readonly pageSize = 8;

  @Input() items: Item[] = [];
  @Input() isLastPage = true;
  @Input() loading = false;
  @Input() showAdd = false;

  @Output() add = new EventEmitter();
  @Output() remove = new EventEmitter();
  @Output() unfavourite = new EventEmitter<number>();
  @Output() unmark = new EventEmitter<number>();
  @Output() nextPage = new EventEmitter();

  public options: NgxMasonryOptions = {
    fitWidth: false,
    horizontalOrder: false,
    transitionDuration: '0.4s',
    resize: true
  };
  public waitForImages = false;
  public updateMasonryLayout: object = {};

  private updateLayoutSubject = new Subject<string>();

  constructor() {}

  ngOnInit() {
    this.updateLayoutSubject.asObservable().pipe(distinctUntilChanged()).subscribe(() => {
      delete this.updateMasonryLayout;
      this.updateMasonryLayout = {};
    });
  }

  /**
   * Triggers Masonry's updateLayout method.
   *
   * The value passed to the component (aka this.updateMasonryLayout) MUST be an object, and Masonry will only react
   * to its creation, thus to trigger the layout update we need to destroy it and assign a new one.
   */
  public refresh(item?: string) {
    this.updateLayoutSubject.next(item || '');
  }

  public onAdd() {
    this.add.emit();
  }

  public onNextPage() {
    this.nextPage.emit();
  }

  public onRemove(index: number) {
    this.items.splice(index, 1);
    this.remove.emit();
  }

  public onUnfavourite(index: number) {
    this.unfavourite.emit(index);
  }

  public onUnmark(index: number) {
    this.unmark.emit(index);
  }

}
