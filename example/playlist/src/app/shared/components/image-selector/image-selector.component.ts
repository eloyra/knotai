import { Component, forwardRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgxCropperjsComponent } from 'ngx-cropperjs';
import { Subject } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { ToastService } from '../../services/toast/toast.service';
import { TranslatableString } from '../../models/translatable-string/translatable-string';

@Component({
  selector: 'app-image-selector',
  templateUrl: './image-selector.component.html',
  styleUrls: ['./image-selector.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImageSelectorComponent),
      multi: true
    }
  ]
})
export class ImageSelectorComponent implements OnInit, ControlValueAccessor {

  public static readonly cropperOptions = {
    viewMode: 1,
    dragMode: 'move',
    aspectRatio: 1,
    preview: false,
    responsive: true,
    guides: false,
    center: false,
    background: false,
    movable: true,
    rotatable: false,
    scalable: true,
    zoomable: true,
    zoomOnTouch: true,
    zoomOnWheel: true,
    cropBoxMovable: false,
    cropBoxResizable: false,
    toggleDragModeOnDblclick: false
  };

  @ViewChild('cropper', {static: false}) cropper: NgxCropperjsComponent;

  @Input() aspectRatio: number = 1;
  @Input() crop = true;
  @Input() type: 'blob' | 'base64' = 'blob';
  @Input() maxSize: number = 3 * 1024 * 1024; // bytes
  @Input() minWidth: number = 1000; // pixels
  @Input() formats: string[] = ['jpg', 'jpeg', 'png', 'gif'];

  public cropperImage: string;
  public image: { base64: string, blob: File | Blob } = {
    base64: null,
    blob: null
  };
  public isHover = false;
  public options: {};
  public zoom = 0;

  private cropSubject = new Subject();

  constructor(
    private toast: ToastService
  ) {}

  ngOnInit() {
    this.cropSubject.pipe(
      debounceTime(500),
      filter(() => !!this.cropper)
    ).subscribe(() => {
      const canvas: HTMLCanvasElement = this.cropper.cropper.getCroppedCanvas();
      this.image.base64 = canvas.toDataURL();
      canvas.toBlob(blob => {
        this.image.blob = blob;
        this.propagateChange(this.image[this.type]);
      });
    });
    this.options = {
      ...ImageSelectorComponent.cropperOptions,
      aspectRatio: this.aspectRatio,
      crop: () => this.cropSubject.next(),
      zoom: (e) => this.zoom = e.detail.ratio
    };
  }

  public ready() {
    const cropperData = this.cropper.cropper.getImageData();
    this.zoom = cropperData.width / cropperData.naturalWidth;
  }

  public changeZoom(zoomLevel: number) {
    const containerData = this.cropper.cropper.getContainerData();
    this.cropper.cropper.zoomTo(zoomLevel, {
      x: containerData.width / 2,
      y: containerData.height / 2,
    });
  }

  public writeValue(value: Blob) {
    if (value !== undefined) {
      this.getImageData(value);
    }
  }

  public registerOnChange(fn) {
    this.propagateChange = fn;
  }

  public registerOnTouched() {}

  public onDrop(e: DragEvent) {
    e.preventDefault();
    e.stopPropagation();
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      this.getImageData(e.dataTransfer.files[0]);
    }
  }

  public onDragOverChange(e: DragEvent) {
    e.preventDefault();
    e.stopPropagation();
    this.isHover = !this.isHover;
  }

  public readFile(e: Event) {
    const target = e.target as HTMLInputElement;
    if (target.files && target.files[0]) {
      this.getImageData(target.files[0]);
    }
  }

  private propagateChange = (_: any) => {};

  private getImageData(file: File | Blob) {
    if (file) {
      if (file.size > this.maxSize) {
        this.toast.open(
          new TranslatableString('imageSelector.maxSize', {mb: this.maxSize / 1024 / 1024}),
          'warn'
        );
        return;
      }
      // Code in "includes()" obtains the file extension.
      // https://stackoverflow.com/questions/190852/how-can-i-get-file-extensions-with-javascript/12900504#12900504
      if (file instanceof File && !this.formats.includes(file.name.slice((file.name.lastIndexOf('.') - 1 >>> 0) + 2))) {
        this.toast.open(new TranslatableString(this.formats.join(', ').toUpperCase()), 'warn');
        return;
      }
      const reader = new FileReader();
      reader.onload = () => {
        const _img = new Image();
        _img.onload = () => {
          if (_img.width < this.minWidth) {
            this.toast.open(
              new TranslatableString('imageSelector.minWidth', {px: this.minWidth}),
              'warn'
            );
            return;
          }
          this.cropperImage = reader.result.toString();
          this.image.blob = file;
          this.image.base64 = this.cropperImage;
          this.propagateChange(this.image[this.type]);
        };
        _img.src = reader.result.toString();
      };
      reader.readAsDataURL(file);
    }
  }

}
