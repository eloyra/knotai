import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { MenuItem } from '../../buttons/menu-button/menu-button.component';
import { Router } from '@angular/router';
import { CurrentUserFactory } from '../../../../backend/auth/current-user';
import { TranslatableString } from '../../../models/translatable-string/translatable-string';
import { filter, finalize, switchMap, tap } from 'rxjs/operators';
import { DialogService } from '../../../services/dialog/dialog.service';
import { EPortfolioService } from '../../../../e-portfolio/services/e-portfolio/e-portfolio.service';
import { ToastService } from '../../../services/toast/toast.service';
import {
  ItemsAddComponent,
  ItemsAddData
} from '../../../../e-portfolio/components/items/items-add/items-add.component';
import { SidenavService } from '../../../services/sidenav/sidenav.service';
import { Role } from '../../../../backend/services/config/BackendConstants';
import {
  ItemOpinionsAction,
  ItemOpinionsComponent,
  ItemOpinionsData
} from '../../../../e-portfolio/components/items/item-opinions/item-opinions.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss']
})
export class CardItemComponent implements OnInit {

  @Input() item: Item;
  @Output() remove = new EventEmitter();
  @Output() unfavourite = new EventEmitter();
  @Output() unmark = new EventEmitter();

  public actions: MenuItem<Item>[];
  public showComments: boolean;
  public isOwnItem: boolean;
  public isAdmin: boolean;

  constructor(
    private router: Router,
    private currentUser: CurrentUserFactory,
    private dialog: DialogService,
    private portfolioService: EPortfolioService,
    private toast: ToastService,
    private sidenav: SidenavService
  ) {}

  ngOnInit() {
    this.isOwnItem = this.item.author && this.item.author.id === this.currentUser.id;
    this.isAdmin = this.currentUser.isRole(Role.ADMIN);
    this.createMenu();
  }

  public openItem() {
    const url = this.item.link || this.router.createUrlTree(['/item', this.item.id]).toString();
    window.open(url, '_blank');
  }

  public onFavouriteChange(favouriteId: string) {
    if (!favouriteId) {
      this.unfavourite.emit();
    }
  }

  public toggleComments() {
    this.showComments = !this.showComments;
  }

  public onCommentsChange(change: number) {
    this.item.numberComments = change;
  }

  private createMenu() {
    this.actions = [];
    if ((this.isAdmin && this.item.inappropriate) || (this.isOwnItem && this.item.inappropriate && !this.item.inappropriate.authorReviewed)) {
      this.actions.push({
        label: new TranslatableString('item.actions.opinions'),
        icon: 'eye',
        color: 'primary',
        action: () => this.viewOpinions()
      });
    }
    if (this.isOwnItem) {
      this.actions.push({
        label: new TranslatableString('edit'),
        icon: 'pencil',
        color: 'primary',
        action: () => this.editItem()
      });
    }
    if (!this.isOwnItem) {
      this.actions.push({
        label: new TranslatableString(`item.actions.${this.item.inappropriate ? 'marked' : 'mark'}`),
        icon: 'flag',
        color: 'primary',
        disabled: !!this.item.inappropriate,
        action: () => this.markInappropriate()
      });
    }
    if (this.isOwnItem || this.isAdmin) {
      this.actions.push({
        label: new TranslatableString('delete'),
        icon: 'cross',
        action: () => this.deleteItem()
      });
    }
  }

  private editItem() {
    this.dialog.openLoader();
    this.portfolioService.getItemById(this.item.id).pipe(
      finalize(() => this.dialog.closeLoader()),
      switchMap(item => this.sidenav.open(ItemsAddComponent, <ItemsAddData>{
        initialPlaylist: item.playlist,
        currentItem: item
      })),
      filter((modifiedItem: Item) => !!modifiedItem)
    ).subscribe((modifiedItem: Item) => {
      this.toast.open(new TranslatableString('toasts.savedOk'));
      Object.assign(this.item, modifiedItem);
    }, (error: HttpErrorResponse) => {
      this.toast.showError(error.status);
    });
  }

  private deleteItem() {
    this.dialog.simple({
      title: new TranslatableString('item.delete.title'),
      text: new TranslatableString('item.delete.text'),
      primaryAction: new TranslatableString('delete'),
      secondaryAction: new TranslatableString('cancel'),
      icon: 'trash'
    }).afterClosed().pipe(
      filter((_delete: boolean) => _delete),
      tap(() => this.dialog.openLoader()),
      switchMap(() => this.portfolioService.removeItem(this.item)),
      finalize(() => this.dialog.closeLoader())
    ).subscribe(() => {
      this.remove.emit();
      this.toast.open(new TranslatableString('toasts.deletedItem'), 'success', new TranslatableString('close'));
    }, (error: HttpErrorResponse) => {
      this.toast.showError(error.status);
    });
  }

  private markInappropriate() {
    this.dialog.markAsInappropriate().pipe(
      filter((comment: string) => !!comment),
      tap(() => this.dialog.openLoader()),
      switchMap((comment: string) => this.portfolioService.markItemInappropriate(this.item, comment)),
      finalize(() => this.dialog.closeLoader())
    ).subscribe(inappropriateEntity => {
      this.toast.open(new TranslatableString('toasts.markInappropriate'));
      this.item.inappropriate = {id: inappropriateEntity.id};
      this.createMenu();
    }, (error: HttpErrorResponse) => {
      this.toast.showError(error.status);
    });
  }

  private viewOpinions() {
    this.sidenav.open(ItemOpinionsComponent, <ItemOpinionsData>{item: this.item})
      .subscribe((action: ItemOpinionsAction) => {
        switch (action) {
          case ItemOpinionsAction.UNMARKED:
            if (this.isAdmin) {
              this.item.inappropriate = null;
            } else {
              this.item.inappropriate.authorReviewed = true;
            }
            this.createMenu();
            this.unmark.emit();
            break;
          case ItemOpinionsAction.DELETED:
            this.remove.emit();
            this.toast.open(new TranslatableString('toasts.deletedItem'), 'success', new TranslatableString('close'));
            break;
        }
      });
  }

}
