import { Component, Input, OnInit } from '@angular/core';
import { Playlist } from '../../../../backend/models/playlist';

@Component({
  selector: 'app-card-playlist',
  templateUrl: './card-playlist.component.html',
  styleUrls: ['./card-playlist.component.scss']
})
export class CardPlaylistComponent implements OnInit {

  @Input() playlist: Playlist;
  @Input() openPlaylist = true;
  @Input() alwaysSmall: boolean;
  @Input() elements: 'items' | 'subs' = 'subs';

  constructor() {}

  ngOnInit() {}

}
