import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsIconsComponent } from './tabs-icons.component';

describe('TabsIconsComponent', () => {
  let component: TabsIconsComponent;
  let fixture: ComponentFixture<TabsIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
