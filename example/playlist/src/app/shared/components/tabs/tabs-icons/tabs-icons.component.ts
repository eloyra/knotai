import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ISimpleTabs } from '../../../models/simple-tabs/isimple-tabs';
import { TranslatableString } from '../../../models/translatable-string/translatable-string';

export interface TabIcon extends ISimpleTabs {
  path: string;
  icon: string;
  amount?: number;
  label: TranslatableString;
  newContent?: boolean;
}

@Component({
  selector: 'app-tabs-icons',
  templateUrl: './tabs-icons.component.html',
  styleUrls: ['./tabs-icons.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabsIconsComponent implements OnInit {

  @Input() navLinks: TabIcon[] = [];
  @Input() disabled: boolean;

  constructor() {}

  ngOnInit() {}

  public clickGuard(event: MouseEvent) {
    if (this.disabled) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

}
