import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsMainNavComponent } from './tabs-main-nav.component';

describe('TabsMainNavComponent', () => {
  let component: TabsMainNavComponent;
  let fixture: ComponentFixture<TabsMainNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsMainNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsMainNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
