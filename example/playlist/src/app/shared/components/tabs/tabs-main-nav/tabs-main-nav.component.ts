import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';

export interface MainNavTab {
  path: string;
  label: string;
  amount: number;
  newContent?: boolean;
  hide?: boolean;
}

@Component({
  selector: 'app-tabs-main-nav',
  templateUrl: './tabs-main-nav.component.html',
  styleUrls: ['./tabs-main-nav.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabsMainNavComponent implements OnInit, AfterViewInit {

  @Input() navLinks: MainNavTab[] = [];

  @ViewChild('tabs', {read: ElementRef, static: false}) tabsRef: ElementRef;

  private isDown = false;
  private startX: number;
  private scrollLeft: number;
  private sliderContainer: HTMLElement;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(filter((event: RouterEvent) => event instanceof NavigationEnd))
      .subscribe(() => {
        for (const tab of this.navLinks) {
          if (this.router.isActive(tab.path, true)) {
            tab.newContent = false;
            break;
          }
        }
      });
  }

  ngAfterViewInit() {
    this.sliderContainer = this.tabsRef.nativeElement as HTMLElement;
  }

  public onMouseDown(event: MouseEvent) {
    this.isDown = true;
    this.startX = event.pageX - this.sliderContainer.offsetLeft;
    this.scrollLeft = this.sliderContainer.scrollLeft;
  }

  public onMouseUp(event: MouseEvent) {
    this.isDown = false;
  }

  public onMouseMove(event: MouseEvent) {
    if (!this.isDown) {
      return;
    }
    event.preventDefault();
    const x = event.pageX - this.sliderContainer.offsetLeft;
    const walk = (x - this.startX) * 2;
    this.sliderContainer.scrollLeft = this.scrollLeft - walk;
  }

}
