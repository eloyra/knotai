import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { TranslatableString } from '../../../models/translatable-string/translatable-string';
import { filter } from 'rxjs/operators';

export interface NavLink {
  path: string;
  label: TranslatableString;
  amount: number;
}

@Component({
  selector: 'app-tabs-buttons',
  templateUrl: './tabs-buttons.component.html',
  styleUrls: ['./tabs-buttons.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabsButtonsComponent implements OnInit {

  @Input() navLinks: NavLink[] = [];

  public selected: NavLink = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.shareRoute();
    this.router.events
      .pipe(filter((event: RouterEvent) => event instanceof NavigationEnd))
      .subscribe(() => this.shareRoute());
  }

  shareRoute() {
    this.route.children[0].url.subscribe((data) => {
      this.selected = this.navLinks.find(link => link.path === data[0].path) || this.navLinks[0];
    });
  }

}
