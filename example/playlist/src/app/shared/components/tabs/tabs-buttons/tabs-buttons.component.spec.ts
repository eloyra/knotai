import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsButtonsComponent } from './tabs-buttons.component';

describe('TabsButtonsComponent', () => {
  let component: TabsButtonsComponent;
  let fixture: ComponentFixture<TabsButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
