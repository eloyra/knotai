import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { finalize, tap } from 'rxjs/operators';
import { MatTooltip } from '@angular/material';
import { EPortfolioService } from '../../../../e-portfolio/services/e-portfolio/e-portfolio.service';

@Component({
  selector: 'app-fav-button',
  templateUrl: './fav-button.component.html',
  styleUrls: ['./fav-button.component.scss']
})
export class FavButtonComponent implements OnInit {

  @ViewChild('tooltip', {static: true}) tooltip: MatTooltip;

  @Input() item: Item;
  @Output() change = new EventEmitter<string | null>();

  public marking = false;
  public unmarking = false;

  constructor(
    private portfolioService: EPortfolioService
  ) {}

  ngOnInit() {}

  public toggleFavourite() {
    if (this.item.idFavourite) {
      this.unmarking = true;
      this.portfolioService.unmarkFavourite(this.item)
        .pipe(finalize(() => this.unmarking = false))
        .subscribe(() => {
          this.change.emit(null);
          this.item.idFavourite = null;
        });
    } else {
      this.marking = true;
      this.portfolioService.markFavourite(this.item)
        .pipe(finalize(() => this.marking = false))
        .subscribe(favourite => {
          this.item.idFavourite = favourite.id;
          this.change.emit(favourite.id);
          this.tooltip.disabled = false;
          setTimeout(() => this.tooltip.show(250), 0);
          setTimeout(() => this.tooltip.disabled = true, 2000);
        });
    }
  }

}
