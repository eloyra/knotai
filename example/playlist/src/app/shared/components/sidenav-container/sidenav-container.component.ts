import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { SidenavService } from '../../services/sidenav/sidenav.service';

export declare type SidenavType = 'default' | 'small' | 'expanded';

export interface SidenavParams {
  level: number;
  type?: SidenavType;
  mode?: MatSidenav['mode'];
}

const SIDENAV_DEFAULT_MODE: MatSidenav['mode'] = 'over';
const SIDENAV_DEFAULT_TYPE: SidenavType = 'default';

@Component({
  selector: 'app-sidenav-container',
  templateUrl: './sidenav-container.component.html',
  styleUrls: ['./sidenav-container.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidenavContainerComponent implements OnInit {

  @ViewChild('sidenav', {static: true}) sidenavRef: MatSidenav;

  public _sidenavLevel: number;
  public sidenavMode: MatSidenav['mode'] = SIDENAV_DEFAULT_MODE;
  public isExpanded: boolean;
  public sidenavType: SidenavType = SIDENAV_DEFAULT_TYPE;

  private originalMode: MatSidenav['mode'] = SIDENAV_DEFAULT_MODE;
  private readonly bodyClass = 'sidenav-open';

  constructor(
    private sidenavService: SidenavService
  ) {
    this._sidenavLevel = this.sidenavService.sidenavs.length;
  }

  ngOnInit() {
    this.listenOnSidenavOpen();
    this.listenOnSidenavClose();
    this.listenOnMatSidenavCloseFinish();
  }

  listenOnSidenavOpen() {
    this.sidenavService.open$.subscribe((params: SidenavParams) => {
      if (this._sidenavLevel === params.level) {
        this.sidenavMode = params.mode || SIDENAV_DEFAULT_MODE;
        this.originalMode = this.sidenavMode;
        this.sidenavType = params.type || SIDENAV_DEFAULT_TYPE;
        this.sidenavRef.open();
        document.body.classList.add(this.bodyClass);
      } else {
        this.isExpanded = true;
      }
    });
  }

  listenOnSidenavClose() {
    this.sidenavRef.closedStart.subscribe(() => this.sidenavService.close(null, this._sidenavLevel));
    this.sidenavService.close$.subscribe((event: { level: number }) => {
      if (this._sidenavLevel === event.level) {
        this.sidenavRef.close();
        if (event.level === 0) {
          document.body.classList.remove(this.bodyClass);
        }
      } else if (this._sidenavLevel === (event.level - 1)) {
        this.sidenavMode = this.originalMode;
        this.isExpanded = false;
      }
    });
  }

  listenOnMatSidenavCloseFinish() {
    this.sidenavRef.openedChange.subscribe((status: boolean) => {
      if (!status) {
        this.sidenavService.detachComponent(this._sidenavLevel);
      }
    });
  }

}
