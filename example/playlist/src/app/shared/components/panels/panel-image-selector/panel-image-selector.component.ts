import { Component, Inject, OnInit } from '@angular/core';
import { SIDENAV_DATA, SidenavService } from '../../../services/sidenav/sidenav.service';
import { FormControl } from '@angular/forms';

export interface ImageSelectorData {
  control?: FormControl;
  aspectRatio?: number;
  type?: 'blob' | 'base64';
  crop?: boolean;
}

@Component({
  selector: 'app-panel-image-selector',
  templateUrl: './panel-image-selector.component.html',
  styleUrls: ['./panel-image-selector.component.scss']
})
export class PanelImageSelectorComponent implements OnInit {

  public control: FormControl;

  constructor(
    private sidenav: SidenavService,
    @Inject(SIDENAV_DATA) public data: ImageSelectorData
  ) {}

  ngOnInit() {
    this.control = new FormControl();
  }

  add() {
    if (this.data && this.data.control) {
      this.data.control.setValue(this.control.value);
      this.data.control.markAsDirty();
      this.sidenav.close(this.control.value);
    }
  }

}
