import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelImageSelectorComponent } from './panel-image-selector.component';

describe('PanelImageSelectorComponent', () => {
  let component: PanelImageSelectorComponent;
  let fixture: ComponentFixture<PanelImageSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelImageSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelImageSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
