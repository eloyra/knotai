import { AfterViewInit, Component, OnInit } from '@angular/core';
import { User } from '../../../backend/models/user';
import { CurrentUserFactory } from '../../../backend/auth/current-user';
import { fromEvent } from 'rxjs';
import { distinctUntilChanged, map, pairwise, throttleTime } from 'rxjs/operators';
import { ApplicationsMenuItem, ContextualMenu } from '../../../backend/models/user-menu';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit, AfterViewInit {

  public show = true;
  public user: User;
  public contextualMenu: ContextualMenu;
  public applicationsMenu: ApplicationsMenuItem[] = [];
  public urlPlay: string;

  public readonly userMenuItems: string[] = ['account', 'notifications', 'news', 'userManual'];

  constructor(
    private currentUser: CurrentUserFactory
  ) {}

  ngOnInit() {
    this.currentUser.user$.subscribe(user => this.user = user);
    this.contextualMenu = this.currentUser.contextualMenu;
    this.applicationsMenu = this.currentUser.applicationsMenu;
    // @TODO Let Esteban decide the default behaviour for this route when there is no urlPlay injected
    this.urlPlay = this.currentUser.urlPlay || '/student/home';
  }

  ngAfterViewInit() {
    fromEvent(window, 'scroll').pipe(
      throttleTime(50),
      map(() => document.documentElement.scrollTop),
      distinctUntilChanged(),
      pairwise()
    ).subscribe(([y1, y2]: [number, number]) => {
      this.show = (y2 < y1) || (y2 < 200);
    });
  }

}
