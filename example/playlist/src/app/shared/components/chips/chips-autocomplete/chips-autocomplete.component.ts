import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { ITag } from '../../../../backend/models/tag';
import { pullAllBy } from 'lodash';

export interface AutocompleteItem extends ITag {}

export interface AutocompleteQueryParams {
  query: string;
  size: number;
}

@Component({
  selector: 'app-chips-autocomplete',
  templateUrl: './chips-autocomplete.component.html',
  styleUrls: ['./chips-autocomplete.component.scss']
})
export class ChipsAutocompleteComponent implements OnInit {

  private static readonly NUM_RESULTS = 3;

  @ViewChild('input', {static: false}) input: ElementRef<HTMLInputElement>;
  @ViewChild('autocomplete', {static: false}) autocomplete: MatAutocomplete;

  @Input() label: string;
  @Input() selectable = true;
  @Input() removable = true;
  @Input() allowCustom = false;
  @Input() separatorKeysCodes: number[] = [ENTER, COMMA];
  @Input() items: AutocompleteItem[] = [];
  @Input() options$: Observable<AutocompleteItem[]>;

  @Output() itemsChange = new EventEmitter<AutocompleteItem[]>();
  @Output() chipRemoved = new EventEmitter<AutocompleteItem>();
  @Output() chipAdded = new EventEmitter<AutocompleteItem>();
  @Output() query = new EventEmitter<AutocompleteQueryParams>();

  public formCtrl = new FormControl();
  public _options$: Observable<AutocompleteItem[]>;

  constructor() {}

  ngOnInit() {
    this.formCtrl.valueChanges.pipe(
      debounceTime(350),
      distinctUntilChanged(),
      map((query: string) => this.query.emit({
        query,
        size: this.items.length + ChipsAutocompleteComponent.NUM_RESULTS
      }))
    ).subscribe();
    this._options$ = this.options$.pipe(
      map((options: AutocompleteItem[]) => {
        pullAllBy(options, this.items, 'id');
        options.length = options.length > ChipsAutocompleteComponent.NUM_RESULTS ? ChipsAutocompleteComponent.NUM_RESULTS : options.length;
        return options;
      })
    )
  }

  public add(event: MatChipInputEvent) {
    if (this.allowCustom) {
      const input = event.input;
      const value = (event.value || '').trim();
      if (value) {
        const item: AutocompleteItem = {
          name: value
        };
        this.items.push(item);
        this.itemsChange.emit(this.items);
        this.chipAdded.emit(item);
      }
      if (input) {
        input.value = '';
      }
      this.formCtrl.setValue(null);
    }
  }

  public remove(item: AutocompleteItem) {
    const index = this.items.indexOf(item);
    if (index >= 0) {
      this.items.splice(index, 1);
      this.itemsChange.emit(this.items);
      this.chipRemoved.emit(item);
    }
  }

  public selected(event: MatAutocompleteSelectedEvent) {
    const item: AutocompleteItem = {
      id: event.option.value,
      name: event.option.viewValue
    };
    this.items.push(item);
    this.input.nativeElement.value = '';
    this.formCtrl.setValue(null);
    this.itemsChange.emit(this.items);
    this.chipAdded.emit(item);
  }

}
