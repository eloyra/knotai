import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChipsPlaylistConfigComponent } from './chips-playlist-config.component';

describe('ChipsPlaylistConfigComponent', () => {
  let component: ChipsPlaylistConfigComponent;
  let fixture: ComponentFixture<ChipsPlaylistConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChipsPlaylistConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChipsPlaylistConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
