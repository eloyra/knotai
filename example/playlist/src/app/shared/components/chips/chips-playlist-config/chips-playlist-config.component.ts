import { Component, Input, OnInit } from '@angular/core';
import { PlaylistConfiguration } from '../../../../backend/models/playlist';

@Component({
  selector: 'app-chips-playlist-config',
  templateUrl: './chips-playlist-config.component.html',
  styleUrls: ['./chips-playlist-config.component.scss']
})
export class ChipsPlaylistConfigComponent implements OnInit {

  @Input() configuration: PlaylistConfiguration;

  public _playlistConfiguration = PlaylistConfiguration;

  constructor() {}

  ngOnInit() {}

}
