import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogConfig } from '@angular/material';

export const LOADER_CONFIG: MatDialogConfig = {
  panelClass: 'loader',
  disableClose: true
};

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoaderComponent implements OnInit {

  @Input() mainLoader = true;

  constructor() {}

  ngOnInit() {}

}
