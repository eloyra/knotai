import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

declare type TimeUnit = 'm' | 'h' | 'd' | 'w' | 'M' | 'y';

interface IDurationSimple {
  value: number;
  unit: TimeUnit;
}

@Pipe({
  name: 'timeAgo'
})
export class TimeAgoPipe implements PipeTransform {

  private static readonly MsInDateUnit: IDurationSimple[] = [
    {
      value: 3.154e10,
      unit: 'y'
    },
    {
      value: 2.628e9,
      unit: 'M'
    },
    {
      value: 6.048e8,
      unit: 'w'
    },
    {
      value: 8.64e7,
      unit: 'd'
    },
    {
      value: 3.6e6,
      unit: 'h'
    },
    {
      value: 6e4,
      unit: 'm'
    }
  ];

  constructor(
    private translate: TranslateService
  ) {}

  transform(value: Date | string): string {
    const diff = Math.abs((new Date()).getTime() - (new Date(value)).getTime());
    let duration: IDurationSimple;
    for (const unit of TimeAgoPipe.MsInDateUnit) {
      duration = {
        value: Math.floor(diff / unit.value),
        unit: unit.unit
      };
      if (duration.value >= 1) {
        break;
      }
    }
    return (
      duration.value >= 1 ?
        duration.value + ' ' + this.translate.instant(`time.${duration.unit}`, {count: duration.value}) :
        this.translate.instant('time.now')
    );
  }

}
