/**
 * @module fullscreen
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import fullscreenIcon from '../assets/icons/fullscreen.svg';

/**
 * The fullscreen plugin.
 *
 * @extends module:core/plugin~Plugin
 */
export default class FullscreenPlugin extends Plugin {

	init() {
		const fullscreenClassName = 'fullscreen';

		this.editor.ui.componentFactory.add('fullscreen', locale => {
			const view = new ButtonView(locale);

			view.set({
				label: 'Fullscreen',
				icon: fullscreenIcon,
				tooltip: true
			});

			view.on('execute', () => {
				this.editor.ui.view.element.classList.toggle(fullscreenClassName);
			});

			return view;
		});
	}

}
