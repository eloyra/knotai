import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MAT_TOOLTIP_DEFAULT_OPTIONS,
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatDialogModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatIconRegistry,
  MatInputModule,
  MatMenuModule,
  MatPaginatorIntl,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import { LoaderComponent } from './components/loader/loader.component';
import { environment } from '../../environments/environment';
import { CardPlaylistComponent } from './components/cards/card-playlist/card-playlist.component';
import { ImgDefaultDirective } from './directives/img-default/img-default.directive';
import { TabsMainNavComponent } from './components/tabs/tabs-main-nav/tabs-main-nav.component';
import { RouterModule } from '@angular/router';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SidenavContainerComponent } from './components/sidenav-container/sidenav-container.component';
import { CloseSidenavDirective } from './directives/close-sidenav/close-sidenav.directive';
import { ChipsAutocompleteComponent } from './components/chips/chips-autocomplete/chips-autocomplete.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardItemComponent } from './components/cards/card-item/card-item.component';
import { TabsButtonsComponent } from './components/tabs/tabs-buttons/tabs-buttons.component';
import { PlaylistsListComponent } from './components/lists/playlists-list/playlists-list.component';
import { ItemsListComponent } from './components/lists/items-list/items-list.component';
import { NgxMasonryModule } from 'ngx-masonry';
import { FavButtonComponent } from './components/buttons/fav-button/fav-button.component';
import { MenuButtonComponent } from './components/buttons/menu-button/menu-button.component';
import { ChipsPlaylistConfigComponent } from './components/chips/chips-playlist-config/chips-playlist-config.component';
import { TabsIconsComponent } from './components/tabs/tabs-icons/tabs-icons.component';
import { CardUserComponent } from './components/cards/card-user/card-user.component';
import { PeopleListComponent } from './components/lists/people-list/people-list.component';
import { NgxCropperjsModule } from 'ngx-cropperjs';
import { ImageSelectorComponent } from './components/image-selector/image-selector.component';
import { PanelImageSelectorComponent } from './components/panels/panel-image-selector/panel-image-selector.component';
import { StopPropagationDirective } from './directives/stop-propagation/stop-propagation.directive';
import { SimpleDialogComponent } from './components/dialogs/simple-dialog/simple-dialog.component';
import { InputDialogComponent } from './components/dialogs/input-dialog/input-dialog.component';
import { EllipsisModule } from 'ngx-ellipsis';
import { CommentComponent } from './components/comment/comment.component';
import { CommentsListComponent } from './components/lists/comments-list/comments-list.component';
import { TimeAgoPipe } from './pipes/time-ago/time-ago.pipe';
import { NgxImagesloadedModule } from 'ngx-imagesloaded';
import { PaginatorIntlService } from './services/paginator-intl/paginator-intl.service';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { ScrollTopButtonComponent } from './components/buttons/scroll-top-button/scroll-top-button.component';
import { AutoFocusDirective } from './directives/auto-focus/auto-focus.directive';

@NgModule({
  declarations: [
    LoaderComponent,
    CardPlaylistComponent,
    ImgDefaultDirective,
    TabsMainNavComponent,
    SidenavContainerComponent,
    CloseSidenavDirective,
    ChipsAutocompleteComponent,
    CardItemComponent,
    TabsButtonsComponent,
    PlaylistsListComponent,
    ItemsListComponent,
    FavButtonComponent,
    MenuButtonComponent,
    ChipsPlaylistConfigComponent,
    TabsIconsComponent,
    CardUserComponent,
    PeopleListComponent,
    ImageSelectorComponent,
    PanelImageSelectorComponent,
    StopPropagationDirective,
    SimpleDialogComponent,
    InputDialogComponent,
    CommentComponent,
    CommentsListComponent,
    TimeAgoPipe,
    TopBarComponent,
    ScrollTopButtonComponent,
    AutoFocusDirective
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMasonryModule,
    NgxImagesloadedModule,
    NgxCropperjsModule,
    EllipsisModule,
    MatButtonModule,
    MatMenuModule,
    MatTooltipModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatSnackBarModule,
    MatRippleModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatSliderModule,
    MatStepperModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatTooltipModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatSnackBarModule,
    MatRippleModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatSliderModule,
    MatStepperModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    TranslateModule,
    LoaderComponent,
    CardPlaylistComponent,
    ImgDefaultDirective,
    TabsMainNavComponent,
    SidenavContainerComponent,
    CloseSidenavDirective,
    ChipsAutocompleteComponent,
    CardItemComponent,
    TabsButtonsComponent,
    PlaylistsListComponent,
    ItemsListComponent,
    FavButtonComponent,
    MenuButtonComponent,
    ChipsPlaylistConfigComponent,
    TabsIconsComponent,
    CardUserComponent,
    PeopleListComponent,
    ImageSelectorComponent,
    PanelImageSelectorComponent,
    StopPropagationDirective,
    SimpleDialogComponent,
    InputDialogComponent,
    CommentComponent,
    CommentsListComponent,
    TimeAgoPipe,
    EllipsisModule,
    TopBarComponent,
    ScrollTopButtonComponent,
    AutoFocusDirective
  ],
  entryComponents: [
    LoaderComponent,
    PanelImageSelectorComponent,
    SimpleDialogComponent,
    InputDialogComponent
  ],
  providers: [
    MatIconRegistry,
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 2500,
        horizontalPosition: 'end',
        verticalPosition: 'top'
      }
    },
    {
      provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
      useValue: {
        position: 'above'
      }
    },
    {
      provide: MatPaginatorIntl,
      useFactory: (translate) => {
        const service = new PaginatorIntlService();
        service.injectTranslateService(translate);
        return service;
      },
      deps: [TranslateService]
    }
  ]
})
export class SharedModule {

  constructor(
    public matIconRegistry: MatIconRegistry
  ) {
    matIconRegistry.setDefaultFontSetClass(environment.iconClass);
  }

}
