export interface ITranslatableString {
  string: string;
  params: { [param: string]: string | number };
}
