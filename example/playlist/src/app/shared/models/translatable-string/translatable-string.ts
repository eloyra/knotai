import { ITranslatableString } from './itranslatable-string';
import { TranslateService } from '@ngx-translate/core';

export class TranslatableString implements ITranslatableString {

  string: string;
  params: { [param: string]: string | number };

  constructor(string: string, params?: { [param: string]: string | number }) {
    this.string = string;
    this.params = params || {};
  }

  public translate(translateService: TranslateService) {
    if (translateService) {
      return translateService.instant(this.string, this.params);
    }
  }

}
