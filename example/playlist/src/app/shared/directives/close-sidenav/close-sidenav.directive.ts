import { Directive, HostListener } from '@angular/core';
import { SidenavService } from '../../services/sidenav/sidenav.service';

@Directive({
  selector: '[appCloseSidenav]'
})
export class CloseSidenavDirective {

  constructor(
    private sidenavService: SidenavService
  ) {}

  @HostListener('click') onClick() {
    this.sidenavService.close();
  }

}
