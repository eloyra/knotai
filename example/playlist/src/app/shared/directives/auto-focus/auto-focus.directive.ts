import { AfterContentInit, Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appAutoFocus]'
})
export class AutoFocusDirective implements AfterContentInit {

  @Input() appAutoFocus = true;

  constructor(
    private el: ElementRef
  ) {}

  public ngAfterContentInit() {
    if (this.appAutoFocus) {
      setTimeout(() => this.el.nativeElement.focus(), 500);
    }
  }

}
