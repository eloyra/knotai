import { Directive, HostListener, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Directive({
  selector: 'img[appImgDefault]',
  host: {
    '[src]': 'src'
  }
})
export class ImgDefaultDirective {

  @Input('appImgDefault') default: string;
  @Input() src: string;

  constructor() {}

  @HostListener('error') onImgError() {
    this.src = environment.deployUrl + environment.imagesPath + this.default;
  }

}
