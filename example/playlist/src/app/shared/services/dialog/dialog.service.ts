import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { LOADER_CONFIG, LoaderComponent } from '../../components/loader/loader.component';
import {
  SIMPLE_DIALOG_CONFIG,
  SimpleDialogComponent,
  SimpleDialogContent
} from '../../components/dialogs/simple-dialog/simple-dialog.component';
import {
  INPUT_DIALOG_CONFIG,
  InputDialogComponent,
  InputDialogContent
} from '../../components/dialogs/input-dialog/input-dialog.component';
import { Observable } from 'rxjs';
import { TranslatableString } from '../../models/translatable-string/translatable-string';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  private loader: MatDialogRef<LoaderComponent>;
  private loadersCount = 0;

  constructor(
    private dialog: MatDialog
  ) {}

  // Loader

  public openLoader(config?: MatDialogConfig) {
    if (!this.loadersCount) {
      this.loader = this.dialog.open(LoaderComponent, {...LOADER_CONFIG, ...config});
      this.loadersCount++;
    }
  }

  public closeLoader() {
    if (this.loadersCount && this.loader) {
      setTimeout(() => {
        this.loader.close();
        this.loadersCount = (this.loadersCount - 1) || 0;
      }, 500);
    }
  }

  // Simple

  public simple(data: SimpleDialogContent, config?: MatDialogConfig): MatDialogRef<SimpleDialogComponent> {
    return this.dialog.open(SimpleDialogComponent, {data, ...SIMPLE_DIALOG_CONFIG, ...config});
  }

  // Input

  public input(data: InputDialogContent, config?: MatDialogConfig): MatDialogRef<InputDialogComponent> {
    return this.dialog.open(InputDialogComponent, {data, ...INPUT_DIALOG_CONFIG, ...config});
  }

  // Custom common dialogs

  public markAsInappropriate(): Observable<string> {
    return this.input({
      title: new TranslatableString('item.mark.title'),
      text: new TranslatableString('item.mark.text'),
      label: new TranslatableString('item.mark.label'),
      type: 'textarea',
      primaryAction: new TranslatableString('send'),
      secondaryAction: new TranslatableString('cancel'),
      icon: 'check',
    }).afterClosed();
  }

}
