import { MatPaginatorIntl } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

export class PaginatorIntlService extends MatPaginatorIntl {

  private translate: TranslateService;

  public get nextPageLabel() {
    return this.translate.instant('paginator.next');
  }

  public set nextPageLabel(value: string) {}

  public get previousPageLabel() {
    return this.translate.instant('paginator.previous');
  }

  public set previousPageLabel(value: string) {}

  public get itemsPerPageLabel() {
    return this.translate.instant('paginator.itemsPerPage');
  }

  public set itemsPerPageLabel(value: string) {}

  public getRangeLabel = (page: number, pageSize: number, length: number) => {
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return this.translate.instant('paginator.range', {
      start: startIndex + 1,
      end: endIndex,
      total: length
    });
  };

  public injectTranslateService(translate: TranslateService) {
    this.translate = translate;
    this.changes.next();
  }

}
