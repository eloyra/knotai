import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  Injectable,
  InjectionToken,
  Injector
} from '@angular/core';
import { ComponentPortal, DomPortalHost, PortalInjector, PortalOutlet } from '@angular/cdk/portal';
import { Observable, Subject } from 'rxjs';
import { distinctUntilKeyChanged } from 'rxjs/operators';
import { SidenavParams, SidenavType } from '../../components/sidenav-container/sidenav-container.component';
import { MatSidenav } from '@angular/material';

export const SIDENAV_DATA = new InjectionToken<{}>('SIDENAV_DATA');

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  public sidenavs: PortalOutlet[] = [];

  private openSource = new Subject<SidenavParams>();
  open$ = this.openSource.asObservable();
  private closeSource = new Subject<{ level: number, data: any }>();
  close$ = this.closeSource.asObservable();

  constructor(
    private applicationRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector
  ) {}

  open(component: any, data: object = {}, type?: SidenavType, mode?: MatSidenav['mode']): Observable<any> {
    const level = this.sidenavs.length;
    const _data = this.createData(data);
    const portal = new ComponentPortal(component, undefined, _data);
    this.attachComponent(portal, this.componentFactoryResolver, this.injector, level);
    this.openSource.next({level, type, mode});
    return new Observable<any>(subscriber => {
      this.close$.pipe(distinctUntilKeyChanged('level')).subscribe((event: { level: number, data: any }) => {
        if (level === event.level) {
          subscriber.next(event.data);
          subscriber.complete();
        }
      });
    });
  }

  close(data?: any, level?: number): void {
    this.closeSource.next({
      level: level || this.sidenavs.length - 1,
      data
    });
  }

  createData(data: any): PortalInjector {
    const injectorTokens = new WeakMap().set(SIDENAV_DATA, data);
    return new PortalInjector(this.injector, injectorTokens);
  }

  attachComponent(
    portal: ComponentPortal<any>,
    componentFactoryResolver: ComponentFactoryResolver,
    injector: Injector,
    level: number
  ): ComponentRef<any> {

    this.sidenavs[level] = new DomPortalHost(
      document.querySelector(`#sidenav-outlet-${level}`),
      componentFactoryResolver,
      this.applicationRef,
      injector
    );

    return this.sidenavs[level].attach(portal);
  }

  detachComponent(level: number): void {
    this.sidenavs[level].detach();
    this.sidenavs.pop();
  }

}
