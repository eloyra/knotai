import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { GestureConfig } from '@angular/material';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateCompiler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { MESSAGE_FORMAT_CONFIG, TranslateMessageFormatCompiler } from 'ngx-translate-messageformat-compiler';
import { environment } from '../environments/environment';
import { BackendModule } from './backend/backend.module';
import { EPortfolioModule } from './e-portfolio/e-portfolio.module';
import { RestangularConfigFactory } from './backend/services/config/RestangularConfigFactory';
import { RestangularModule } from 'ngx-restangular';
import { AuthService } from './backend/auth/auth.service';

registerLocaleData(localeEs, 'es');

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function restangularConfigFactory(RestangularProvider, authService) {
  return RestangularConfigFactory(RestangularProvider, authService);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      compiler: {
        provide: TranslateCompiler,
        useClass: TranslateMessageFormatCompiler
      }
    }),
    BackendModule.forRoot(),
    RestangularModule.forRoot([AuthService], restangularConfigFactory),
    SharedModule,
    EPortfolioModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: GestureConfig
    },
    {
      provide: LOCALE_ID,
      useValue: environment.defaultLang
    },
    {
      provide: MESSAGE_FORMAT_CONFIG,
      useValue: {
        locales: environment.availableLangs
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
