import { Component, OnInit } from '@angular/core';
import { NavLink } from '../../../shared/components/tabs/tabs-buttons/tabs-buttons.component';
import { TranslatableString } from '../../../shared/models/translatable-string/translatable-string';
import { CurrentUserFactory } from '../../../backend/auth/current-user';
import { Role } from '../../../backend/services/config/BackendConstants';
import { SidenavService } from '../../../shared/services/sidenav/sidenav.service';
import { RecycleBinComponent } from '../recycle-bin/recycle-bin.component';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {

  public navLinks: NavLink[] = [];
  public showMenu = false;

  constructor(
    private currentUser: CurrentUserFactory,
    private sidenav: SidenavService
  ) {}

  ngOnInit() {
    if (this.currentUser.isRole(Role.ADMIN)) {
      this.showMenu = true;
      this.currentUser.relations$.subscribe(relations => {
        this.navLinks = [
          {
            path: 'own',
            label: new TranslatableString('sections.myItems'),
            amount: relations.reviews && relations.reviews.elements.inappropriate.elements
          },
          {
            path: 'users',
            label: new TranslatableString('sections.userItems'),
            amount: relations.reviews && relations.reviews.elements.inappropriateOthers.elements
          }
        ];
      });
    }
  }

  public openRecycleBin() {
    this.sidenav.open(RecycleBinComponent);
  }

}
