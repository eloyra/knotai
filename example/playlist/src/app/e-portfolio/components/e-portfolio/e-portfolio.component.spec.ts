import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPortfolioComponent } from './e-portfolio.component';

describe('EPortfolioComponent', () => {
  let component: EPortfolioComponent;
  let fixture: ComponentFixture<EPortfolioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPortfolioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
