import { Component, OnInit } from '@angular/core';
import { MainNavTab } from '../../../shared/components/tabs/tabs-main-nav/tabs-main-nav.component';
import { User, UserRelations } from '../../../backend/models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentUserFactory } from '../../../backend/auth/current-user';
import { UsersService } from '../../../backend/services';
import { Location } from '@angular/common';
import { switchMapTo, tap } from 'rxjs/operators';

@Component({
  selector: 'app-e-portfolio',
  templateUrl: './e-portfolio.component.html',
  styleUrls: ['./e-portfolio.component.scss']
})
export class EPortfolioComponent implements OnInit {

  public mainTabs: MainNavTab[] = [];
  public user: User;
  public isCurrentUser = true;

  constructor(
    private currentUser: CurrentUserFactory,
    private usersService: UsersService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    this.currentUser.user$.pipe(
      tap(user => this.user = user),
      switchMapTo(this.route.data)
    ).subscribe((data: { user: User }) => {
      if (data && data.user) {
        if (data.user.id !== this.currentUser.id) {
          this.isCurrentUser = false;
          this.user = data.user;
          this.createTabs(data.user.relations);
        } else {
          this.router.navigate(['/']);
        }
      } else {
        this.currentUser.relations$.subscribe(relations => {
          this.createTabs(relations);
        });
      }
    });
  }

  public back() {
    if (!this.isCurrentUser) {
      this.location.back();
    }
  }

  private createTabs(relations: UserRelations) {
    this.mainTabs = [
      {
        path: 'playlists',
        label: `sections.${this.isCurrentUser ? 'myPlaylists' : 'playlists'}`,
        amount: relations.playlists && relations.playlists.elements
      },
      {
        path: 'subscriptions',
        label: 'sections.mySubs',
        amount: relations.subscriptions && relations.subscriptions.elements.playlists.elements,
        hide: !this.isCurrentUser
      },
      {
        path: 'favourites',
        label: 'sections.favourites',
        amount: relations.favourites && relations.favourites.elements
      },
      {
        path: 'review',
        label: 'sections.review',
        amount: relations.reviews && relations.reviews.elements.inappropriate.elements,
        hide: !this.isCurrentUser
      }
    ];
  }

}
