import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeoplePlaylistSubscribersComponent } from './people-playlist-subscribers.component';

describe('PeoplePlaylistSubscribersComponent', () => {
  let component: PeoplePlaylistSubscribersComponent;
  let fixture: ComponentFixture<PeoplePlaylistSubscribersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeoplePlaylistSubscribersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeoplePlaylistSubscribersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
