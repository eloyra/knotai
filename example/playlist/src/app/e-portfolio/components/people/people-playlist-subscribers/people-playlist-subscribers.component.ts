import { Component, OnInit } from '@angular/core';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { User } from '../../../../backend/models/user';
import { PeopleListComponent } from '../../../../shared/components/lists/people-list/people-list.component';
import { Playlist } from '../../../../backend/models/playlist';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentUserFactory } from '../../../../backend/auth/current-user';

@Component({
  selector: 'app-people-playlist-subscribers',
  templateUrl: './people-playlist-subscribers.component.html',
  styleUrls: ['./people-playlist-subscribers.component.scss']
})
export class PeoplePlaylistSubscribersComponent implements OnInit {

  public playlist: Playlist;
  public users: User[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;

  constructor(
    private portfolioService: EPortfolioService,
    private route: ActivatedRoute,
    private router: Router,
    private currentUser: CurrentUserFactory
  ) {}

  ngOnInit() {
    this.route.parent.data.subscribe((data: { playlist: Playlist }) => {
      if (data.playlist && data.playlist.author.id === this.currentUser.id) {
        this.playlist = data.playlist;
        this.getPeople();
      } else {
        this.router.navigate(['../items'], {relativeTo: this.route});
      }
    });
  }

  public getPeople() {
    this.loading = true;
    this.portfolioService.getPlaylistSubscribers(this.playlist.id, ++this.page, PeopleListComponent.pageSize)
      .pipe(finalize(() => this.loading = false))
      .subscribe(users => {
        this.users.push(...users);
        this.isLastPage = this.users.length >= (users as RestangularCollection<User>).totalItems;
      });
  }

}
