import { Component, Inject, OnInit } from '@angular/core';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { Observable, Subject } from 'rxjs';
import { AutocompleteQueryParams } from '../../../../shared/components/chips/chips-autocomplete/chips-autocomplete.component';
import { Tag } from '../../../../backend/models/tag';
import { filter, finalize, switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IPlaylist, Playlist, PlaylistConfiguration } from '../../../../backend/models/playlist';
import { DialogService } from '../../../../shared/services/dialog/dialog.service';
import { SIDENAV_DATA, SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import {
  ImageSelectorData,
  PanelImageSelectorComponent
} from '../../../../shared/components/panels/panel-image-selector/panel-image-selector.component';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastService } from '../../../../shared/services/toast/toast.service';

export interface PlaylistsAddData {
  currentPlaylist?: Playlist;
}

@Component({
  selector: 'app-playlists-add',
  templateUrl: './playlists-add.component.html',
  styleUrls: ['./playlists-add.component.scss']
})
export class PlaylistsAddComponent implements OnInit {

  public tagsSubject = new Subject<AutocompleteQueryParams>();
  public tags$: Observable<Tag[]>;
  public tags: Tag[] = [];

  public form: FormGroup;
  public preview: string;

  public _playlistConfigs = PlaylistConfiguration;

  constructor(
    private portfolioService: EPortfolioService,
    private fb: FormBuilder,
    private dialog: DialogService,
    private sidenav: SidenavService,
    private toast: ToastService,
    @Inject(SIDENAV_DATA) public data: PlaylistsAddData
  ) {
    if (data.currentPlaylist) {
      this.tags = data.currentPlaylist.tags.slice();
      this.preview = data.currentPlaylist.avatar;
    }
  }

  get _playlistConfigsKeys() {
    const keys = Object.keys(this._playlistConfigs);
    return keys.slice(0, keys.length / 2);
  }

  ngOnInit() {
    this.form = this.fb.group({
      avatar: null,
      title: [this.data.currentPlaylist && this.data.currentPlaylist.title || '', Validators.required],
      description: this.data.currentPlaylist && this.data.currentPlaylist.description || '',
      configuration: [
        (this.data.currentPlaylist && this.data.currentPlaylist.configuration || PlaylistConfiguration.PUBLIC).toString(),
        Validators.required
      ]
    });
    this.form.controls.avatar.valueChanges.subscribe(blob => {
      const reader = new FileReader();
      reader.onload = _ => this.preview = reader.result.toString();
      reader.readAsDataURL(blob);
    });
    this.tags$ = this.tagsSubject.asObservable().pipe(
      filter(value => !value.query || value.query.length >= 3),
      switchMap((value: AutocompleteQueryParams) => this.portfolioService.getTags(value.query, value.size))
    );
  }

  public editCover() {
    this.sidenav.open(PanelImageSelectorComponent, <ImageSelectorData>{
      control: this.form.get('avatar'),
      crop: true,
      aspectRatio: 1
    });
  }

  public submit() {
    this.dialog.openLoader();
    const playlist: IPlaylist = this.form.value as IPlaylist;
    playlist.tags = this.tags.map(tag => {
      if (tag.id) {
        delete tag.name
      }
      return tag;
    });
    if (this.data.currentPlaylist) {
      playlist.id = this.data.currentPlaylist.id;
    }
    this.portfolioService.addPlaylist(playlist)
      .pipe(finalize(() => this.dialog.closeLoader()))
      .subscribe(addedPlaylist => {
        this.sidenav.close(addedPlaylist);
      }, (error: HttpErrorResponse) => {
        this.toast.showError(error.status);
      });
  }

}
