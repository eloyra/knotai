import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistsSubscribedComponent } from './playlists-subscribed.component';

describe('PlaylistsSubscribedComponent', () => {
  let component: PlaylistsSubscribedComponent;
  let fixture: ComponentFixture<PlaylistsSubscribedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistsSubscribedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsSubscribedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
