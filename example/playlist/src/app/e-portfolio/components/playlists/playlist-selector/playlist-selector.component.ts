import { Component, OnInit } from '@angular/core';
import { PlaylistsListComponent } from '../../../../shared/components/lists/playlists-list/playlists-list.component';
import { filter, finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { Playlist } from '../../../../backend/models/playlist';
import { SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { PlaylistsAddComponent } from '../playlists-add/playlists-add.component';

@Component({
  selector: 'app-playlist-selector',
  templateUrl: './playlist-selector.component.html',
  styleUrls: ['./playlist-selector.component.scss']
})
export class PlaylistSelectorComponent implements OnInit {

  public playlists: Playlist[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;

  constructor(
    private sidenav: SidenavService,
    private portfolioService: EPortfolioService
  ) {}

  ngOnInit() {
    this.getPlaylists();
  }

  public add() {
    this.sidenav.open(PlaylistsAddComponent)
      .pipe(filter(addedPlaylist => !!addedPlaylist))
      .subscribe(addedPlaylist => {
        setTimeout(() => this.select(addedPlaylist), 500);
      });
  }

  public getPlaylists() {
    this.loading = true;
    this.portfolioService.getOwnPlaylists(++this.page, PlaylistsListComponent.pageSize)
      .pipe(finalize(() => this.loading = false))
      .subscribe(playlists => {
        this.playlists.push(...playlists);
        this.isLastPage = this.playlists.length >= (playlists as RestangularCollection<Playlist>).totalItems;
      });
  }

  public select(playlist: Playlist) {
    this.sidenav.close(playlist);
  }

}
