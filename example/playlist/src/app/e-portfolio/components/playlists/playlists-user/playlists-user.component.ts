import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../../../backend/models/playlist';
import { SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { PlaylistsAddComponent } from '../playlists-add/playlists-add.component';
import { filter, finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { PlaylistsListComponent } from '../../../../shared/components/lists/playlists-list/playlists-list.component';
import { CurrentUserFactory } from '../../../../backend/auth/current-user';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../../backend/models/user';
import { ToastService } from '../../../../shared/services/toast/toast.service';
import { TranslatableString } from '../../../../shared/models/translatable-string/translatable-string';

@Component({
  selector: 'app-playlists-user',
  templateUrl: './playlists-user.component.html',
  styleUrls: ['./playlists-user.component.scss']
})
export class PlaylistsUserComponent implements OnInit {

  public playlists: Playlist[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;
  public userId: string;

  constructor(
    private sidenav: SidenavService,
    private portfolioService: EPortfolioService,
    private currentUser: CurrentUserFactory,
    private route: ActivatedRoute,
    private toast: ToastService
  ) {}

  ngOnInit() {
    this.route.parent.data.subscribe((data: { user: User }) => {
      this.userId = data && data.user && data.user.id;
      this.reload();
    });
  }

  public add() {
    this.sidenav.open(PlaylistsAddComponent)
      .pipe(filter(addedPlaylist => !!addedPlaylist))
      .subscribe(addedPlaylist => {
        this.toast.open(new TranslatableString('toasts.addedItem'));
        this.reload();
      });
  }

  public getPlaylists() {
    this.loading = true;
    this.portfolioService.getPlaylistsByUser(this.userId || this.currentUser.id, ++this.page, PlaylistsListComponent.pageSize)
      .pipe(finalize(() => this.loading = false))
      .subscribe(playlists => {
        this.playlists.push(...playlists);
        this.isLastPage = this.playlists.length >= (playlists as RestangularCollection<Playlist>).totalItems;
      });
  }

  public reload() {
    this.page = 0;
    this.playlists = [];
    this.getPlaylists();
  }

}
