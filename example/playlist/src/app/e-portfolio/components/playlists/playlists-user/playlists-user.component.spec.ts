import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistsUserComponent } from './playlists-user.component';

describe('PlaylistsUserComponent', () => {
  let component: PlaylistsUserComponent;
  let fixture: ComponentFixture<PlaylistsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
