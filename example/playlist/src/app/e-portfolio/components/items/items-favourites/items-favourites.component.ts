import { Component, OnInit } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { ItemsListComponent } from '../../../../shared/components/lists/items-list/items-list.component';
import { finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { CurrentUserFactory } from '../../../../backend/auth/current-user';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { User } from '../../../../backend/models/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-items-favourites',
  templateUrl: './items-favourites.component.html',
  styleUrls: ['./items-favourites.component.scss']
})
export class ItemsFavouritesComponent implements OnInit {

  public items: Item[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;

  private userId: string;

  constructor(
    private route: ActivatedRoute,
    private portfolioService: EPortfolioService,
    private currentUser: CurrentUserFactory
  ) {}

  ngOnInit() {
    this.route.parent.data.subscribe((data: { user: User }) => {
      this.userId = data && data.user && data.user.id;
      this.getItems();
    });
  }

  public getItems(lastOnly?: boolean) {
    this.loading = true;
    const _page = lastOnly ? this.items.length + 1 : ++this.page;
    const _size = lastOnly ? 1 : ItemsListComponent.pageSize;
    this.portfolioService.getFavouriteItems(this.userId || this.currentUser.id, _page, _size)
      .pipe(finalize(() => this.loading = false))
      .subscribe(items => {
        this.items.push(...items);
        this.isLastPage = this.items.length >= (items as RestangularCollection<Item>).totalItems;
      });
  }

  onUnfavourite(index: number) {
    this.items.splice(index, 1);
    this.getItems(true);
  }

}
