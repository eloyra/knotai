import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { map, startWith, switchMap } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { MatPaginator, MatSort } from '@angular/material';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { Item } from '../../../../backend/models/item';
import { merge } from 'rxjs';

@Component({
  selector: 'app-items-deleted',
  templateUrl: './items-deleted.component.html',
  styleUrls: ['./items-deleted.component.scss']
})
export class ItemsDeletedComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  @Input() userId: string;

  public items: Item[] = [];
  public displayedColumns: string[] = ['avatar', 'title', 'author', 'created'];
  public resultsLength = 0;
  public loading = true;

  constructor(
    private portfolioService: EPortfolioService
  ) {}

  ngOnInit() {
    if (this.userId) {
      this.displayedColumns.splice(this.displayedColumns.findIndex(val => val === 'author'), 1);
    }
  }

  ngAfterViewInit() {
    merge(this.paginator.page, this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.loading = true;
          return this.portfolioService.getDeletedItems(this.userId, this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort);
        }),
        map(items => {
          this.loading = false;
          this.resultsLength = (items as RestangularCollection<Item>).totalItems;
          return items;
        })
      ).subscribe((items: Item[]) => this.items = items);
  }

}
