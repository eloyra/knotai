import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsDeletedOwnComponent } from './items-deleted-own.component';

describe('ItemsDeletedOwnComponent', () => {
  let component: ItemsDeletedOwnComponent;
  let fixture: ComponentFixture<ItemsDeletedOwnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsDeletedOwnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsDeletedOwnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
