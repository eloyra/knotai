import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { SIDENAV_DATA, SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { FormControl } from '@angular/forms';
import { filter, finalize, map, startWith, switchMap, take, tap } from 'rxjs/operators';
import { DialogService } from '../../../../shared/services/dialog/dialog.service';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { TranslatableString } from '../../../../shared/models/translatable-string/translatable-string';
import { Inappropriate } from '../../../../backend/models/inappropriate';
import { MatPaginator } from '@angular/material';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastService } from '../../../../shared/services/toast/toast.service';

export interface ItemOpinionsData {
  item: Item
}

export enum ItemOpinionsAction {
  UNMARKED,
  DELETED
}

@Component({
  selector: 'app-item-opinions',
  templateUrl: './item-opinions.component.html',
  styleUrls: ['./item-opinions.component.scss']
})
export class ItemOpinionsComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  public markAppropriateControl: FormControl;
  public comments: Inappropriate[] = [];

  public displayedColumns: string[] = ['comment', 'date'];
  public resultsLength = 0;
  public loading = true;

  constructor(
    private sidenav: SidenavService,
    private dialog: DialogService,
    private toast: ToastService,
    private portfolioService: EPortfolioService,
    @Inject(SIDENAV_DATA) public data: ItemOpinionsData
  ) {}

  ngOnInit() {
    this.markAppropriateControl = new FormControl(false);
    this.markAppropriateControl.valueChanges.pipe(take(1)).subscribe(() => {
      this.markAsAppropriate();
    });
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          this.loading = true;
          return this.portfolioService.getItemOpinions(this.data.item.id, this.paginator.pageIndex + 1, this.paginator.pageSize);
        }),
        map(comments => {
          this.loading = false;
          this.resultsLength = (comments as RestangularCollection<Inappropriate>).totalItems;
          return comments;
        })
      ).subscribe((comments: Inappropriate[]) => this.comments = comments);
  }

  public removeItem() {
    this.dialog.simple({
      title: new TranslatableString('item.delete.title'),
      text: new TranslatableString('item.delete.text'),
      primaryAction: new TranslatableString('delete'),
      secondaryAction: new TranslatableString('cancel'),
      icon: 'trash'
    }).afterClosed().pipe(
      filter((_delete: boolean) => _delete),
      tap(() => this.dialog.openLoader()),
      switchMap(() => this.portfolioService.removeInappropriateItem(this.data.item)),
      finalize(() => this.dialog.closeLoader())
    ).subscribe(() => {
      this.sidenav.close(ItemOpinionsAction.DELETED);
    }, (error: HttpErrorResponse) => {
      this.toast.showError(error.status);
    });
  }

  private markAsAppropriate() {
    setTimeout(() => {
      this.markAppropriateControl.disable();
      this.dialog.openLoader();
      this.portfolioService.markItemAppropriate(this.data.item.inappropriate)
        .pipe(finalize(() => this.dialog.closeLoader()))
        .subscribe(() => {
          this.sidenav.close(ItemOpinionsAction.UNMARKED);
        }, (error: HttpErrorResponse) => {
          this.toast.showError(error.status);
        });
    }, 600);
  }

}
