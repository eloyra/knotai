import { Component, OnInit } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { ItemsListComponent } from '../../../../shared/components/lists/items-list/items-list.component';
import { finalize, map, switchMap, take } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { CurrentUserFactory } from '../../../../backend/auth/current-user';

@Component({
  selector: 'app-items-review-own',
  templateUrl: './items-review-own.component.html',
  styleUrls: ['./items-review-own.component.scss']
})
export class ItemsReviewOwnComponent implements OnInit {

  public items: Item[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;

  constructor(
    private portfolioService: EPortfolioService,
    private currentUser: CurrentUserFactory
  ) {}

  ngOnInit() {
    this.getItems();
  }

  public getItems(lastOnly?: boolean) {
    this.loading = true;
    const _page = lastOnly ? this.items.length + 1 : ++this.page;
    const _size = lastOnly ? 1 : ItemsListComponent.pageSize;
    this.portfolioService.getInappropriateItems(this.currentUser.id, _page, _size).pipe(
      finalize(() => this.loading = false),
      switchMap(items => this.currentUser.user$.pipe(
        map(user => {
          for (let i = 0; i < items.length; i++) {
            items[i].author = user;
          }
          return items;
        })
      )),
      take(1)
    ).subscribe(items => {
      this.items.push(...items);
      this.isLastPage = this.items.length >= (items as RestangularCollection<Item>).totalItems;
    });
  }

  onUnmark(index: number) {
    this.items.splice(index, 1);
    this.getItems(true);
  }

}
