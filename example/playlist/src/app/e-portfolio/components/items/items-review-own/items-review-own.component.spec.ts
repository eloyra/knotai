import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsReviewOwnComponent } from './items-review-own.component';

describe('ItemsReviewOwnComponent', () => {
  let component: ItemsReviewOwnComponent;
  let fixture: ComponentFixture<ItemsReviewOwnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsReviewOwnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsReviewOwnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
