import { Component, OnInit } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { ItemsListComponent } from '../../../../shared/components/lists/items-list/items-list.component';

@Component({
  selector: 'app-items-subscribed',
  templateUrl: './items-subscribed.component.html',
  styleUrls: ['./items-subscribed.component.scss']
})

export class ItemsSubscribedComponent implements OnInit {

  public items: Item[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;

  constructor(
    private portfolioService: EPortfolioService
  ) {}

  ngOnInit() {
    this.getItems();
  }

  public getItems() {
    this.loading = true;
    this.portfolioService.getSubscribedItems(++this.page, ItemsListComponent.pageSize)
      .pipe(finalize(() => this.loading = false))
      .subscribe(items => {
        this.items.push(...items);
        this.isLastPage = this.items.length >= (items as RestangularCollection<Item>).totalItems;
      });
  }

}
