import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsSubscribedComponent } from './items-subscribed.component';

describe('ItemsSubscribedComponent', () => {
  let component: ItemsSubscribedComponent;
  let fixture: ComponentFixture<ItemsSubscribedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemsSubscribedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsSubscribedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
