import { Component, OnInit } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { ItemsListComponent } from '../../../../shared/components/lists/items-list/items-list.component';
import { filter, finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';
import { SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { Playlist } from '../../../../backend/models/playlist';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CurrentUserFactory } from '../../../../backend/auth/current-user';
import { ItemsAddComponent, ItemsAddData } from '../items-add/items-add.component';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { User } from '../../../../backend/models/user';

@Component({
  selector: 'app-items-playlist',
  templateUrl: './items-playlist.component.html',
  styleUrls: ['./items-playlist.component.scss']
})
export class ItemsPlaylistComponent implements OnInit {

  public playlist: Playlist;
  public items: Item[] = [];
  public showAdd: boolean;
  public page = 0;
  public isLastPage = true;
  public loading = false;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private currentUser: CurrentUserFactory,
    private sidenav: SidenavService,
    private portfolioService: EPortfolioService
  ) {}

  ngOnInit() {
    this.route.parent.data.subscribe((data: { playlist: Playlist, reload: boolean }) => {
      if (data.playlist) {
        this.playlist = data.playlist;
        this.showAdd = !data.playlist.author || data.playlist.author.id === this.currentUser.id;
        if (data.reload !== false) {
          this.reload();
        }
      }
    });
  }

  public add() {
    this.sidenav.open(ItemsAddComponent, <ItemsAddData>{
      initialPlaylist: this.playlist
    }).pipe(filter((addedItem: Item) => !!addedItem))
      .subscribe((addedItem: Item) => {
        this.updatePlaylist(addedItem.playlist);
      });
  }

  public getItems(lastOnly?: boolean) {
    this.loading = true;
    const _page = lastOnly ? this.items.length + 1 : ++this.page;
    const _size = lastOnly ? 1 : ItemsListComponent.pageSize;
    this.portfolioService.getPlaylistItems(this.playlist.id, _page, _size)
      .pipe(finalize(() => this.loading = false))
      .subscribe(items => {
        this.items.push(...items);
        this.isLastPage = this.items.length >= (items as RestangularCollection<Item>).totalItems;
        if (lastOnly) {
          this.playlist.numberItems = (items as RestangularCollection<Item>).totalItems;
          this.updatePlaylist(this.playlist, false);
        }
      });
  }

  public reload() {
    this.page = 0;
    this.items = [];
    this.getItems();
  }

  private updatePlaylist(playlist: Playlist, reload = true) {
    if (playlist.id === this.playlist.id) {
      (this.route.parent.data as BehaviorSubject<any>).next({
        ...(this.route.parent.data as BehaviorSubject<any>).value,
        playlist: Object.assign({}, this.playlist, playlist),
        reload
      });
    }
  }

}
