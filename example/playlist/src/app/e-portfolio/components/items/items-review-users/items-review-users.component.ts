import { Component, OnInit } from '@angular/core';
import { Item } from '../../../../backend/models/item';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { ItemsListComponent } from '../../../../shared/components/lists/items-list/items-list.component';
import { finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';

@Component({
  selector: 'app-items-review-users',
  templateUrl: './items-review-users.component.html',
  styleUrls: ['./items-review-users.component.scss']
})
export class ItemsReviewUsersComponent implements OnInit {

  public items: Item[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;

  constructor(
    private portfolioService: EPortfolioService
  ) {}

  ngOnInit() {
    this.getItems();
  }

  public getItems(lastOnly?: boolean) {
    this.loading = true;
    const _page = lastOnly ? this.items.length + 1 : ++this.page;
    const _size = lastOnly ? 1 : ItemsListComponent.pageSize;
    this.portfolioService.getInappropriateItems(null, _page, _size)
      .pipe(finalize(() => this.loading = false))
      .subscribe(items => {
        this.items.push(...items);
        this.isLastPage = this.items.length >= (items as RestangularCollection<Item>).totalItems;
      });
  }

}
