import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { ItemsAddComponent, ItemsAddData } from '../items-add/items-add.component';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { Item } from '../../../../backend/models/item';

declare var window: Window;

@Component({
  selector: 'app-items-bookmarklet',
  templateUrl: './items-bookmarklet.component.html',
  styleUrls: ['./items-bookmarklet.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ItemsBookmarkletComponent implements OnInit {

  public state: 'init' | 'success' = 'init';
  public item: Item;

  constructor(
    private route: ActivatedRoute,
    private sidenav: SidenavService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params: { url: string }) => {
      if (params.url) {
        this.sidenav.open(ItemsAddComponent, <ItemsAddData>{
          link: decodeURIComponent(params.url),
          initialPlaylist: null
        }, 'expanded').subscribe(addedItem => {
          if (addedItem) {
            this.item = addedItem;
            this.state = 'success';
          }
          setTimeout(() => window.close(), addedItem ? 2500 : 0);
        });
      }
    });
  }

}
