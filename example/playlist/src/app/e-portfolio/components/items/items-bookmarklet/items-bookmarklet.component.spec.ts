import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsBookmarkletComponent } from './items-bookmarklet.component';

describe('ItemsBookmarkletComponent', () => {
  let component: ItemsBookmarkletComponent;
  let fixture: ComponentFixture<ItemsBookmarkletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsBookmarkletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsBookmarkletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
