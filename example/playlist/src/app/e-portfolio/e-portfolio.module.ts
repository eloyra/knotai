import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EPortfolioRoutingModule } from './e-portfolio-routing.module';
import { EPortfolioComponent } from './components/e-portfolio/e-portfolio.component';
import { PlaylistsAddComponent } from './components/playlists/playlists-add/playlists-add.component';
import { SharedModule } from '../shared/shared.module';
import { MySubscriptionsComponent } from './components/my-subscriptions/my-subscriptions.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlaylistDetailComponent } from './components/playlists/playlist-detail/playlist-detail.component';
import { ItemDetailComponent } from './components/items/item-detail/item-detail.component';
import { PlaylistsSubscribedComponent } from './components/playlists/playlists-subscribed/playlists-subscribed.component';
import { PlaylistsUserComponent } from './components/playlists/playlists-user/playlists-user.component';
import { ItemsSubscribedComponent } from './components/items/items-subscribed/items-subscribed.component';
import { ItemsPlaylistComponent } from './components/items/items-playlist/items-playlist.component';
import { PeoplePlaylistSubscribersComponent } from './components/people/people-playlist-subscribers/people-playlist-subscribers.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ItemsAddComponent } from './components/items/items-add/items-add.component';
import { PlaylistSelectorComponent } from './components/playlists/playlist-selector/playlist-selector.component';
import { ItemsFavouritesComponent } from './components/items/items-favourites/items-favourites.component';
import { ItemsBookmarkletComponent } from './components/items/items-bookmarklet/items-bookmarklet.component';
import { ReviewComponent } from './components/review/review.component';
import { ItemsReviewOwnComponent } from './components/items/items-review-own/items-review-own.component';
import { ItemsReviewUsersComponent } from './components/items/items-review-users/items-review-users.component';
import { ItemOpinionsComponent } from './components/items/item-opinions/item-opinions.component';
import { RecycleBinComponent } from './components/recycle-bin/recycle-bin.component';
import { ItemsDeletedComponent } from './components/items/items-deleted/items-deleted.component';

@NgModule({
  declarations: [
    EPortfolioComponent,
    PlaylistsAddComponent,
    MySubscriptionsComponent,
    PlaylistsAddComponent,
    PlaylistDetailComponent,
    PlaylistsUserComponent,
    PlaylistsSubscribedComponent,
    ItemsSubscribedComponent,
    ItemDetailComponent,
    ItemsPlaylistComponent,
    PeoplePlaylistSubscribersComponent,
    ItemsAddComponent,
    PlaylistSelectorComponent,
    ItemsFavouritesComponent,
    ItemsBookmarkletComponent,
    ReviewComponent,
    ItemsReviewOwnComponent,
    ItemsReviewUsersComponent,
    ItemOpinionsComponent,
    RecycleBinComponent,
    ItemsDeletedComponent
  ],
  imports: [
    CommonModule,
    EPortfolioRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule
  ],
  entryComponents: [
    PlaylistsAddComponent,
    ItemsAddComponent,
    PlaylistSelectorComponent,
    ItemOpinionsComponent,
    RecycleBinComponent
  ]
})
export class EPortfolioModule {}
