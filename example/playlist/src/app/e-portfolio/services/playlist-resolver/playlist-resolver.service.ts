import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Playlist } from '../../../backend/models/playlist';
import { EMPTY, Observable } from 'rxjs';
import { EPortfolioService } from '../e-portfolio/e-portfolio.service';
import { catchError, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlaylistResolverService implements Resolve<Playlist> {

  constructor(
    private portfolioService: EPortfolioService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Playlist> {
    const id = route.paramMap.get('playlistId');
    return this.portfolioService.getPlaylist(id).pipe(
      take(1),
      catchError(() => this.errorHandling())
    );
  }

  private errorHandling(): Observable<never> {
    this.router.navigate(['/']);
    return EMPTY;
  }

}
