import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { User } from '../../../backend/models/user';
import { UsersService } from '../../../backend/services';
import { EMPTY, Observable } from 'rxjs';
import { catchError, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User> {

  constructor(
    private usersService: UsersService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    const id = route.paramMap.get('userId');
    return this.usersService.get(id).pipe(
      take(1),
      catchError(() => this.errorHandling())
    );
  }

  private errorHandling(): Observable<never> {
    this.router.navigate(['/']);
    return EMPTY;
  }

}
