import { TestBed } from '@angular/core/testing';

import { EPortfolioService } from './e-portfolio.service';

describe('EPortfolioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EPortfolioService = TestBed.get(EPortfolioService);
    expect(service).toBeTruthy();
  });
});
