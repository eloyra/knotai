import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Item } from '../../../backend/models/item';
import { EMPTY, Observable } from 'rxjs';
import { EPortfolioService } from '../e-portfolio/e-portfolio.service';
import { catchError, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemResolverService implements Resolve<Item> {

  constructor(
    private portfolioService: EPortfolioService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Item> {
    const id = route.paramMap.get('itemId');
    return this.portfolioService.getItemById(id).pipe(
      take(1),
      catchError(() => this.errorHandling())
    );
  }

  private errorHandling(): Observable<never> {
    this.router.navigate(['/']);
    return EMPTY;
  }

}
