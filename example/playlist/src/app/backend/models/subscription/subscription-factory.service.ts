import { Injectable } from '@angular/core';
import { IFactory } from '../../services/base/IFactory';
import { Subscription } from './subscription';
import { ISubscription } from './isubscription';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionFactoryService implements IFactory<Subscription> {

  constructor() {}

  build(data: ISubscription): Subscription {

    return new Subscription(data);

  }

}
