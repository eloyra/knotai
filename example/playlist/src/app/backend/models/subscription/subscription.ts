import { BaseModel } from '../base-model';
import { ISubscription } from './isubscription';

export class Subscription extends BaseModel implements ISubscription {

  public static readonly _resource: string = 'subscriptions';

  id?: string;
  idPlaylist?: string;

  constructor(data?: ISubscription) {
    super(data);
  }

}
