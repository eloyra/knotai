import { BaseModel } from '../base-model';
import { IItem } from './iitem';
import { Playlist } from '../playlist';
import { User } from '../user';
import { Inappropriate } from '../inappropriate';

export class Item extends BaseModel implements IItem {

  public static readonly _resource: string = 'items';

  id: string;
  description?: string;
  summary?: string;
  title: string;
  link?: string;
  created?: string;
  avatar?: string;
  image?: string;
  inappropriate?: Inappropriate;
  idFavourite?: string | null;
  playlist?: Playlist;
  author?: User;
  idPlaylist?: string;
  numberComments?: number;

  constructor(data?: IItem) {
    super(data);
  }

}
