import { IPlaylist } from '../playlist';
import { IUser } from '../user';
import { IInappropriate } from '../inappropriate';

export interface IItem {
  id: string;
  description?: string;
  summary?: string;
  title: string;
  link?: string;
  created?: string;
  avatar?: Blob | string;
  inappropriate?: IInappropriate;
  idFavourite?: string | null;
  playlist?: IPlaylist;
  author?: IUser;
  idPlaylist?: string;
  numberComments?: number;
}
