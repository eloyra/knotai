import { BaseModel } from '../base-model';
import { IComment } from './icomment';
import { User } from '../user';

export class Comment extends BaseModel implements IComment {

  public static readonly _resource: string = 'comments';

  id?: string;
  comment: string;
  idUser?: string;
  idItem?: string;
  author?: User;
  created?: Date;

  constructor(data?: IComment) {
    super(data);
  }

}
