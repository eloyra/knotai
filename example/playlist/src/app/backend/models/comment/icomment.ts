import { IUser } from '../user';

export interface IComment {
  id?: string;
  comment: string;
  idUser?: string;
  idItem?: string;
  author?: IUser;
  created?: Date;
}
