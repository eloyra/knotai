import { IUser } from './iuser';
import { BaseModel } from '../base-model';
import { UserRelations } from './user-relations';
import { Role } from '../../services/config/BackendConstants';

export class User extends BaseModel implements IUser {

  public static readonly _resource: string = 'users';

  id: string;
  firstname?: string;
  lastname?: string;
  email: string;
  lang?: string;
  avatar?: string;
  relations?: UserRelations;
  roles?: Role[];

  constructor(data?: IUser) {
    super(data);
  }

  get displayName() {
    return this.firstname + ' ' + this.lastname;
  }

}
