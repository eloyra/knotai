import { UserRelations } from './user-relations';
import { Role } from '../../services/config/BackendConstants';

export interface IUser {
  id: string;
  firstname?: string;
  lastname?: string;
  email: string;
  lang?: string;
  avatar?: string;
  relations?: UserRelations,
  roles?: Role[];
}
