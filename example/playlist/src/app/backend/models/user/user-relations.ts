interface UserRelationsBase {
  [item: string]: {
    elements: number | UserRelationsBase
  }
}

export interface UserRelations extends UserRelationsBase {
  playlists?: {
    elements: number,
    hasNewSubscribers?: boolean
  },
  subscriptions?: {
    elements: {
      playlists?: {
        elements: number,
        hasNewItems?: boolean
      },
      items?: {
        elements: number
      }
    }
  },
  favourites?: {
    elements: number
  },
  reviews?: {
    elements: {
      inappropriate?: {
        elements: number,
        hasNewItems?: boolean
      },
      inappropriateOthers?: {
        elements: number,
        hasNewItems?: boolean
      }
    }
  },
}
