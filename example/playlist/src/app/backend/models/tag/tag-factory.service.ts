import { Injectable } from '@angular/core';
import { IFactory } from '../../services/base/IFactory';
import { Tag } from './tag';
import { ITag } from './itag';

@Injectable({
  providedIn: 'root'
})
export class TagFactoryService implements IFactory<Tag> {

  constructor() {}

  build(data: ITag): Tag {

    return new Tag(data);

  }

}
