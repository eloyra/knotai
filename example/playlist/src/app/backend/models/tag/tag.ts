import { BaseModel } from '../base-model';
import { ITag } from './itag';
import { IPlaylist } from '../playlist';

export class Tag extends BaseModel implements ITag {

  public static readonly _resource: string = 'tags';

  id?: string;
  name?: string;

  constructor(data?: ITag) {
    super(data);
  }

}
