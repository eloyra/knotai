import { Injectable } from '@angular/core';
import { IFactory } from '../../services/base/IFactory';
import { Favourite } from './favourite';
import { IFavourite } from './ifavourite';

@Injectable({
  providedIn: 'root'
})
export class FavouriteFactoryService implements IFactory<Favourite> {

  constructor() {}

  build(data: IFavourite): Favourite {

    return new Favourite(data);

  }

}
