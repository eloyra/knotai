import { BaseModel } from '../base-model';
import { IFavourite } from './ifavourite';

export class Favourite extends BaseModel implements IFavourite {

  public static readonly _resource: string = 'favourites';

  id?: string;
  idItem?: string;

  constructor(data?: IFavourite) {
    super(data);
  }

}
