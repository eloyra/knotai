export interface IInappropriate {
  id?: string;
  comment?: string;
  idItem?: string;
  authorReviewed?: boolean;
  active?: boolean;
}
