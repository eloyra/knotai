import { Injectable } from '@angular/core';
import { IFactory } from '../../services/base/IFactory';
import { Inappropriate } from './inappropriate';
import { IInappropriate } from './iinappropriate';

@Injectable({
  providedIn: 'root'
})
export class InappropriateFactoryService implements IFactory<Inappropriate> {

  constructor() {}

  build(data: IInappropriate): Inappropriate {

    return new Inappropriate(data);

  }

}
