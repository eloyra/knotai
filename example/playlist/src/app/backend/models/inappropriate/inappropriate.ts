import { BaseModel } from '../base-model';
import { IInappropriate } from './iinappropriate';

export class Inappropriate extends BaseModel implements IInappropriate {

  public static readonly _resource: string = 'inappropriates';

  id?: string;
  comment?: string;
  idItem?: string;
  authorReviewed?: boolean;
  active?: boolean;

  constructor(data?: IInappropriate) {
    super(data);
  }

}
