export interface ApplicationsMenuItem {
  image: {
    url: string;
    label: string;
  };
  url: string;
  active?: boolean;
  app: 'lcloud' | 'lplay' | 'lcentral' | 'lsocial';
  home?: boolean;
  target?: '_self' | '_blank';
}
