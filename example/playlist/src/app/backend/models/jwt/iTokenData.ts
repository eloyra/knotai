import { Role } from '../../services/config/BackendConstants';

export interface ITokenData {
  sub?: string;
  locale?: string;
  user_id?: string;
  jtid?: string;
  user_name?: string;
  tenant_id?: string;
  client_id?: string;
  aud: string[];
  scope: string[];
  authorities?: Role[];
  exp: number;
}
