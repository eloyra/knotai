import * as jwt_decode from 'jwt-decode';
import { ITokenData } from './iTokenData';

export class JwtToken {

  public static readonly _resource: string = 'auth';

  constructor(
    public token: string
  ) {}

  get _resource(): string {
    return JwtToken._resource;
  }

  decode(): ITokenData {
    return jwt_decode(this.token);
  }

}
