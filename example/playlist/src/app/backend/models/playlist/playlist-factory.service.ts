import { Injectable } from '@angular/core';
import { IFactory } from '../../services/base/IFactory';
import { Playlist } from './playlist';
import { IPlaylist } from './iplaylist';
import { User, UserFactoryService } from '../user';
import { ITag, Tag, TagFactoryService } from '../tag';

@Injectable({
  providedIn: 'root'
})
export class PlaylistFactoryService implements IFactory<Playlist> {

  private tagFactoryService = new TagFactoryService();

  constructor() {}

  build(data: IPlaylist): Playlist {

    if (data.author) {
      data.author = new UserFactoryService().build(data.author);
    }

    if (data.tags && data.tags[0] && typeof data.tags[0] !== 'string') {
      data.tags = (data.tags as ITag[]).map(tag => this.tagFactoryService.build(tag));
    }

    return new Playlist(data);

  }

}
