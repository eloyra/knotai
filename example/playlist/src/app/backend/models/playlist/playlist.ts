import { BaseModel } from '../base-model';
import { IPlaylist, PlaylistConfiguration } from './iplaylist';
import { User } from '../user';
import { Tag } from '../tag';

export class Playlist extends BaseModel implements IPlaylist {

  public static readonly _resource: string = 'playlists';

  id?: string;
  configuration: PlaylistConfiguration;
  title: string;
  description?: string;
  avatar?: string;
  numberItems?: number;
  numberSubscribers?: number;
  author?: User;
  tags?: Tag[];
  idSubscription?: string | null;

  constructor(data?: IPlaylist) {
    super(data);
  }

}
