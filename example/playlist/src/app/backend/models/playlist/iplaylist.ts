import { IUser } from '../user';
import { ITag } from '../tag';

export enum PlaylistConfiguration {
  PUBLIC = 1,
  PRIVATE = 2,
  OWNER_ONLY = 3
}

export interface IPlaylist {
  id?: string;
  configuration: PlaylistConfiguration;
  title: string;
  description?: string;
  avatar?: string;
  numberItems?: number;
  numberSubscribers?: number;
  author?: IUser;
  tags?: ITag[] | ITag['id'][];
  idSubscription?: string | null;
}
