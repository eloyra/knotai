export abstract class BaseModel {

  protected constructor(data?: object) {
    if (data) {
      Object.assign(this, data);
    }
  }

}
