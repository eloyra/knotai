import { AuthService } from '../../auth/auth.service';
import { environment } from '../../../../environments/environment';
import { isArray, isObject } from 'lodash';
import { Restangular } from 'ngx-restangular';

export function RestangularConfigFactory(RestangularProvider: Restangular['provider'], authService: AuthService) {

  // RestangularProvider.setDefaultHttpFields({withCredentials: true});
  RestangularProvider.setBaseUrl(environment.apiUrl);
  RestangularProvider.setRestangularFields({
    id: '@id',
    selfLink: 'self.link'
  });

  // Add headers.
  RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {
    const token = authService.getJwtToken() && authService.getJwtToken().token;
    headers = Object.assign({}, headers, {Accept: 'application/hal+json'});
    if (operation === 'put') {
      headers = Object.assign({}, headers, {'Content-Type': 'application/hal+json'});
    }
    if (token) {
      headers = Object.assign({}, headers, {Authorization: `Bearer ${token}`});
    }
    return {
      headers: headers
    };
  });

  // Extract items from list.
  RestangularProvider.addResponseInterceptor((data, operation, what) => {
    if (operation === 'getList') {
      const resp = data._embedded ? data._embedded.item : [];
      resp.itemsPerPage = data.itemsPerPage;
      resp.totalItems = data.totalItems;
      resp._links = data._links;
      return resp;
    }
    return data;
  });

  // Extract embedded.
  RestangularProvider.addResponseInterceptor((data, operation) => {
    if (!data) {
      return {};
    }

    function getFilterEmbedded(data: any) {
      if (data) {
        if (isArray(data)) {
          for (let i = 0; i < data.length; i++) {
            data[i] = getFilterEmbedded(data[i]);
          }
        } else if (isObject(data)) {
          for (const key of Object.keys(data)) {
            if (key === '_embedded') {
              for (const keyEmbedded of Object.keys(data[key])) {
                data[keyEmbedded] = getFilterEmbedded(data[key][keyEmbedded]);
              }
              delete data[key];
            } else if (isArray(data[key])) {
              data[key] = getFilterEmbedded(data[key]);
            }
          }
        }
      }
      return data;
    }

    switch (operation) {
      case 'getList':
      case 'get':
      case 'post':
        return getFilterEmbedded(data);
      case 'patch':
        if (typeof data === 'boolean') {
          return {
            ok: data
          };
        }
    }

    return data;

  })

}
