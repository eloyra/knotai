export enum Role {
  ADMIN = 'playlist-admin',
  USER = 'playlist-user'
}
