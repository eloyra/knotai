export interface IFactory<T> {
  build(data: T): T;
}
