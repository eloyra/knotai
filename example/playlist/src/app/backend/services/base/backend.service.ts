import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { isArray } from 'lodash';
import { Restangular } from 'ngx-restangular';
import { Sort } from '@angular/material';

export interface JsonPatch<T> {
  op: 'add' | 'remove' | 'replace';
  path: string;
  value?: T;
}

export class RestangularCollection<T> extends Array<T> {
  totalItems?: number;
  itemsPerPage?: number;
}

@Injectable()
export abstract class BackendService<T> {

  protected alias: string;
  protected idField = 'id';
  protected factory = null;

  protected constructor(
    protected restangular: Restangular
  ) {
    this.setup();
  }

  protected abstract get resource(): string;

  protected abstract get class(): any;

  static makeCriteriasCopy(criterias: Object, additionalCriterias: Object = {}): Object {
    return Object.assign({}, criterias, additionalCriterias);
  }

  static makeCriterias(criterias: Object = {}, pageNumber?: number): Object {
    const pageCriteria = BackendService.makeCriteriasCopy(criterias);
    for (const criteria of Object.keys(pageCriteria)) {
      if (isArray(pageCriteria[criteria])) {
        pageCriteria[criteria] = pageCriteria[criteria].join(',');
      }
      if (pageCriteria[criteria] === undefined || pageCriteria[criteria] === null) {
        delete pageCriteria[criteria];
      }
    }
    if (pageCriteria['sort'] && typeof pageCriteria['sort'] !== 'string') {
      pageCriteria['sort'] = `${(pageCriteria['sort'] as Sort).active},${(pageCriteria['sort'] as Sort).direction}`;
    }
    if (pageNumber) {
      pageCriteria['page'] = pageNumber;
    }
    return pageCriteria;
  }

  setup(): void {
    this.addDefaultTransformer(this.resource);
    if (this.class.alias) {
      this.addDefaultTransformer(this.class.alias);
    }
  }

  setIdField(idField: string): void {
    this.idField = idField;
  }

  getAlias(): string {
    return (this.class.alias) ? this.class.alias : this.resource;
  }

  get(id: string, criterias: Object = {}): Observable<T> {
    return this.restangular.one(this.resource, id).get(BackendService.makeCriterias(criterias));
  }

  getAll(pageNumber?: number, criterias: Object = {}): Observable<T[]> {
    return this.restangular.all(this.resource).getList(BackendService.makeCriterias(criterias, pageNumber));
  }

  getAllBy(field: string, value: any, pageNumber?: number, criterias: Object = {}): Observable<T[]> {
    return this.restangular.all(this.resource).customGETLIST(`${field}/${value}`, BackendService.makeCriterias(criterias, pageNumber));
  }

  getAllByFilter(filter: string, value: any, pageNumber?: number, criterias: Object = {}): Observable<T[]> {
    const getCriterias = BackendService.makeCriteriasCopy(criterias);
    getCriterias[filter] = value;
    return this.getAll(pageNumber, getCriterias);
  }

  add(item: T): Observable<T> {
    return this.restangular.all(this.resource).post(item);
  }

  update(item: T): Observable<T> {
    return this.restangular.one(this.resource, item[this.idField]).customPUT(item);
  }

  addFormData(item: T): Observable<T> {
    const formData: FormData = new FormData();
    for (const key in item) {
      if (item.hasOwnProperty(key)) {
        formData.append(key, isArray(item[key]) ? (<any>item[key]).map(_ => JSON.stringify(_)) : <any>item[key]);
      }
    }
    return (
      // If idField is present this POST acts as a PUT
      this.restangular.one(this.resource, item[this.idField] || '')
        .customPOST(formData, undefined, undefined, {'Content-Type': undefined})
    );
  }

  updateJsonPatch(added: T[], removed: T[]): Observable<any> {
    const patch: JsonPatch<T>[] = [
      ...removed.map(item => this.genJsonPatch(item, 'remove')),
      ...added.map(item => this.genJsonPatch(item, 'add'))
    ];
    if (!patch.length) {
      return of([]);
    }
    return this.restangular.all(this.resource).patch(patch);
  }

  remove(item: T): Observable<any> {
    return this.restangular.one(this.resource, item[this.idField]).remove();
  }

  private genJsonPatch(item: T, op: JsonPatch<T>['op']): JsonPatch<T> {
    const jsonPatch: JsonPatch<T> = {
      op,
      path: '/' + (item[this.idField] || '-')
    };
    if (op === 'add') {
      jsonPatch.value = item as T;
    }
    return jsonPatch;
  }

  private addDefaultTransformer(resource) {
    this.restangular.provider.addElementTransformer(resource, item => {
      if (!item[this.idField]) {
        return item;
      }
      item = item.plain();
      if (this.factory) {
        item = this.factory.build(item);
      } else {
        item = new (this.class)(item);
      }
      return item;
    });
  }

}
