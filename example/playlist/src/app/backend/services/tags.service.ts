import { Injectable } from '@angular/core';
import { BackendService } from './base/backend.service';
import { Tag, TagFactoryService } from '../models/tag';
import { Restangular } from 'ngx-restangular';

@Injectable()
export class TagsService extends BackendService<Tag> {

  constructor(
    protected restangular: Restangular,
    protected factory: TagFactoryService
  ) {
    super(restangular);
  }

  protected get resource() {
    return Tag._resource;
  }

  protected get class() {
    return Tag;
  }

}
