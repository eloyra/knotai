import { Injectable } from '@angular/core';
import { BackendService } from './base/backend.service';
import { Subscription, SubscriptionFactoryService } from '../models/subscription';
import { Restangular } from 'ngx-restangular';

@Injectable()
export class SubscriptionsService extends BackendService<Subscription> {

  constructor(
    protected restangular: Restangular,
    protected factory: SubscriptionFactoryService
  ) {
    super(restangular);
  }

  protected get resource() {
    return Subscription._resource;
  }

  protected get class() {
    return Subscription;
  }

}
