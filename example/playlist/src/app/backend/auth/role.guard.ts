import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { CurrentUserFactory } from './current-user';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    private currentUser: CurrentUserFactory,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.currentUser.isRole(next.data.role)) {
      return true;
    }
    if (next.data.redirectTo) {
      this.router.navigate([next.data.redirectTo]);
    } else {
      this.router.navigate(['/']);
    }
    return false;
  }

}
