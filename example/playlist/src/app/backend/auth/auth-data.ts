import { JwtToken } from '../models/jwt/jwt-token';

export interface AuthDataInterface {
  jwtToken?: JwtToken;
}

export class AuthData implements AuthDataInterface {
  jwtToken: JwtToken = null;

  constructor(data?: AuthDataInterface) {
    Object.assign(this, data);
  }
}

