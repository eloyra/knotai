import { Injectable } from '@angular/core';
import { User, UserRelations } from '../models/user';
import { AuthService } from './auth.service';
import { environment } from '../../../environments/environment';
import { Role } from '../services/config/BackendConstants';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { UsersService } from '../services';
import { ApplicationsMenuItem, ContextualMenu } from '../models/user-menu';
import { ITokenData } from '../models/jwt/iTokenData';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserFactory {

  public relations$: Observable<UserRelations>;

  public companyId: string;
  public id: string;
  public userName: string;
  public contextualMenu: ContextualMenu;
  public applicationsMenu: ApplicationsMenuItem[];
  public urlPlay: string;

  private relationsSubject = new BehaviorSubject<UserRelations>({});
  private userSubject = new ReplaySubject<User>(1);
  private loadingUser: boolean;

  private authorities: Role[] = [];
  private user: User;
  private _user$: Observable<User>;

  constructor(
    private authService: AuthService,
    private userService: UsersService
  ) {
    this.relations$ = this.relationsSubject.asObservable();
    this._user$ = this.userSubject.asObservable();
  }

  get user$(): Observable<User> {
    if (!this.loadingUser && !this.user) {
      this.refreshUserData();
    }
    return this._user$;
  }

  public refreshUserData() {
    const token = this.authService.getJwtToken();
    if (token) {
      const tokenData: ITokenData = token.decode();
      if (tokenData) {
        this.companyId = tokenData.tenant_id;
        this.userName = tokenData.user_name;
      }
    }
    if (!environment.production && !this.id) {
      this.id = environment['userId'];
    }
    this.getUserData();
  }

  public isRole(role: Role) {
    return this.authorities.includes(role);
  }

  public reload() {
    this.getUserData();
  }

  private getUserData() {
    this.loadingUser = true;
    this.userService.get(this.id).pipe(
      finalize(() => this.loadingUser = false)
    ).subscribe(user => {
      this.user = user;
      if (user.roles && user.roles.length) {
        this.authorities = user.roles;
      }
      this.userSubject.next(user);
      this.relationsSubject.next(user.relations);
    });
  }

}
