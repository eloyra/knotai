import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './auth/auth.service';
import {
  CommentsService,
  FavouritesService,
  InappropriateService,
  ItemsService,
  PlaylistsService,
  SubscriptionsService,
  TagsService,
  UsersService
} from './services';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class BackendModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: BackendModule,
      providers: [
        AuthService,
        CommentsService,
        FavouritesService,
        InappropriateService,
        ItemsService,
        PlaylistsService,
        SubscriptionsService,
        TagsService,
        UsersService
      ]
    };
  }
}
