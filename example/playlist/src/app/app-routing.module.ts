import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemsBookmarkletComponent } from './e-portfolio/components/items/items-bookmarklet/items-bookmarklet.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'e-portfolio',
    pathMatch: 'full'
  },
  {
    path: 'bookmarklet/share',
    component: ItemsBookmarkletComponent
  },
  {
    path: '**',
    redirectTo: 'e-portfolio'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
