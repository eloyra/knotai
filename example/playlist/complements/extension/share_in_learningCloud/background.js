(function () {

  var webAppUrl = 'http://localhost:4200/bookmarklet/share';
  var contextMenuId = '_learningCloud';

  window.browser = (function () {
    return window.msBrowser || window.browser || window.chrome;
  })();

  /* Browser action (extension clicked) */

  window.browser.browserAction.onClicked.addListener(function () {
    window.browser.tabs.query({active: true, currentWindow: true}, function (tabs) {
      runExtension(tabs[0].url);
    });
  });

  /* Context menu on page links */

  window.browser.contextMenus.create({
    'id': contextMenuId,
    'title': window.browser.i18n.getMessage('brwActionTitle'),
    'contexts': ['link']
  });

  window.browser.contextMenus.onClicked.addListener(function (info) {
    if (info.menuItemId === contextMenuId) {
      runExtension(info.linkUrl);
    }
  });

  /* Extension execution */

  function runExtension(url) {
    window.browser.windows.create({
      url: webAppUrl + '?url=' + url,
      type: 'popup',
      width: 650,
      height: 600,
      incognito: false,
      state: 'normal'
    });
  }

})();
